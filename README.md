# MPExpanded

MPExpanded is a modded effort to bring 16 or more players to Rise Of Nations. The mod is currently in the development stage

## Progress

- [x] `ConnectionData::player_data_server` (41 functions)
- [ ] `ConnectionData::last_sent_player_data` (5 functions)
- [ ] `ConnectionData::allocated_slots` (8 functions)
- [ ] 16 colors
- [ ] `SetupWin::combo_boxes` (A lot, haven't looked yet, it's gonna be crippling depression)
- [ ] `SetupWin::ready_boxes` (Guess what, crippling depression)
- [ ] `SetupWin::elo_boxes` (iDubbbz is having competition)
- [ ] `NetSys::players` (16 functions)
- [ ] `GameAccess::game::info::players` (231 functions. Yes, I'm serious. I need help, like, a therapist or something)
- [ ] `Random shit I have overlooked so far`
- [ ] Bigger maps?

## Inner workings

Patching C++ compiled binaries is hard. This mod luckily needs only support for the 32-bit compiled binary since I don't think that there are other versions. If there is an ARM version I'm gonna cry. I'd rather like someone to buy a new processor than requiring an ARM version of this. SO, with that out of the way, problematic areas in the compiled code are identified using Ghidra, and when found out, using the macros `INJECTION_CODE_BEGIN` and `INJECTION_CODE_END` we can write assembly code in between that will be inline-compiled and directly injected into the compiled binary. The macros identify the start and end point of the assembly and make it possible to read them from the mod binary. After that, the instructions are copied and often inserted using JMP-instructions to the appended executable section in the PE. This is done for every line that requires patching since the patches mostly require more space than is available and fucking hell, there's no way I'm gonna relocate the binary since that's a nightmare nightmare nightmare. As you can see, I'm slowly losing my sanity

## Development process

We have a lot of information about naming and such since the RON developers were kindly enough to leave PDB files shipped with the game. Combined with a decompiler like Ghidra we can infer information and code structure. Beginning here, it is identifiable where patches need to be made. After a list of patches were applied to replace a field/function, testing begins. This can be done using Cheat Engine and the Event Log from windows, showing potential crash locations that are not identifiable using Cheat Engine. A lot of patience is adviced.

