#include "InjectionBase.h"
#include <iostream>

namespace MPExpanded
{
#pragma region INJECTION_CODE
	// Currently patching basic_string::assign to find invalid EBX values
	INJECTION_FUNCTION void Injector::ConnectionDataDebugPatch()
	{
		INJECTION_CODE_BEGIN(DebugPatchStart, DebugPatchEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH EBX
			MOV EBX, dword ptr [EBP + 0x8]
			PUSH ESI
			MOV ESI, ECX
			MOV ECX, dword ptr [EBP + 0xC]
			PUSH EDI

			CMP EBX, 0x40000000
			JLE doReturn

			NOP

			doReturn:
				MOV EAX, 0x0045a0ae
				JMP EAX
		}
		INJECTION_CODE_END(DebugPatchEnd)
		
		InsertJumpWithData(start, end, 0x0005a0a0, "DebugPatch");
	}

	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataUnreadyAllPlayers()
	{
		INJECTION_CODE_BEGIN(ConnectionDataUnreadyAllPlayersStart, ConnectionDataUnreadyAllPlayersEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			AND ESP, 0xFFFFFFF8
			PUSH ECX
			PUSH EBX
			MOV EBX, ECX
			PUSH ESI
			PUSH EDI
			XOR ESI, ESI
			LEA EDI, [EBX + 0x14C8]

			loopBegin:
				PUSH 0x0
				PUSH ESI
				MOV ECX, EBX
				MOV byte ptr [EDI], 0x0
				MOV EAX, 0x0100a220     ; ConnectionData::send_player
				CALL EAX

				INC ESI
				LEA EDI, [EDI + 0x3B]
				CMP ESI, NEW_PLAYER_COUNT
				JL loopBegin

			POP EDI
			POP ESI
			POP EBX
			MOV ESP, EBP
			POP EBP
			RET
		}
		INJECTION_CODE_END(ConnectionDataUnreadyAllPlayersEnd)

		return InsertJumpWithData(start, end, 0x00c08720, "ExtendedConnectionData::UnreadyAllPlayers()");
	}
	
	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataGetPlayerIndex()
	{
		INJECTION_CODE_BEGIN(ConnectionDataGetPlayerIndexStart, ConnectionDataGetPlayerIndexEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH -0x1
			PUSH 0x11b38d8
			MOV EAX, FS:[0x0]
			PUSH EAX
			MOV dword ptr FS:[0x0], ESP
			PUSH ECX
			PUSH EBX
			PUSH ESI
			PUSH EDI
			LEA EDI, [ECX + CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN]
			MOV dword ptr [EBP - 0x4], 0x0
			XOR EAX, EAX
			MOV dword ptr [EBP - 0x10], EAX ; playerIndex

			loopBegin:
				CMP dword ptr [EBP + 0x1C], 0x8
				LEA EAX, [EBP + 0x8]
				MOV ECX, dword ptr [EBP + 0x18]
				CMOVNC EAX, dword ptr [EBP + 0x8]

				CMP dword ptr [EDI + 0x14], 0x8
				JC someFlagComparison
				MOV EDX, dword ptr [EDI]
				JMP someFlagComparisonEnd
				someFlagComparison:
					MOV EDX, EDI
				someFlagComparisonEnd:
					MOV EBX, dword ptr [EDI + 0x10]
					MOV ESI, ECX
					CMP EBX, ECX
					CMOVC ESI, EBX
					TEST ESI, ESI

				forLoopStart:
					JNZ forLoopContent
					XOR EAX, EAX

				forEnd:
					TEST EAX, EAX
					JNZ holyShitCompilerWhatHaveYouDone
					MOV EAX, dword ptr [EBP + 0x18]
					CMP EBX, EAX
					JC holyShitCompilerWhatHaveYouDone
					JBE endLabel

				holyShitCompilerWhatHaveYouDone:
					MOV ECX, dword ptr [EBP - 0x10]
					ADD EDI, 0x3B
					INC ECX
					MOV dword ptr [EBP - 0x10], ECX
					CMP ECX, NEW_PLAYER_COUNT
					JL loopBegin
					OR EBX, 0xFFFFFFFF

				whyWouldYouStructureCodeLikeThis:
					MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
					MOV EAX, dword ptr [EBP + 0x1C]
					CMP EAX, 0x8
					JC functionEnd
					PUSH 0x2
					INC EAX
					PUSH EAX
					PUSH dword ptr [EBP + 0x8]
					MOV EAX, 0x00458340                ; std::_Deallocate
					CALL EAX
					MOV EAX, dword ptr [EBP + 0x1C]
					ADD ESP, 0xC

				functionEnd:
					MOV ECX, dword ptr [EBP - 0xC]
					XOR EAX, EAX
					POP EDI
					MOV word ptr [EBP + 0x8], AX
					MOV EAX, EBX
					POP ESI
					MOV dword ptr [EBP + 0x1C], 0x7
					MOV dword ptr [EBP + 0x18], 0x0
					POP EBX
					MOV dword ptr FS:[0x0], ECX
					MOV ESP, EBP
					POP EBP
					RET 0x18

				forLoopContent:
					MOV CX, word ptr [EDX]
					CMP CX, word ptr [EAX]
					JNZ idsEqual
					ADD EDX, 0x2
					ADD EAX, 0x2
					SUB ESI, 0x1
					JMP forLoopStart

				idsEqual:
					SBB EAX, EAX
					AND EAX, 0xFFFFFFFE
					INC EAX
					JMP forEnd

				endLabel:
					MOV EBX, dword ptr [EBP - 0x10]
					JMP whyWouldYouStructureCodeLikeThis
		}
		INJECTION_CODE_END(ConnectionDataGetPlayerIndexEnd)

		return InsertJumpWithData(start, end, 0x00c08750, "ExtendedConnectionData::GetPlayerIndex(std::wstring playerId)");
	}
	
	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataInitFunction()
	{
		INJECTION_CODE_BEGIN(ConnectionDataInitStart, ConnectionDataInitEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			SUB ESP, 0x8
			PUSH EBX
			PUSH ESI
			LOAD_CONNECTION_DATA(ESI)
			MOV ECX, ESI
			PUSH EDI
			MOV dword ptr [EBP - 0x8], ESI
			MOV EAX, 0x0100b880                               ; ConnectionData::close
			CALL EAX
			MOV EAX, dword ptr [EBP + 0x8]
			XOR EBX, EBX
			MOV dword ptr [ESI + 0x1450], EAX
			XOR EDI, EDI
			MOV EAX, dword ptr [EBP - 0x8]
			ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN ; player_data_server
			ADD EAX, 0x1D8                                    ; allocated_slots
			MOV dword ptr [EBP - 0x4], EAX

			loopBegin:
				PUSH 0x0
				PUSH 0x131cfbc
				MOV ECX, EAX
				MOV EAX, 0x00459fa0                          ; basic_string::assign
				CALL EAX
				
				CMP dword ptr [EBP + 0x8], 0x0
				; GameAccessConst::gamec
				LOAD_STATIC_VALUE(ECX, 0x014aea28)
				MOV byte ptr [ESI + 0x34], 0x4
				JZ paramTwoIsZero

				MOVZX EAX, byte ptr [EDI + ECX * 0x1 + 0x76]
				MOV byte ptr [ESI + 0x35], AL
				MOVZX EAX, byte ptr [EDI + ECX * 0x1 + 0x77]
				MOV byte ptr [ESI + 0x36], AL
				MOVZX EAX, byte ptr [EDI + ECX * 0x1 + 0x78]
				MOV byte ptr [ESI + 0x37], AL
				MOVZX EAX, byte ptr [EDI + ECX * 0x1 + 0x79]
				MOV byte ptr [ESI + 0x38], AL
				MOVZX EAX, byte ptr [EDI + ECX * 0x1 + 0x7c]
				MOV byte ptr [ESI + 0x39], AL
				JMP ifStatementDone

				paramTwoIsZero:
					MOV byte ptr [ESI + 0x35], 0x18
					MOV byte ptr [ESI + 0x36], BL
					MOV byte ptr [ESI + 0x37], 0x8
					MOV byte ptr [ESI + 0x39], 0x2

				ifStatementDone:
					MOV EAX, dword ptr [EBP - 0x4]
					ADD EDI, 0x8C
					MOV byte ptr [ESI + 0x3A], 0x0
					INC EBX
					MOV dword ptr [EBP - 0x4], EAX
					ADD ESI, 0x3B
					CMP EBX, NEW_PLAYER_COUNT
					JL loopBegin

			MOVUPS XMM0, xmmword ptr [ECX + 0x24]
			MOV EDX, dword ptr [EBP - 0x8]
			POP EDI
			POP ESI
			POP EBX
			MOVUPS xmmword ptr [EDX + 0x298], XMM0
			MOVQ XMM0, qword ptr [ECX + 0x34]
			MOVQ qword ptr [EDX + 0x2A8], XMM0
			MOV EAX, dword ptr [ECX + 0x3C]
			MOV dword ptr [EDX + 0x2B0], EAX
			MOVZX EAX, word ptr [ECX + 0x40]
			MOV word ptr [EDX + 0x2B4], AX
			DEC byte ptr [EDX + 0x2AA]
			MOV dword ptr [EDX + 0x2BA], 0x0
			MOV EAX, dword ptr [ECX + 0x20]
			MOV dword ptr [EDX + 0x2BE], EAX
			MOVZX EAX, byte ptr [ECX + 0x14]
			MOV byte ptr [EDX + 0x2C4], AL
			MOVZX EAX, word ptr [ECX + 0x18]
			MOV word ptr [EDX + 0x2C2], AX
			MOVZX EAX, byte ptr [ECX + 0x1C]
			MOV byte ptr [EDX + 0x2C5], AL
			MOV ESP, EBP
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(ConnectionDataInitEnd)

		return InsertJumpWithData(start, end, 0x00c0aa20, "ExtendedConnectionData::Init()");
	}

	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataConstructor()
	{
		/*
			The old ConnectionData was looking like this:
			struct ConnectionData {
				PlayerConnectionData player_data_server[8];    // 59x8 bytes = 472
				basic_string allocated_slots[8];               // 192 bytes total
				GameConnectionDataFull game_data;              // 2031 bytes
				PlayerConnectionData[8] last_send_player_data; // 59x8 bytes = 472
				GameConnectionDataFull last_send_data;         // 2031 bytes
				int using_config;                              // 4 bytes
				String full_save_path;                         // 20 bytes
				String full_scenario_path;                     // 20 bytes
				String full_script_path;                       // 20 bytes
			}

			The new ConnectionData will be appended by two PlayerConnectionData[16]'s representing the new data that will replace the old ones.
			Re-ordering the struct will take way too much work in a lot of places, so no thank you. The new struct will append to the old 5264 bytes,
			for a total memory size of 7150 bytes.
			The new ctor will first of all malloc 7152 bytes and store the resulting pointer base in the global connection_data, thus replacing the old existing pointer
			to the preallocated memory. After a memset to -1 it will initialize everything like before except for the two player connection arrays.
		*/

		INJECTION_CODE_BEGIN(NewConnectionDataConstructorStart, NewConnectionDataConstructorEnd)
		_asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH -0x1
			PUSH 0xdb3c0c
			MOV EAX, FS:[0x0]
			PUSH EAX
			MOV dword ptr FS:[0x0], ESP
			PUSH ECX                              ; Complete stack safety checks
			PUSH EBX
			PUSH EDX

			PUSH CONNECTION_DATA_SIZE             ; Push new size to stack for malloc call
			MOV EAX, 0x011d58c4                   ; malloc
			CALL dword ptr [EAX]                  ; Call malloc

			ADD ESP, 0x4                          ; Clean up stack since malloc is cdecl
			TEST EAX, EAX                         ; Check if return of malloc is 0
			JNZ mallocSuccess
			MOV ECX, 0x014aea6c                   ; connection_data
			MOV dword ptr [ECX], 0x0              ; set connection_data to zero
			JMP cleanup
			mallocSuccess:
				MOV ECX, 0x014aea6c               ; connection_data
				MOV dword ptr [ECX], EAX          ; set connection_data to the newly created pointer
				PUSH CONNECTION_DATA_SIZE
				PUSH 0x0
				PUSH EAX
				MOV EDX, 0x00c1b270               ; _memset
				CALL EDX                          ; Call memset to 0 the entire memory
				ADD ESP, 0xC
				MOV dword ptr [EBP - 0x10], EAX   ; Load EAX into EBP - 0x10

				XOR ECX, ECX                      ; Set ECX to 0
				MOV EAX, dword ptr [EBP - 0x10]
				MOV dword ptr [EAX + 0x1458], ECX ; full_save_path.offset and curr_len
				MOV dword ptr [EAX + 0x1452], ECX ; full_save_path.const_str
				MOV byte ptr [EAX + 0x145C],  CL  ; full_save_path.flags
				MOV ECX, [0x015ab035]             ; STR_MODULE_ID
				XOR EBX, EBX
				MOV BL, [ECX]
				XOR ECX, ECX                      ; Clear ECX again
				MOV byte ptr [EAX + 0x145D],  BL  ; full_save_path.module_id
				MOV dword ptr [EAX + 0x145E], ECX ; full_save_path.hash_value
				MOV dword ptr [EAX + 0x1462], ECX ; full_save_path.hash_value_insensitive
				MOV word ptr [EAX + 0x1456],  CX  ; full_save_path.const_len
				MOV dword ptr [EAX + 0x146C], ECX ; full_scenario_path.offset and curr_len
				MOV dword ptr [EAX + 0x1466], ECX ; full_scenario_path.const_str
				MOV byte ptr [EAX + 0x1470],  CL  ; full_scenario_path.flags
				MOV byte ptr [EAX + 0x1471],  BL  ; full_scenario_path.module_id
				MOV dword ptr [EAX + 0x1472], ECX ; full_scenario_path.hash_value
				MOV dword ptr [EAX + 0x1476], ECX ; full_scenario_path.hash_value_insensitive
				MOV word ptr [EAX + 0x146A],  CX  ; full_scenario_path.const_len
				MOV dword ptr [EAX + 0x1480], ECX ; full_script_path.offset and curr_len
				MOV dword ptr [EAX + 0x147A], ECX ; full_script_path.const_str
				MOV byte ptr [EAX + 0x1484],  CL  ; full_script_path.flags
				MOV byte ptr [EAX + 0x1485],  BL  ; full_script_path.module_id
				MOV dword ptr [EAX + 0x1486], ECX ; full_script_path.hash_value
				MOV dword ptr [EAX + 0x148A], ECX ; full_script_path.hash_value_insensitive
				MOV word ptr [EAX + 0x147E],  CX  ; full_script_path.const_len
				MOV dword ptr [EAX + 0x144E], ECX ; connection_data.using_config

				PUSH 0x004721b0                   ; Push PlayerConnection::~PlayerConnection
				PUSH 0x0049b0f0                   ; Push PlayerConnection::PlayerConnection
				PUSH NEW_PLAYER_COUNT             ; Push array size 16
				PUSH 0x3b                         ; Push sizeof(PlayerConnection)
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX                          ; Push connection_data.player_data_server
				MOV EDX, 0x00c186d4               ; vectorCtorIter
				CALL EDX

				PUSH 0x004721b0                   ; Push PlayerConnection::~PlayerConnection
				PUSH 0x0049b0f0                   ; Push PlayerConnection::PlayerConnection
				PUSH NEW_PLAYER_COUNT             ; Push array size 16
				PUSH 0x3b                         ; Push sizeof(PlayerConnection)
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN
				PUSH EAX                          ; Push connection_data.last_sent_player_data
				MOV EDX, 0x00c186d4               ; vectorCtorIter
				CALL EDX

				PUSH 0x00459c80                   ; basic_string::~basic_string
				PUSH 0x00465330                   ; basic_string::basic_string
				PUSH NEW_PLAYER_COUNT
				PUSH 0x18
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, CONNECTION_DATA_ALLOCATED_SLOTS_BEGIN
				PUSH EAX                          ; EAX containing connection_data at an offset of 472 makes allocated_slots
				MOV EDX, 0x00c186d4               ; Create an array of 8 std::basic_string types. vectorCtorIter
				CALL EDX

				; Create the TeamColor array of size 18 (16 for all colors and 2 extra for all-white and transparent)
				PUSH 0x455cf0                     ; guard_check_icall
				PUSH 0x4e26c0                     ; TeamColor::TeamColor
				PUSH 0x12
				PUSH 0x64
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN
				PUSH EAX
				MOV EDX, 0x00c186d4
				CALL EDX

				; initialize colors
				XOR EBX, EBX

				; Color red
				XOR ECX, ECX
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0000FF
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000066
				MOV word ptr [EAX + ECX + 0x8], 0x3000
				MOV word ptr [EAX + ECX + 0xA], 0x6000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF6464FF
				MOV word ptr [EAX + ECX + 0x8], 0x7D8C
				MOV word ptr [EAX + ECX + 0xA], 0xFB2C
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0000FF
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0000FF
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0000FF
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF2F2F7F
				MOV word ptr [EAX + ECX + 0x8], 0x3CA5
				MOV word ptr [EAX + ECX + 0xA], 0x7965
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF7F5F5F
				MOV word ptr [EAX + ECX + 0x8], 0x3D6B
				MOV word ptr [EAX + ECX + 0xA], 0x7AEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color blue
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF5222
				MOV word ptr [EAX + ECX + 0x8], 0x115F
				MOV word ptr [EAX + ECX + 0xA], 0x229F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFAC391A
				MOV word ptr [EAX + ECX + 0x8], 0x0CF5
				MOV word ptr [EAX + ECX + 0xA], 0x19D5
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF6446
				MOV word ptr [EAX + ECX + 0x8], 0x219F
				MOV word ptr [EAX + ECX + 0xA], 0x433F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF6446
				MOV word ptr [EAX + ECX + 0x8], 0x219F
				MOV word ptr [EAX + ECX + 0xA], 0x433F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF0000
				MOV word ptr [EAX + ECX + 0x8], 0x001F
				MOV word ptr [EAX + ECX + 0xA], 0x001F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF0000
				MOV word ptr [EAX + ECX + 0x8], 0x001F
				MOV word ptr [EAX + ECX + 0xA], 0x001F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9F2F2F
				MOV word ptr [EAX + ECX + 0x8], 0x14B3
				MOV word ptr [EAX + ECX + 0xA], 0x2973
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF7F5F5F
				MOV word ptr [EAX + ECX + 0x8], 0x2D6F
				MOV word ptr [EAX + ECX + 0xA], 0x5AEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color purple
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFF00FF
				MOV word ptr [EAX + ECX + 0x8], 0x7C1F
				MOV word ptr [EAX + ECX + 0xA], 0xF81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF800080
				MOV word ptr [EAX + ECX + 0x8], 0x4010
				MOV word ptr [EAX + ECX + 0xA], 0x8010
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFA00BC
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFA00BC
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFA00BC
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFA00BC
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF7F3F7F
				MOV word ptr [EAX + ECX + 0x8], 0x3CEF
				MOV word ptr [EAX + ECX + 0xA], 0x79EF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF7F5F7F
				MOV word ptr [EAX + ECX + 0x8], 0x3D6F
				MOV word ptr [EAX + ECX + 0xA], 0x7AEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color green
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00D000
				MOV word ptr [EAX + ECX + 0x8], 0x0340
				MOV word ptr [EAX + ECX + 0xA], 0x0680
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF005900
				MOV word ptr [EAX + ECX + 0x8], 0x0160
				MOV word ptr [EAX + ECX + 0xA], 0x02C0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF21EC00
				MOV word ptr [EAX + ECX + 0x8], 0x03A4
				MOV word ptr [EAX + ECX + 0xA], 0x0764
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FF00
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FF00
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FF00
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF3F7F3F
				MOV word ptr [EAX + ECX + 0x8], 0x1DE7
				MOV word ptr [EAX + ECX + 0xA], 0x3BE7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF5F7F5F
				MOV word ptr [EAX + ECX + 0x8], 0x2DEB
				MOV word ptr [EAX + ECX + 0xA], 0x5BEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color yellow
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF006062
				MOV word ptr [EAX + ECX + 0x8], 0x3180
				MOV word ptr [EAX + ECX + 0xA], 0x6300
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF37EFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FA6
				MOV word ptr [EAX + ECX + 0xA], 0xFF66
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF00FFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF3F7F3F
				MOV word ptr [EAX + ECX + 0x8], 0x3DE7
				MOV word ptr [EAX + ECX + 0xA], 0x7BE7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF5F7F5F
				MOV word ptr [EAX + ECX + 0x8], 0x3DEB
				MOV word ptr [EAX + ECX + 0xA], 0x7BEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color cyan
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFF00
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF6C5C00
				MOV word ptr [EAX + ECX + 0x8], 0x016D
				MOV word ptr [EAX + ECX + 0xA], 0x02ED
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFF00
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFF00
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFF00
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFF00
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF3F7F3F
				MOV word ptr [EAX + ECX + 0x8], 0x1DEF
				MOV word ptr [EAX + ECX + 0xA], 0x3BEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF5F7F5F
				MOV word ptr [EAX + ECX + 0x8], 0x2DEF
				MOV word ptr [EAX + ECX + 0xA], 0x5BEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color white
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF787878
				MOV word ptr [EAX + ECX + 0x8], 0x3DEF
				MOV word ptr [EAX + ECX + 0xA], 0x7BCF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFBFBFBF
				MOV word ptr [EAX + ECX + 0x8], 0x5EF7
				MOV word ptr [EAX + ECX + 0xA], 0xBDF7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9F9F9F
				MOV word ptr [EAX + ECX + 0x8], 0x4E73
				MOV word ptr [EAX + ECX + 0xA], 0x9CF3
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color orange
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0069FF
				MOV word ptr [EAX + ECX + 0x8], 0x7DA0
				MOV word ptr [EAX + ECX + 0xA], 0xFB40
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF003070
				MOV word ptr [EAX + ECX + 0x8], 0x38C0
				MOV word ptr [EAX + ECX + 0xA], 0x7180
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF06B6FF
				MOV word ptr [EAX + ECX + 0x8], 0x7EC0
				MOV word ptr [EAX + ECX + 0xA], 0xFDA0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0080FF
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0080FF
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0080FF
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF2F5F7F
				MOV word ptr [EAX + ECX + 0x8], 0x3D65
				MOV word ptr [EAX + ECX + 0xA], 0x7AE5
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF5F6F7F
				MOV word ptr [EAX + ECX + 0x8], 0x3DAB
				MOV word ptr [EAX + ECX + 0xA], 0x7B6B
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color black
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x3000
				MOV word ptr [EAX + ECX + 0xA], 0x6000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x7D8C
				MOV word ptr [EAX + ECX + 0xA], 0xFB2C
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x7C00
				MOV word ptr [EAX + ECX + 0xA], 0xF800
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x3CA5
				MOV word ptr [EAX + ECX + 0xA], 0x7965
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF000000
				MOV word ptr [EAX + ECX + 0x8], 0x3D6B
				MOV word ptr [EAX + ECX + 0xA], 0x7AEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color brown
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x115F
				MOV word ptr [EAX + ECX + 0xA], 0x229F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x0CF5
				MOV word ptr [EAX + ECX + 0xA], 0x19D5
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x219F
				MOV word ptr [EAX + ECX + 0xA], 0x433F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x219F
				MOV word ptr [EAX + ECX + 0xA], 0x433F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x001F
				MOV word ptr [EAX + ECX + 0xA], 0x001F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x001F
				MOV word ptr [EAX + ECX + 0xA], 0x001F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x14B3
				MOV word ptr [EAX + ECX + 0xA], 0x2973
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004064
				MOV word ptr [EAX + ECX + 0x8], 0x2D6F
				MOV word ptr [EAX + ECX + 0xA], 0x5AEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color silver
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x7C1F
				MOV word ptr [EAX + ECX + 0xA], 0xF81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x4010
				MOV word ptr [EAX + ECX + 0xA], 0x8010
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x5C1F
				MOV word ptr [EAX + ECX + 0xA], 0xB81F
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x3CEF
				MOV word ptr [EAX + ECX + 0xA], 0x79EF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF808080
				MOV word ptr [EAX + ECX + 0x8], 0x3D6F
				MOV word ptr [EAX + ECX + 0xA], 0x7AEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color dark purple
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x0340
				MOV word ptr [EAX + ECX + 0xA], 0x0680
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x0160
				MOV word ptr [EAX + ECX + 0xA], 0x02C0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x03A4
				MOV word ptr [EAX + ECX + 0xA], 0x0764
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x03E0
				MOV word ptr [EAX + ECX + 0xA], 0x07E0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x1DE7
				MOV word ptr [EAX + ECX + 0xA], 0x3BE7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFC40040
				MOV word ptr [EAX + ECX + 0x8], 0x2DEB
				MOV word ptr [EAX + ECX + 0xA], 0x5BEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color dark green
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x3180
				MOV word ptr [EAX + ECX + 0xA], 0x6300
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x7FA6
				MOV word ptr [EAX + ECX + 0xA], 0xFF66
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x7FE0
				MOV word ptr [EAX + ECX + 0xA], 0xFFE0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x3DE7
				MOV word ptr [EAX + ECX + 0xA], 0x7BE7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF004000
				MOV word ptr [EAX + ECX + 0x8], 0x3DEB
				MOV word ptr [EAX + ECX + 0xA], 0x7BEB
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color gold
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x016D
				MOV word ptr [EAX + ECX + 0xA], 0x02ED
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x03FF
				MOV word ptr [EAX + ECX + 0xA], 0x07FF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x1DEF
				MOV word ptr [EAX + ECX + 0xA], 0x3BEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF0F8EC4
				MOV word ptr [EAX + ECX + 0x8], 0x2DEF
				MOV word ptr [EAX + ECX + 0xA], 0x5BEF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color pink
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x3DEF
				MOV word ptr [EAX + ECX + 0xA], 0x7BCF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x5EF7
				MOV word ptr [EAX + ECX + 0xA], 0xBDF7
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF9089C0
				MOV word ptr [EAX + ECX + 0x8], 0x4E73
				MOV word ptr [EAX + ECX + 0xA], 0x9CF3
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				INC EBX
				ADD EAX, 0x64

				; Color dark gray
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x7DA0
				MOV word ptr [EAX + ECX + 0xA], 0xFB40
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x38C0
				MOV word ptr [EAX + ECX + 0xA], 0x7180
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x7EC0
				MOV word ptr [EAX + ECX + 0xA], 0xFDA0
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x7E00
				MOV word ptr [EAX + ECX + 0xA], 0xFC00
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x3D65
				MOV word ptr [EAX + ECX + 0xA], 0x7AE5
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFF303030
				MOV word ptr [EAX + ECX + 0x8], 0x3DAB
				MOV word ptr [EAX + ECX + 0xA], 0x7B6B
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				XOR EBX, EBX
				ADD EAX, 0x64

				; Color allwhite
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0xFFFFFFFF
				MOV word ptr [EAX + ECX + 0x8], 0x7FFF
				MOV word ptr [EAX + ECX + 0xA], 0xFFFF
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				ADD EAX, 0x64

				; Color transparent
				XOR ECX, ECX
				MOV dword ptr [EAX], EBX ; TeamColor::who

				; TeamColor::unit
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::build
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::text
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::neon
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::border
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::iface
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::unit_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

				; TeamColor::built_tint
				ADD ECX, 0xC
				MOV dword ptr [EAX + ECX + 0x4], 0x00000000
				MOV word ptr [EAX + ECX + 0x8], 0x0000
				MOV word ptr [EAX + ECX + 0xA], 0x0000
				MOV byte ptr [EAX + ECX + 0xC], 0x2
				MOV byte ptr [EAX + ECX + 0xD], 0x0

			cleanup:
				POP EDX
				POP EBX
				MOV ECX, dword ptr [EBP + -0xc]
				MOV dword ptr FS:[0x0], ECX
				MOV ESP, EBP
				POP EBP
				RET
		}
		INJECTION_CODE_END(NewConnectionDataConstructorEnd)
			
		return InsertJumpWithData(start, end, 0x00c0b750, "ExtendedConnectionData::ExtendedConnectionData");
	}
	
	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataCloseFunction()
	{
		INJECTION_CODE_BEGIN(NewConnectionDataCloseFuncStart, NewConnectionDataCloseFuncEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH ECX
			PUSH EBX
			PUSH ESI
			PUSH EDI
			MOV EDI, ECX
			MOV dword ptr [EBP - 0x4], EDI
			LEA EBX, [EDI + 0x1D8]                                    ; connection_data.allocated_slots
			LEA ESI, [EDI + CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN] ; connection_data.player_data_server (Offset by 0x38)
			MOV EDI, NEW_PLAYER_COUNT
			; There used to be a multi-byte nop here but for some reason this shitty c-inline compiler doesn't support them

			playerDataServerLoop:
				PUSH 0x0
				PUSH 0x131cfc0
				LEA ECX, [ESI]
				MOV word ptr [ESI], 0x0
				MOV EAX, 0x00459fa0     ; basic_string::assign
				CALL EAX
					
				MOV ECX, EBX
				MOV byte ptr [ESI + 0x3A], 0x0
				MOV dword ptr [ESI + 0x34], 0x0
				MOV dword ptr [ESI + 0x30], 0x0

				CMP EDI, 0x8
				JLE completePlayerDataServerLoop

				PUSH 0x0
				PUSH 0x131cfd0
				MOV EAX, 0x00459fa0     ; basic_string::assign
				CALL EAX
				ADD EBX, 0x18
					
				completePlayerDataServerLoop:
					LEA ESI, [ESI + 0x3B]
					SUB EDI, 0x1
					JNZ playerDataServerLoop

			PUSH 0x7ef ; sizeof(GameConnectionDataFull)
			PUSH EDI
			MOV EDI, dword ptr [EBP - 0x4]
			LEA EAX, [EDI + 0x298] ; connection_data.game_data
			PUSH EAX
			MOV EAX, 0x00c1b270      ; _memset
			CALL EAX

			ADD ESP, 0xC
			LEA ECX, [EDI + 0x1452]  ; connection_data.full_save_path
			PUSH 0x179d06c          ; EMPTY_STRING
			MOV EAX, 0x010da9c0      ; String::operator=
			CALL EAX
					
			PUSH 0x179d06c          ; EMPTY_STRING
			LEA ECX, [EDI + 0x1466]  ; connection_data.full_scenario_path
			MOV EAX, 0x010da9c0      ; String::operator=
			CALL EAX
					
			PUSH 0x179d06c          ; EMPTY_STRING
			LEA ECX, [EDI + 0x147A]  ; connection_data.full_script_path
			MOV EAX, 0x010da9c0      ; String::operator=
			CALL EAX

			LEA ESI, [EDI + CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN]  ; connection_data.last_sent_player_data offset by 0x18
			MOV EBX, NEW_PLAYER_COUNT

			lastSentPlayerDataLoop:
				PUSH 0x0
				PUSH 0x131cfd4
				MOV ECX, ESI
				MOV EAX, 0x00459fa0     ; basic_string::assign
				CALL EAX

				PUSH 0x0
				PUSH 0x131cfcc
				LEA ECX, [ESI + 0x18]
				MOV EAX, 0x00459fa0     ; basic_string::assign
				CALL EAX

				MOV dword ptr [ESI + 0x30], 0xFFFFFFFF
				MOV dword ptr [ESI + 0x34], 0xFFFFFFFF
				MOV word ptr [ESI + 0x38], 0xFFFF
				MOV byte ptr [ESI + 0x3A], 0xFF
				ADD ESI, 0x3B
				SUB EBX, 0x1
				JNZ lastSentPlayerDataLoop

			PUSH 0x2E
			LEA EAX, [EDI + 0xC5F] ; connection_data.last_sent_data
			PUSH -0x1
			PUSH EAX
			MOV EAX, 0x00c1b270               ; _memset
			CALL EAX
					
			XOR EAX, EAX
			MOV byte ptr [EDI + 0xC8D], BL
			MOV word ptr [EDI + 0xD41], AX
			MOV byte ptr [EDI + 0xF49], AL
			MOV byte ptr [EDI + 0x1149], AL
			MOV dword ptr [EDI + 0x13FD], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x1401], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x1405], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x1409], 0xFFFFFFFF

			PUSH 0x2D
			LEA EAX, [EDI + 0x140D]
			PUSH -0x1
			PUSH EAX
			MOV EAX, 0x00c1b270               ; _memset
			CALL EAX

			ADD ESP, 0x18
			MOV dword ptr [EDI + 0x143A], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x143E], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x1442], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x1446], 0xFFFFFFFF
			MOV dword ptr [EDI + 0x144a], 0xFFFFFFFF

			POP EDI
			POP ESI
			POP EBX
			MOV ESP, EBP
			POP EBP
			RET
		}
		INJECTION_CODE_END(NewConnectionDataCloseFuncEnd)

		return InsertJumpWithData(start, end, 0x00c0b880, "ExtendedConnectionData::Close");
	}

	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataDestructor()
	{
		INJECTION_CODE_BEGIN(NewConnectionDataDestructorStart, NewConnectionDataDestructorEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH -0x1
			PUSH 0x1115c4d
			MOV EAX, FS:[0x0]
			PUSH EAX
			MOV dword ptr FS:[0x0], ESP
			PUSH ECX

			LOAD_CONNECTION_DATA(ECX)
			MOV dword ptr [EBP - 0x10], ECX   ; Load ECX into EBP - 0x10
			MOV EAX, 0x0100b880               ; ConnectionData::close
			CALL EAX

			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, 0x147c                   ; connection_data.full_script_path
			MOV EAX, 0x010d8a50               ; String::close
			CALL EAX

			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, 0x1468                   ; connection_data.full_scenario_path
			MOV EAX, 0x010d8a50               ; String::close
			CALL EAX

			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, 0x1454                   ; connection_data.full_save_path
			MOV EAX, 0x010d8a50               ; String::close
			CALL EAX

			PUSH 0x004721b0                  ; PlayerConnectionData::~PlayerConnectionData
			PUSH NEW_PLAYER_COUNT
			PUSH 0x3b
			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN
			PUSH ECX                         ; connection_data.last_sent_player_data
			MOV EAX, 0x00c185ed              ; vectorDtorIter
			CALL EAX

			PUSH 0x00459c80                  ; basicStringDtor
			PUSH 0x8
			PUSH 0x18
			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, 0x1D8
			PUSH ECX
			MOV EAX, 0x00c185ed              ; vectorDtorIter
			CALL EAX

			PUSH 0x004721b0                  ; PlayerConnectionData::~PlayerConnectionData
			PUSH NEW_PLAYER_COUNT
			PUSH 0x3b
			MOV ECX, dword ptr [EBP - 0x10]
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			PUSH ECX                         ; connection_data.player_data_server
			MOV EAX, 0x00c185ed              ; vectorDtorIter
			CALL EAX

			MOV ECX, dword ptr [EBP - 0xc]
			MOV dword ptr FS:[0x0], ECX
			MOV ESP, EBP
			POP EBP
			RET
		}
		INJECTION_CODE_END(NewConnectionDataDestructorEnd)

		return InsertJumpWithData(start, end, 0x00072270, "ExtendedConnectionData::~ExtendedConnectionData()");
	}

	INJECTION_FUNCTION bool Injector::InsertNewRandomLambdaConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(RandomLambdaStart, RandomLambdaEnd)
		__asm
		{
			XOR EBX, EBX
			XOR EDI, EDI
			loopBegin:
				LOAD_CONNECTION_DATA(ESI)
				ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x10], ESP
				XOR EAX, EAX
				ADD ESI, EDI
				PUSH -0x1
				PUSH EAX
				MOV dword ptr [ECX - 0x20], 0x7
				MOV dword ptr [ECX - 0x24], 0x0
				PUSH ESI
				MOV word ptr [ECX], AX
				MOV EDX, 0x0045a0a0 ; basic_string::assign
				CALL EDX

				; netsys
				LOAD_STATIC_VALUE(EAX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
				CALL dword ptr [EAX + 0x90]

				CMP byte ptr [ESI + 0x34], 0x0
				JNZ connectionSuccess

				TEST EAX, EAX
				JZ connectionMayHaveFailed

				CMP byte ptr [EAX + 0x28], 0x0
				JNZ connectionSuccess

				connectionMayHaveFailed:
					CMP dword ptr [ESI + 0x14], 0x8
					JC dontLoadEsiAsPtr

					MOV ESI, dword ptr [ESI]
				dontLoadEsiAsPtr:
					; netsys
					LOAD_STATIC_VALUE(EAX, 0x0171c2e8)
					PUSH ESI
					PUSH EBX
					PUSH 0x1350ea0
					MOV ECX, dword ptr [EAX]
					PUSH EAX
					CALL dword ptr [ECX + 0xBC]
					ADD ESP, 0x10

				connectionSuccess:
					ADD EDI, 0x3b
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL loopBegin

			MOV ECX, 0x004859A8
			JMP ECX
		}
		INJECTION_CODE_END(RandomLambdaEnd)

		return InsertJumpWithData(start, end, 0x00085918, "SomeRandomLambda");
	}

	INJECTION_FUNCTION bool Injector::InsertNewRandomBoolLambdaConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(RandomBoolLambdaStart, RandomBoolLambdaEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EBX)
			IMUL ECX, EAX, 0x3B
			ADD EBX, ECX
			ADD EBX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			LEA ECX, [EBP + 0x20]
			LEA EAX, [EBP - 0x54]
			ADD EDI, 0x1C
			PUSH EBX
			PUSH EAX
			MOV EAX, 0x004f6690 ; a func holding some string value whatever
			CALL EAX

			SUB EBX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN ; Just to be sure the lamdba doesnt fuck shit up
			MOV EAX, 0x004f5c36
			JMP EAX
		}
		INJECTION_CODE_END(RandomBoolLambdaEnd)

		return InsertJumpWithData(start, end, 0x000f5c1d, "SomeRandomBoolLambda", JumpType::USE_ECX);
	}

	INJECTION_FUNCTION bool Injector::InsertNewRandomIntLambdaConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(RandomIntLambdaStart, RandomIntLambdaEnd)
		__asm
		{
			loopBegin:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, ESI
				CMP byte ptr [EAX + 0x34], 0x0
				JNZ slotTypeNotPlayer

				SUB ESP, 0x18
				XOR EDX, EDX
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x10], ESP
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; string::assign
				CALL EAX

				MOV dword ptr [EBP - 0x4], 0x0
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
				CALL dword ptr [EAX + 0x90]

				TEST EAX, EAX
				JZ someElse

				CMP byte ptr [EAX + 0x28], 0x0
				JZ someElse

				MOV EAX, 0x1
				JMP afterIfElse

				someElse:
					XOR EAX, EAX

				afterIfElse:
					AND BL, AL

				slotTypeNotPlayer:
					ADD ESI, 0x3B
					CMP ESI, NEW_PLAYER_OBJECT_SIZE
					JL loopBegin

			MOV EAX, 0x0049b2cf
			JMP EAX
		}
		INJECTION_CODE_END(RandomIntLambdaEnd)

		return InsertJumpWithData(start, end, 0x0009B262, "SomeRandomIntLambda");
	}

	INJECTION_FUNCTION bool Injector::InsertNewLambdaDataHandlingForStartGame()
	{
		INJECTION_CODE_BEGIN(StartGameSomeLambdaStart, StartGameSomeLambdaEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(ECX)
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			PUSH 0x0
			PUSH 0x0
			MOV byte ptr [ECX + 0x3A], 0x2 ; ready = ingame
			SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV EAX, 0x0100a220 ; ConnectionData::send_player
			CALL EAX

			; int_str_array_orig
			LOAD_STATIC_VALUE(EAX, 0x014aebb8)
			LEA ECX, [ESI + 0x25C0]
			MOV EDX, dword ptr [ECX]
			PUSH 0x0
			MOV EAX, dword ptr [EAX + 0x10]
			ADD EAX, 0x57D0
			PUSH EAX
			CALL dword ptr [EDX + 0x424]

			PUSH 0x1
			MOV ECX, EAX
			MOV EAX, 0x0110fb60 ; Window::set_enabled
			CALL EAX

			POP ESI
			MOV ECX, dword ptr [EBP - 0xC]
			MOV dword ptr FS:[0x0], ECX
			MOV ESP, EBP
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(StartGameSomeLambdaEnd)

		return InsertJumpWithData(start, end, 0x0009b4b5, "SomeLambdaForStartGame");
	}

	INJECTION_FUNCTION bool Injector::InsertNewCheckForUwpPlayersConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(CheckForUwpPlayersStart, CheckForUwpPlayersEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x30], ESP
				XOR EDX, EDX
				ADD EAX, ESI
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				MOV EAX, 0x00c749fb
				JMP EAX
			}
			INJECTION_CODE_END(CHeckForUwpPlayersEnd)

			if (!InsertJumpWithData(start, end, 0x008749d0, "ExtendedSetupWin::check_for_uwp_players"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineCmpPatchStart, SingleLineCmpPatchEnd)
		__asm
		{
			CMP ESI, NEW_PLAYER_OBJECT_SIZE
		}
		INJECTION_CODE_END(SingleLineCmpPatchEnd)
		
		return WriteBytesToData(start, end, _codeBase + 0x00874a93);
	}

	INJECTION_FUNCTION bool Injector::InsertNewCheckAllReadyConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(CheckAllReadyStart, checkAllReadyEnd)
		__asm
		{
			loopBegin:
				LOAD_CONNECTION_DATA(ESI)
				IMUL EAX, EDI, 0x3B
				ADD ESI, EAX
				ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				CMP byte ptr [ESI + 0x34], 0x0
				JNZ dontBreak

				CMP byte ptr [ESI + 0x3A], 0x0
				JNZ dontBreak

				SUB ESP, 0x18
				XOR EAX, EAX
				MOV ECX, ESP
				MOV dword ptr [EBP + 0x14], ESP
				PUSH -0x1
				PUSH EAX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH ESI
				MOV word ptr [ECX], AX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]
					
				MOV ECX, EAX
				TEST ECX, ECX
				JZ perhapsBreakFromLoop

				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x10]
				TEST EAX, EAX
				JNZ dontBreak

				perhapsBreakFromLoop:
					TEST EDI, EDI
					JNZ breakFromLoop

					CMP dword ptr [EBP + 0x8], EDI
					JZ breakFromLoop

				dontBreak:
					LEA EAX, [EBP - 0x2C]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00466ee0 ; basic_string::compare
					CALL EAX

					TEST EAX, EAX
					MOV ESI, 0x1
					CMOVZ EBX, ESI
					INC EDI
					CMP EDI, NEW_PLAYER_COUNT
					JL loopBegin
					JMP loopEnd

				breakFromLoop:
					MOV ESI, 0x1

			loopEnd:
				TEST EBX, EBX
				JZ notAllReady

				CMP EDI, NEW_PLAYER_COUNT
				JNZ notAllReady

				MOV EAX, 0x00c79286
				JMP EAX

			notAllReady:
				MOV EAX, 0x00c792aa
				JMP EAX
		}
		INJECTION_CODE_END(CheckAllReadyEnd)

		return InsertJumpWithData(start, end, 0x008791f0, "ExtendedSetupWin::check_all_ready");
	}
	
	INJECTION_FUNCTION bool Injector::InsertNewValidateStartConnectionDataHandling()
	{
		// These names are _this_ stupid that I have to keep them. I mean, they aren't even necessarry other than if
		// two injections happen to be in one function, but I like the idea behind names and labels
		INJECTION_CODE_BEGIN(ValidateStartStart, ValidateStartEnd)
		__asm
		{
			JMP actualCode
			returnZero:
				XOR EAX, EAX
				POP EDI
				POP ESI
				POP EBX
				MOV ESp, EBP
				POP EBP
				RET 0x8

			actualCode:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				XOR ECX, ECX

			loopBegin:
				CMP byte ptr [EAX + 0x34], 0x0
				JNZ loopEnd
				CMP dword ptr [EAX + 0x30], 0x0
				JLE loopContent

				loopEnd:
					INC ECX
					ADD EAX, 0x3B
					CMP ECX, NEW_PLAYER_COUNT
					JL loopBegin
					MOV EAX, 0x00c77964
					JMP EAX

				loopContent:
					PUSH 0x0
					MOV ECX, 0x179dcc0 ; some popup
					MOV EAX, 0x00e7d5b0 ; Popup::init
					CALL EAX

					; loc_str_array_orig
					LOAD_STATIC_VALUE(EAX, 0x01535540)
					MOV ECX, 0x179dcc0 ; popup
					ADD EAX, 0x137a4
					PUSH EAX
					MOV EAX, 0x00566dc0 ; BasePop::string
					CALL EAX

					PUSH 0x0
					PUSH 0x0
					MOV ECX, 0x179dcc0 ; popup
					MOV EAX, 0x00567cc0 ; BasePop::exec
					CALL EAX

					; GameAccess::game
					LOAD_STATIC_VALUE(EAX, 0x014aea2c)
					CMP byte ptr [EAX + 0x4A4], 0x0 ; is_ranked
					JZ returnZero

					; netsys
					LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
					TEST ECX, ECX
					JZ returnZero

					MOV EAX, dword ptr [ECX]
					MOV EAX, dword ptr [EAX + 0x10]
					CALL EAX
					TEST AL, AL
					JZ returnZero

					MOV EAX, dword ptr [EDI + 0x2184] ; SetupWin::combo_boxes[0x36] TODO will need modification
					MOV EDX, dword ptr [EDI + 0x338C]
					MOV ECX, dword ptr [EAX + 0x568]
					MOV EAX, dword ptr [EDX]
					MOV ESI, dword ptr [EAX + 0xC]
					CMP EAX, 0x1345404 ; SteamELOHelper::vftable
					JNZ lastReturn

					MOV EAX, 0x010207d0 ; GameSpy::GetLeaderboardFromGameMode
					CALL EAX
					CMP EAX, -0x1
					JZ anotherReturn

					CMP EAX, 0x5
					JGE anotherReturn

					MOV EAX, dword ptr [EAX * 0x4 + 0x133AEF0]
					LEA ECX, [EDX + 0x10]
					PUSH EAX
					MOV EAX, 0x010f1940
					CALL EAX

					XOR EAX, EAX
					POP EDI
					POP ESI
					POP EBX
					MOV ESP, EBP
					POP EBP
					RET 0x8

					anotherReturn:
						XOR EAX, EAX
						LEA ECX, [EDX + 0x10]
						PUSH EAX
						MOV EAX, 0x010f1940 ; SteamLeaderboards::FindLeaderboard
						CALL EAX

						XOR EAX, EAX
						POP EDI
						POP ESI
						POP EBX
						MOV ESP, EBP
						POP EBP
						RET 0x8

					lastReturn:
						PUSH ECX
						MOV ECX, EDX
						CALL ESI
						POP EDI
						POP ESI
						XOR EAX, EAX
						POP EBX
						MOV ESP, EBP
						POP EBP
						RET 0x8
		}
		INJECTION_CODE_END(ValidateStartEnd)

		return InsertJumpWithData(start, end, 0x00877944, "ExtendedSetupWin::validate_start");
	}
	
	INJECTION_FUNCTION bool Injector::InjectSetupWinAddChatConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupWinAddChatJmpModificationStart, SetupWinAddChatJmpModificationEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL ECX, EDI, 0x3B
				MOVZX EAX, byte ptr [ECX + EAX + 0x3A]
				TEST EAX, EAX
				JZ ifZeroJmpHere

				MOV EAX, 0x00c847c2
				JMP EAX

				ifZeroJmpHere:
					MOV EAX, 0x00c8473a
					JMP EAX
			}
			INJECTION_CODE_END(SetupWinAddChatJmpModificationEnd)

			if (!InsertJumpWithData(start, end, 0x008847AD, "ExtendedSetupWin::AddChat()"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupWinAddChatJmpModificationTwoStart, SetupWinAddChatJmpModificationTwoEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				LEA ECX, [ESI + ECX]
				MOV EAX, 0x00c846cc
				JMP EAX
			}
			INJECTION_CODE_END(SetupWinAddChatJmpModificationTwoEnd)

			if (!InsertJumpWithData(start, end, 0x008846c2, "ExtendedSetupWin::AddChat()", JumpType::USE_ECX))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineCmpPatchStart, SingleLineCmpPatchEnd)
		__asm
		{
			CMP ESI, NEW_PLAYER_OBJECT_SIZE
		}
		INJECTION_CODE_END(SingleLineCmpPatchEnd)
		
		return WriteBytesToData(start, end, _codeBase + 0x0088472e);
	}

	INJECTION_FUNCTION bool Injector::InjectSetupWinUpdateLobbyNumPlayersConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinUpdateLobbyNumPlayersJmpModificationStart, SetupWinUpdateLobbyNumPlayersJmpModificationEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(ECX)
			XOR EBX, EBX
			XOR EDX, EDX
			XOR EDI, EDI
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			LEA ESI, [EBX + NEW_PLAYER_COUNT]

			loopBegin:
				MOVZX EAX, byte ptr [ECX + 0x34] ; slot_type
				TEST EAX, EAX
				JZ slotTypeIsPlayer

				CMP EAX, 0x7
				JZ slotTypeIsPlayer

				CMP EAX, 0x4
				JNZ slotTypeIsNotOpen

				INC EDX
				JMP ifElseEnd

				slotTypeIsNotOpen:
					CMP EAX, 0x1
					JZ slotTypeIsComputer

					CMP EAX, 0x2
					JZ slotTypeIsComputer

					CMP EAX, 0x3
					JNZ ifElseEnd

				slotTypeIsComputer:
					INC EDI
					JMP ifElseEnd

				slotTypeIsPlayer:
					INC EBX

				ifElseEnd:
					ADD ECX, 0x3B
					SUB ESI, 0x1
					JNZ loopBegin

			MOV ECX, 0x00c83f9f
			JMP ECX
		}
		INJECTION_CODE_END(SetupWinUpdateLobbyNumPlayersJmpModificationEnd)

		return InsertJumpWithData(start, end, 0x00883f5E, "ExtendedSetupWin::UpdateLobbyNumPlayers()");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnCheckClickedFunction()
	{
		INJECTION_CODE_BEGIN(SetupWinReplaceLoopInOnCheckClickedStart, SetupWinReplaceLoopInOnCheckClickedEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH -0x1
			PUSH 0x11948cf
			MOV EAX, FS:[0x0]
			PUSH EAX
			MOV dword ptr FS:[0x0], ESP
			SUB ESP, 0x24
			PUSH EBX
			PUSH ESI
			PUSH EDI
			MOV ESI, ECX
			XOR EBX, EBX
			PUSH 0x111
			MOV dword ptr [EBP - 0x14], ESI
			MOV dword ptr [EBP - 0x18], EBX
			MOV EDI, 0x0103b2c0 ; SoundGlobal::play
			CALL EDI

			MOV EDI, dword ptr [EBP + 0x8]
			TEST EDI, EDI
			JNS param2IsNotNegative

			XOR EDI, EDI
			XOR ESI, ESI

			loopBegin:
				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				TEST ECX, ECX
				JZ netSysOrLocalPlayerNull
					
				MOV ECX, dword ptr [ECX + 0x28] ; local_player
				TEST ECX, ECX
				JZ netSysOrLocalPlayerNull

				MOV EAX, dword ptr [ECX]
				LEA EDX, [EBP - 0x30]
				PUSH EDX
				CALL dword ptr [EAX + 0x24]
				OR EBX, 0x1
				MOV dword ptr [EBP - 0x18], EBX
				LOAD_CONNECTION_DATA(ECX)
				PUSH EAX
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD ECX, ESI
				MOV EAX, 0x00466ee0 ; basic_string::compare
				CALL EAX

				MOV byte ptr [EBP - 0xD], 0x1
				TEST EAX, EAX
				JZ local11WillNotBeZero

				netSysOrLocalPlayerNull:
					MOV byte ptr [EBP - 0xD], 0x0

				local11WillNotBeZero:
					MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
					TEST BL, 0x1
					JZ someBoolIsFalse

					AND EBX, 0xFFFFFFFE
					MOV dword ptr [EBP - 0x18], EBX
					MOV EAX, dword ptr [EBP - 0x1C]
					CMP EAX, 0x8
					JC dontDeallocateARandomString

					PUSH 0x2
					INC EAX
					PUSH EAX
					PUSH dword ptr [EBP - 0x30]
					MOV EAX, 0x00458340 ; std::_Deallocate
					ADD ESP, 0xC

					dontDeallocateARandomString:
						XOR EAX, EAX
						MOV dword ptr [EBP - 0x1C], 0x7
						MOV dword ptr [EBP - 0x20], EAX
						MOV word ptr [EBP - 0x30], AX

				someBoolIsFalse:
					CMP byte ptr [EBP - 0xD], 0x0
					JNZ loopBreak

					ADD ESI, 0x3B
					INC EDI
					CMP EDI, NEW_PLAYER_COUNT
					JL loopBegin

					MOV ESI, dword ptr [EBP - 0x14]
					MOV EDI, dword ptr [EBP + 0x8]

			param2IsNotNegative:
				OR EBX, 0xFFFFFFFF

			someRandomAssLabel:
				TEST byte ptr [ESI + 0x1F14], 0x2 ; flags & 2 == 0
				JZ flagsNotEven

				TEST EDI, EDI
				JS functionEnd

				MOV EAX, dword ptr [ESI + EDI * 0x4 + 0x21E4] ; check_buttons[param2]
				PUSH ECX
				MOV ECX, ESI
				MOVZX EAX, byte ptr [EAX + 0x284] ; check_buttons[param2]->state
				NOT EAX
				AND EAX, 0x1
				PUSH EAX
				PUSH EDI
				MOV EAX, 0x00c78250 ; SetupWin::process_check_change
				CALL EAX

				PUSH 0x0
				MOV ECX, ESI
				MOV EAX, 0x00c70d10 ; SetupWin::update_interface
				CALL EAX
				JMP functionEnd

			loopBreak:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV EBX, EDI
				MOV ESI, dword ptr [EBP - 0x14]
				IMUL ECX, EDI, 0x3B
				CMP byte ptr [ECX + EAX * 0x1 + 0x3A], 0x0 ; is_ready
				JNZ dontValidateFileTransfer

				MOV ECX, ESI
				MOV EAX, 0x00c77ae0 ; SetupWin::validate_file_transfer
				CALL EAX

				TEST EAX, EAX
				JZ functionEnd

				dontValidateFileTransfer:
					MOV EDI, dword ptr [EBP + 0x8]
					JMP someRandomAssLabel

			flagsNotEven:
				TEST EBX, EBX
				JS ivarZeroNegative

				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL EDX, EBX, 0x3B
				PUSH 0x0
				PUSH EBX
				CMP byte ptr [EDX + ECX + 0x3A], 0x0 ; is_ready
				SETZ AL
				MOV byte ptr [EDX + ECX + 0x3A], AL
				MOV EAX, 0x0100a220 ; ConnectionData::send_player
				CALL EAX
				JMP functionEnd

			ivarZeroNegative:
				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				MOV EAX, dword ptr [EAX + 0x10]
				CALL EAX
				TEST Al, AL
				JZ functionEnd

				MOV EAX, dword ptr [ESI + EDI * 0x4 + 0x21E4]
				MOV ECX, ESI
				MOVZX EAX, byte ptr [EAX + 0x284]
				NOT EAX
				AND EAX, 0x1
				PUSH EAX
				PUSH EDI
				MOV EAX, 0x00c783c0 ; SetupWin::request_check_change
				CALL EAX

			functionEnd:
				MOV ECX, dword ptr [EBP - 0xC]
				XOR EAX, EAX
				POP EDI
				POP ESI
				POP EBX
				MOV dword ptr FS:[0x0], ECX
				MOV ESP, EBP
				POP EBP
				RET 0x4
		}
		INJECTION_CODE_END(SetupWinReplaceLoopInOnCheckClickedEnd)

		return InsertJumpWithData(start, end, 0x00881570, "ExtendedSetupWin::OnCheckClicked(int checkId)");
	}

	INJECTION_FUNCTION bool Injector::InjectSetupWinStartGameSuccessModifications()
	{
		INJECTION_CODE_BEGIN(SetupWinStartGameSuccessJumpModificationStart, SetupWinStartGameSuccessJumpModificationEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(ECX)
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			PUSH 0x0
			PUSH 0x0
			MOV byte ptr [ECX + 0x3A], 0x2 ; probably set ready to ingame
			SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV EAX, 0x0100a220 ; ConnectionData::send_player
			CALL EAX

			; int_str_array_orig
			LOAD_STATIC_VALUE(EAX, 0x014aebb8)
			LEA ECX, [ESI + 0x25C0]
			MOV EDX, dword ptr [ECX]
			PUSH 0x0
			MOV EAX, dword ptr [EAX + 0x10]
			ADD EAX, 0x57D0
			PUSH EAX
			CALL dword ptr [EDX + 0x424]
			PUSH 0x1
			MOV ECX, EAX
			MOV EAX, 0x0110fb60
			CALL EAX ; Window::set_enabled

			POP ESI
			MOV ESP, EBP
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(SetupWinStartGameSuccessJumpModificationEnd)

		return InsertJumpWithData(start, end, 0x00873B2F, "ExtendedSetupWin::StartGameSuccess");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnTimerConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinOnTimerStart, SetupWinOnTimerEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(ECX)
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV AL, byte ptr [ECX + 0x3A]
			CMP AL, 0x1
			JBE returnHere

			PUSH 0x0
			INC AL
			PUSH 0x0
			MOV byte ptr [ECX + 0x3A], AL
			SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV EAX, 0x0100a220 ; ConnectionData::send_player
			CALL EAX

			returnHere:
				RET 0x4
		}
		INJECTION_CODE_END(SetupWinOnTimerEnd)

		return InsertJumpWithData(start, end, 0x0087f070, "ExtendedSetupWin::OnTimer");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSendPlayerConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(ConnectionDataSendPlayerDataComparisonStart, ConnectionDataSendPlayerDataComparisonEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ESI)
				IMUL EAX, EDI, 0x3B
				ADD ESI, EAX
				LEA EDI, [ESI + CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN] ; TODO old last_sent_player_data
				ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV BL, byte ptr [EBP + 0xC]
				MOV EAX, 0x0100a30a
				JMP EAX
			}
			INJECTION_CODE_END(ConnectionDataSendPlayerDataComparisonEnd)

			if (!InsertJumpWithData(start, end, 0x00c0a2f8, "ExtendedConnectionData::SendPlayer"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(ConnectionDataSendPlayerDataEarlyIndexCheckStart, ConnectionDataSendPlayerDataEarlyIndexCheckEnd)
		__asm
		{
			CMP EDI, NEW_PLAYER_COUNT
			JGE noLocalNoHostPlayer

			MOV EAX, dword ptr [EDX]
			LEA ECX, [EBP - 0x44]
			PUSH ECX
			MOV ECX, EDX
			CALL dword ptr [EAX + 0x24]
			PUSH EAX

			LOAD_CONNECTION_DATA(ECX)
			IMUL EAX, EDI, 0x3B
			ADD ECX, EAX
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV EAX, 0x00466ee0 ; basic_string::compare
			CALL EAX

			MOV ESI, EAX
			LEA ECX, [EBP - 0x44]
			MOV EAX, 0x00459c80 ; basic_string::~basic_string
			CALL EAX

			MOV EAX, 0x0100a28f
			JMP EAX

			noLocalNoHostPlayer:
				MOV EAX, 0x0100a299
				JMP EAX
		}
		INJECTION_CODE_END(ConnectionDataSendPlayerDataEarlyIndexCheckEnd)

		return InsertJumpWithData(start, end, 0x00c0a25c, "ExtendedConnectionData::SendPlayer");
	}

	INJECTION_FUNCTION bool Injector::InsertNewAddPlayerConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(AddPlayerSlotCheckStart, AddPlayerSlotCheckEnd)
			__asm
			{
				XORPS XMM0, XMM0
				MOVUPS xmmword ptr [EBP + 0xffffff48], XMM0
				LEA EDI, [ESI + 0x34]
				ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, EDI
				MOV EDX, NEW_PLAYER_COUNT
				MOVUPS xmmword ptr [EBP + 0xffffff58], XMM0

				loopBegin:
					MOV AL, byte ptr [ECX]
					TEST AL, AL
					JZ ifConditionTrue

					CMP AL, 0x1
					JZ ifConditionTrue

					CMP AL, 0x2
					JZ ifConditionTrue

					CMP AL, 0x3
					JZ ifConditionTrue

					CMP AL, 0x8
					JMP ifConditionTrue

					CMP AL, 0x9
					JNZ ifConditionFalse

					ifConditionTrue:
						MOV AL, byte ptr [ECX + 0x2]
						CMP AL, 0x8
						JNC ifConditionFalse

						MOVZX EAX, AL
						INC dword ptr [EBP + EAX * 0x4 + 0xFFFFFF48]

					ifConditionFalse:
						ADD ECX, 0x3B
						SUB EDX, 0x1
						JNZ loopBegin

				MOV EAX, 0x0100ad76
				JMP EAX
			}
			INJECTION_CODE_END(AddPlayerSlotCheckEnd)

			if (!InsertJumpWithData(start, end, 0x00c0ad28, "ExtendedConnectionData::AddPlayer"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(AddPlayerSlotCheckTwoStart, AddPlayerSlotCheckTwoEnd)
			__asm
			{
				MOV EAX, NEW_PLAYER_COUNT
				DEC EAX
				IMUL EAX, 0x3B
				ADD EAX, 0x34
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				LEA ECX, [ESI + EAX]
				MOV EAX, NEW_PLAYER_COUNT
				MOV dword ptr [EBP - 0x2C], EAX

				loopBeginToo:
					CMP byte ptr [ECX], 0x4 ; slot_type == open
					JZ breakLoop
					DEC EAX
					SUB ECX, 0x3B
					MOV dword ptr [EBP - 0x2C], EAX
					TEST EAX, EAX
					JNZ loopBeginToo

				breakLoop:
					MOV ECX, 0x0100aef2
					JMP ECX
			}
			INJECTION_CODE_END(AddPlayerSlotCheckTwoEnd)

			if (!InsertJumpWithData(start, end, 0x00c0ae98, "ExtendedConnectionData::AddPlayer"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(AddPlayerSlotStart, AddPlayerSlotEnd)
		__asm
		{
			MOV ESI, dword ptr [EBP - 0x2C]
			LEA EAX, [EBX + 0x8]
			IMUL EDI, ESI, 0x3B
			ADD EDI, dword ptr [EBP - 0x30]
			ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV byte ptr [EDI + 0x34], 0x0
			PUSH 0x0100b027 ; I just hope this works but literally 0 addresses are available
			RET
		}
		INJECTION_CODE_END(AddPlayerSlotEnd)

		return InsertJumpWithData(start, end, 0x00c0b017, "ExtendedConnectionData::AddPlayer");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinRequestComboChangeConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinRequestComboChangeStart, SetupWinRequestComboChangeEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			AND ESP, 0xFFFFFFF8
			PUSH ECX
			PUSH EBX
			MOV EBX, ECX
			PUSH ESI
			PUSH EDI
			MOV dword ptr [ESP + 0xC], EBX
			TEST byte ptr [EBX + 0x1F14], 0x2
			JNZ processComboChange

			; netsys
			LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
			TEST ECX, ECX
			JZ processComboChange

			MOV ESI, dword ptr [EBP + 0x8]
			MOV EAX, NEW_PLAYER_COUNT
			IMUL EAX, 0x6
			CMP ESI, EAX
			JGE notAPlayerComboBox

			LOAD_CONNECTION_DATA(EDI)
			ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			CMP ESI, NEW_PLAYER_COUNT
			JGE checkForTribeModification

			; slot type
			MOV AL, byte ptr [EBP + 0xC]
			IMUL ECX, ESI, 0x3B
			MOV byte ptr [ECX + EDI + 0x34], AL
			JMP sendPlayer

			checkForTribeModification:
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x2
				CMP ESI, EAX
				JGE checkForColorModification

				SUB ESI, EAX
				ADD ESI, NEW_PLAYER_COUNT
				IMUL ECX, ESI, 0x3B
				MOV AL, byte ptr [EBP + 0xC]
				MOV byte ptr [ECX + EDI + 0x35], AL
				JMP sendPlayer

			checkForColorModification:
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x3
				CMP ESI, EAX
				JGE checkForHandicapModification
					
				SUB ESI, EAX
				ADD ESI, NEW_PLAYER_COUNT
				; GameAccess::game
				LOAD_STATIC_VALUE(EAX, 0x014aea2c)
				CMP byte ptr [EAX + 0x4A4], 0x0 ; is_ranked
				JNZ checkSlotTypeForColor

				LOAD_CONNECTION_DATA(EAX)
				MOV AL, byte ptr [EAX + 0x298]
				CMP AL, 0x8
				JZ updateColorValue
				CMP AL, 0x9
				JZ updateColorValue
				CMP AL, 0xA
				JZ updateColorValue
				CMP AL, 0xB
				JZ updateColorValue
				CMP AL, 0xC
				JZ updateColorValue

				checkSlotTypeForColor:
					XOR EDX, EDX
					LEA ECX, [EDI + 0x34]

				colorLoopCheckModification:
					MOV AL, byte ptr [ECX]
					TEST AL, AL
					JZ doFinalColorLoopChecks
					CMP AL, 0x1
					JZ doFinalColorLoopChecks
					CMP AL, 0x2
					JZ doFinalColorLoopChecks
					CMP AL, 0x3
					JZ doFinalColorLoopChecks
					CMP AL, 0x8
					JZ doFinalColorLoopChecks
					CMP AL, 0x9
					JNZ colorLoopEnd

				doFinalColorLoopChecks:
					MOV AL, byte ptr [ECX + 0x2]
					CMP AL, 0x8
					JZ colorLoopEnd
					MOVZX EAX, AL
					CMP EAX, dword ptr [EBP + 0xC]
					JZ sendPlayer

				colorLoopEnd:
					INC EDX
					ADD ECX, 0x3B
					CMP EDX, NEW_PLAYER_COUNT
					JL colorLoopCheckModification

				updateColorValue:
					MOV ECX, dword ptr [EBP + 0xC]
					IMUL EAX, ESI, 0x3B
					MOV byte ptr [EAX + EDI + 0x36], CL
					JMP sendPlayer

			checkForHandicapModification:
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x4
				CMP ESI, EAX
				JGE checkForDifficultyModification

				SUB ESI, EAX
				ADD ESI, NEW_PLAYER_COUNT
				MOV AL, byte ptr [EBP + 0xC]
				IMUL ECX, ESI, 0x3B
				MOV byte ptr [ECX + EDI + 0x38], AL
				JMP sendPlayer

			checkForDifficultyModification:
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x5
				CMP ESI, EAX
				JGE checkForTeamModification

				SUB ESI, EAX
				ADD ESI, NEW_PLAYER_COUNT
				MOV AL, byte ptr [EBP + 0xC]
				IMUL ECX, ESI, 0x3B
				MOV byte ptr [ECX + EDI + 0x39], AL
				JMP sendPlayer

			checkForTeamModification:
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x5
				SUB ESI, EAX
				MOV AL, byte ptr [EBP + 0xC]
				IMUL ECX, ESI, 0x3B
				MOV byte ptr [ECX + EDI + 0x37], AL

			sendPlayer:
				PUSH 0x0
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, 0x0100a220 ; ConnectionData::send_player
				CALL EAX

				MOV ECX, EBX
				MOV EAX, 0x00c83f20 ; SetupWin::update_lobby_num_players
				CALL EAX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8

			notAPlayerComboBox:
				MOV EAX, dword ptr [ECX]
				MOV EAX, dword ptr [EAX + 0x10]
				CALL EAX
				TEST AL, AL
				JZ returnImmediately

				MOV EBX, dword ptr [EBP + 0xC]
				MOV ECX, dword ptr [ESP + 0xC]
				PUSH 0x0
				PUSH 0x0
				PUSH EBX
				PUSH ESI
				MOV EAX, 0x00c75a70 ; SetupWin::process_combo_change
				CALL EAX

				LOAD_CONNECTION_DATA(EAX)
				MOV byte ptr [EAX + ESI + 0x268], BL ; TODO modification here for allocated_slots
				MOV EAX, 0x010097d0 ; ConnectionData::send_game
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8

			processComboChange:
				PUSH 0x0
				PUSH 0x0
				PUSH dword ptr [EBP + 0xC]
				MOV ECX, EBX
				PUSH dword ptr [EBP + 0x8]
				MOV EAX, 0x00c75a70 ; SetupWin::process_combo_change
				CALL EAX

				PUSH 0x0
				MOV ECX, EBX
				MOV EAX, 0x00c70d10 ; SetupWin::update_interface
				CALL EAX

			returnImmediately:
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8
		}
		INJECTION_CODE_END(SetupWinRequestComboChangeEnd)

		return InsertJumpWithData(start, end, 0x008772e0, "ExtendedSetupWin::RequestComboChange");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinValidateModStartConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinValidateModStartStart, SetupWinValidateModStartEnd)
		__asm
		{
			XOR ECX, ECX
			XOR EDI, EDI
			MOV dword ptr [EBP - 0x14], ECX

			loopBegin:
				; netsys
				LOAD_STATIC_VALUE(EAX, 0x0171c2e8)
				LEA EDX, [EBP - 0x2C]
				PUSH EDX
				MOV ECX, dword ptr [EAX + 0x28]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x24]

				LOAD_CONNECTION_DATA(ESI)
				ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD ESI, EDI
				CMP dword ptr [EAX + 0x14], 0x8
				JC skipIf

				MOV EDX, dword ptr [EAX]
				JMP continueRest

				skipIf:
					MOV EDX, EAX

				continueRest:
					CMP dword ptr [ESI + 0x14], 0x8
					JC skipIf2
					
					MOV ECX, dword ptr [ESI]
					JMP continueRest2

				skipIF2:
					MOV ECX, ESI

				continueRest2:
					MOV EBX, dword ptr [EAX + 0x10]
					MOV EAX, dword ptr [ESI + 0x10]
					CMP EAX, EBX
					MOV ESI, EBX
					MOV dword ptr [EBP - 0x10], EAX
					CMOVC ESI, EAX
					TEST ESI, ESI

				randomForLoop:
					JNZ forLoopContentHolyShitCompiler
					XOR ESI, ESI

				anotherRandomAsFuckLabel:
					TEST ESI, ESI
					JNZ iHaveNoIdeaWhatThisUnstructuredMessIs
					MOV EAX, dword ptr [EBP - 0x10]
					CMP EBX, EAX
					JBE somewhereElseThisIsAMess
					OR ESI, 0xFFFFFFFF
					JMP iHaveNoIdeaWhatThisUnstructuredMessIs

				forLoopContentHolyShitCompiler:
					MOV AX, word ptr [ECX]
					CMP AX, word ptr [EDX]
					JNZ breakForLoop
					ADD ECX, 0x2
					ADD EDX, 0x2
					SUB ESI, 0x1
					JMP randomForLoop

				breakForLoop:
					SBB ESI, ESI
					AND ESI, 0xFFFFFFFE
					INC ESI
					JMP anotherRandomAsFuckLabel

				somewhereElseThisIsAMess:
					SBB ESI, ESI
					NEG ESI

				iHaveNoIdeaWhatThisUnstructuredMessIs:
					MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
					MOV EAX, dword ptr [EBP - 0x18]
					CMP EAX, 0x8
					JC atLeastThisIsTheNextLabel

					PUSH 0x2
					INC EAX
					PUSH EAX
					PUSH dword ptr [EBP - 0x2C]
					MOV EAX, 0x00458340 ; std::_Deallocate
					CALL EAX
					ADD ESP, 0xC

				atLeastThisIsTheNextLabel:
					XOR EAX, EAX
					MOV dword ptr [EBP - 0x18], 0x7
					MOV dword ptr [EBP - 0x1C], 0x0
					MOV word ptr [EBP - 0x2C], AX
					TEST ESI, ESI
					JZ unreadyPlayer

					INC dword ptr [EBP - 0x14]
					ADD EDI, 0x3b
					CMP EDI, NEW_PLAYER_OBJECT_SIZE
					JL loopBegin

				breakBack:
					MOV ECX, 0x00c750cb
					JMP ECX

				unreadyPlayer:
					MOV ECX, dword ptr [EBP - 0x14]
					LOAD_CONNECTION_DATA(EDX)
					ADD EDX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					IMUL EAX, ECX, 0x3B
					CMP byte ptr [EAX + EDX + 0x3A], 0x0 ; is_ready
					JZ breakBack

					PUSH 0x0
					PUSH ECX
					MOV ECX, EDX
					MOV byte ptr [EAX + EDX + 0x3A], 0x0 ; is_ready = false
					SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					MOV EAX, 0x0100a220 ; ConnectionData::send_player
					CALL EAX
					JMP breakBack
		}
		INJECTION_CODE_END(SetupWinValidateModStartEnd)

		return InsertJumpWithData(start, end, 0x00874fbe, "ExtendedSetupWin::ValidateModStart");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupGameConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupGameFirstPatchStart, SetupGameFirstPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV EDX, dword ptr [EBP - 0x18]
				MOV EAX, dword ptr [EDX + EAX + 0x30]
				MOV dword ptr [EBX + ECX + 0x80], EAX
				MOV EAX, 0x00c708d2
				JMP EAX
			}
			INJECTION_CODE_END(SetupGameFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00870895, "ExtendedSetupWin::SetupGame"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameSecondPatchStart, SetupGameSecondPatchEnd)
			__asm
			{
				SUB ESP, 0x18
				LEA EAX, [ESI + ECX]
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x20], ESP
				XOR EDX, EDX
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				MOV EAX, 0x00c7057c
				JMP EAX
			}
			INJECTION_CODE_END(SetupGameSecondPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00870555, "ExtendedSetupWin::SetupGame"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameThirdPatchStart, SetupGameThirdPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EDX)
				ADD EDX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ESI, dword ptr [EBP - 0x18]
				ADD EDX, ESI
				CMP dword ptr [EDX + 0x14], 0x8
				MOV EAX, 0x00c70a0e
				JMP EAX
			}
			INJECTION_CODE_END(SetupGameThirdPatchEnd)

			if (!InsertJumpWithData(start, end, 0x008709ff, "ExtendedSetupWin::SetupGame"))
			{
				return false;
			}
		}

		INJECTION_CODE_BEGIN(SingleLineCmpPatchStart, SingleLineCmpPatchEnd)
		__asm
		{
			CMP ESI, NEW_PLAYER_OBJECT_SIZE
		}
		INJECTION_CODE_END(SingleLineCmpPatchEnd)

		return WriteBytesToData(start, end, _codeBase + 0x00870a4b);
	}

	INJECTION_FUNCTION bool Injector::InsertNewDownloadAllPlayerScoresConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(DownloadAllPlayerScoresStart, DownloadAllPlayerScoresEnd)
		__asm
		{
			loopBegin:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, ESI
				CMP byte ptr [EAX + 0x34], 0x0
				JNZ endOfLoop

				SUB ESP, 0x18
				XOR EDX, EDX
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x10], ESP
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX - 0x44], 0x7
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]
				MOV ECX, EAX
				TEST ECX, ECX
				JZ endOfLoop

				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x10]
				TEST EAX, EAX
				JNZ endOfLoop

				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				XOR ECX, ECX
				PUSH -0x1
				ADD EAX, 0x18
				MOV word ptr [EBP - 0x34], CX
				PUSH ECX
				ADD EAX, ESI
				MOV dword ptr [EBP - 0x24], 0x0
				MOV dword ptr [EBP - 0x20], 0x0
				LEA ECX, [EBP - 0x34]
				PUSH EAX
				INC EDI
				MOV dword ptr [EBP - 0x20], 0x7
				MOV dword ptr [EBP - 0x24], 0x0
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				LEA EAX, [EBP - 0x34]
				PUSH EAX
				LEA ECX, [EBP - 0x1C]
				MOV EAX, 0x004a5ec0 ; vector::push_back
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x20]
				CMP EAX, 0x8
				JC dontDeallocate
				INC EAX
				PUSH 0x2
				PUSH EAX
				PUSH dword ptr [EBP - 0x34]
				MOV EAX, 0x00458340 ; std::_Deallocate
				ADD ESP, 0xC

				dontDeallocate:
					XOR EAX, EAX
					MOV dword ptr [EBP - 0x20], 0x7
					MOV dword ptr [EBP - 0x24], 0x0
					MOV word ptr [EBP - 0x24], AX

				endOfLoop:
					ADD ESI, 0x3B
					CMP ESI, NEW_PLAYER_OBJECT_SIZE
					JL loopBegin

			MOV ESI, 0x00c6bbe5
			JMP ESI
		}
		INJECTION_CODE_END(DownloadAllPlayerScoresEnd)

		return InsertJumpWithData(start, end, 0x0086bb00, "ExtendedSetupWin::DownloadAllPlayerScores");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnFindLeaderboardConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(OnFindLeaderboardStart, OnFindLeaderboardEnd)
		__asm
		{
			loopBegin:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, ESI
				CMP byte ptr [EAX + 0x34], 0x0
				JNZ loopEnd

				SUB ESP, 0x18
				XOR EDX, EDX
				MOV ECX, ESP
				MOV dword ptr [EBP + 0x8], ESP
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]

				MOV ECX, EAX
				TEST ECX, ECX
				JZ loopEnd

				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x10]
				TEST EAX, EAX
				JNZ loopEnd

				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				PUSH EDI
				MOV dword ptr [ESI + ECX + 0x30], EAX
				SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV EAX, 0x0100a220 ; ConnectionData::send_player
				CALL EAX

				loopEnd:
					ADD ESI, 0x3B
					INC EDI
					CMP EDI, NEW_PLAYER_COUNT
					JL loopBegin

					MOV EAX, 0x00c7e6d9
					JMP EAX
		}
		INJECTION_CODE_END(OnFindLeaderboardEnd)

		return InsertJumpWithData(start, end, 0x0087e660, "ExtendedSetupWin::OnFindLeaderboard");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinCheckAllConnectedConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupWinCheckAllConnectedStart, SetupWinCheckAllConnectedEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ESI)
				ADD ESI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x10], ESP
				XOR EAX, EAX
				ADD ESI, EBX
				PUSH -0x1
				PUSH EAX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH ESI
				MOV word ptr [ECX], AX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				MOV EAX, 0x00c78f0c
				JMP EAX
			}
			INJECTION_CODE_END(SetupWinCheckAllConnectedEnd)

			if (!InsertJumpWithData(start, end, 0x00878ee0, "ExtendedSetupWin::CheckAllConnected"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineCmpPatchStart, SingleLineCmpPatchEnd)
		__asm
		{
			CMP EBX, NEW_PLAYER_OBJECT_SIZE
		}
		INJECTION_CODE_END(SingleLineCmpPatchEnd)
		
		return WriteBytesToData(start, end, _codeBase + 0x00878f8e);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinMakeComboChangeConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(MakeComboChangePlayerSlotStart, MakeComboChangePlayerSlotEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL ECX, ESI, 0x3B
				ADD EAX, ECX
				MOV byte ptr [EAX + 0x34], BL
				CMP EBX, 0x7
				MOV ECX, 0x00c76f0d
				JMP ECX
			}
			INJECTION_CODE_END(MakeComboChangePlayerSlotEnd)

			if (!InsertJumpWithData(start, end, 0x00876efd, "ExtendedSetupWin::MakeComboChange"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboChangeTribeStart, MakeComboChangeTribeEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESI, NEW_PLAYER_COUNT
				IMUL ECX, ESI, 0x3B
				PUSH 0x0
				PUSH ESI
				MOV byte ptr [ECX + EAX + 0x35], BL
				LOAD_CONNECTION_DATA(ECX)
				MOV EBX, 0x0100a220 ; ConnectionData::send_player
				CALL EBX

				MOV ECX, dword ptr [EBP - 0xC]
				MOV dword ptr FS:[0x0], ECX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8
			}
			INJECTION_CODE_END(MakeComboChangeTribeEnd)

			if (!InsertJumpWithData(start, end, 0x00876fea, "ExtendedSetupWin::MakeComboChange"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboChangeColorStart, MakeComboChangeColorEnd)
			__asm
			{
				MOV EDX, NEW_PLAYER_COUNT
				IMUL EDX, 0x2
				SUB ESI, EDX
				LOAD_CONNECTION_DATA(EDX)
				ADD EDX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL EAX, ESI, 0x3B
				MOV byte ptr [EAX + EDX + 0x36], BL
				MOV EAX, dword ptr [EDI + 0x2184]
				CMP dword ptr [EAX + 0x568], 0xA
				MOV ECX, 0x00c7703c
				JMP ECX
			}
			INJECTION_CODE_END(MakeComboChangeColorEnd)

			if (!InsertJumpWithData(start, end, 0x0087701f, "ExtendedSetupWin::MakeComboChange"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboChangeHandicapStart, MakeComboChangeHandicapEnd)
			__asm
			{
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x3
				SUB ESI, EAX
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL ECX, ESI, 0x3B
				PUSH 0x0
				PUSH ESI
				MOV byte ptr [ECX + EAX + 0x38], BL
				LOAD_CONNECTION_DATA(ECX)
				MOV EBX, 0x0100a220 ; ConnectionData::send_player
				CALL EBX

				MOV ECX, dword ptr [EBP - 0xC]
				MOV dword ptr FS:[0x0], ECX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8
			}
			INJECTION_CODE_END(MakeComboChangeHandicapEnd)

			if (!InsertJumpWithData(start, end, 0x008770e7, "ExtendedSetupWin::MakeComboChange"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboChangeDifficultyStart, MakeComboChangeDifficultyEnd)
			__asm
			{
				MOV EAX, NEW_PLAYER_COUNT
				IMUL EAX, 0x4
				SUB ESI, EAX
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL ECX, ESI, 0x3B
				PUSH 0x0
				PUSH ESI
				MOV byte ptr [ECX + EAX + 0x39], BL
				LOAD_CONNECTION_DATA(ECX)
				MOV EBX, 0x0100a220 ; ConnectionData::send_player
				CALL EBX

				MOV ECX, dword ptr [EBP - 0xC]
				MOV dword ptr FS:[0x0], ECX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8
			}
			INJECTION_CODE_END(MakeComboChangeDifficultyEnd)

			if (!InsertJumpWithData(start, end, 0x00877113, "ExtendedSetupWin::MakeComboChange"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboChangeTeamStart, MakeComboChangeTeamEnd)
		__asm
		{
			MOV EAX, NEW_PLAYER_COUNT
			IMUL EAX, 0x5
			SUB ESI, EAX
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			IMUL ECX, ESI, 0x3B
			MOV byte ptr [ECX + EAX + 0x37], BL
			LOAD_CONNECTION_DATA(ECX)
			PUSH 0x0
			PUSH ESI
			MOV EBX, 0x0100a220 ; ConnectionData::send_player
			CALL EBX

			MOV ECX, dword ptr [EBP - 0xC]
			MOV dword ptr FS:[0x0], ECX
			POP EDI
			POP ESI
			POP EBX
			MOV ESP, EBP
			POP EBP
			RET 0x8
		}
		INJECTION_CODE_END(MakeComboChangeTeamEnd)

		return InsertJumpWithData(start, end, 0x0087713e, "ExtendedSetupWin::MakeComboChange");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnComboRedrawConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(OnComboRedrawStart, OnComboRedrawEnd)
		__asm
		{
			loopBegin:
				CMP dword ptr [EDI + 0xC], EAX
				JNZ loopEnd

				CMP dword ptr [EDI + 0x568], 0x0
				JNZ loopEnd

				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x18], ESP
				XOR EDX, EDX
				ADD EAX, ESI
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]

				TEST EAX, EAX
				JZ enterIf

				MOV AL, byte ptr [EAX + 0x28]
				TEST AL, AL
				JNZ justBeforeLoopEnd

				enterIf:
					MOV EAX, [0x01535a58] ; Color GREY
					MOVQ XMM0, qword ptr [EAX]
					MOVQ qword ptr [EBX + 0x90], XMM0
					; GREY.flags
					LOAD_STATIC_VALUE(EAX, 0x01535a60)
					MOV dword ptr [EBX + 0x98], EAX
					MOV EAX, [0x017a7674] ; Color NONE
					MOVQ XMM0, qword ptr [EAX]
					MOVQ qword ptr [EBX + 0xCC], XMM0
					; GREY.flags
					LOAD_STATIC_VALUE(EAX, 0x017a767c)
					MOV dword ptr [EBX + 0xD4], EAX
					MOV byte ptr [EBX + 0x108], 0x1
					MOV byte ptr [EBX + 0x10D], 0x1

				justBeforeLoopEnd:
					MOV EAX, dword ptr [EBP + 0x8]

				loopEnd:
					INC EAX
					ADD ESI, 0x3B
					MOV dword ptr [EBP + 0x8], EAX
					CMP ESI, NEW_PLAYER_OBJECT_SIZE
					JL loopBegin

			MOV ESI, 0x00c80e6c
			JMP ESI
		}
		INJECTION_CODE_END(OnComboRedrawEnd)

		return InsertJumpWithData(start, end, 0x00880db0, "ExtendedSetupWin::OnComboRedraw");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateUiForRankedConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(UpdateUIForRankedFirstLoopStart, UpdateUIForRankedFirstLoopEnd)
			__asm
			{
				loopBegin:
					LOAD_CONNECTION_DATA(EAX)
					ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					SUB ESP, 0x18
					MOV ECX, ESP
					MOV dword ptr [EBP - 0x24], ESP
					XOR EDX, EDX
					ADD EAX, ESI
					PUSH -0x1
					PUSH EDX
					MOV dword ptr [ECX - 0x24], 0x7
					MOV dword ptr [ECX - 0x28], 0x0
					PUSH EAX
					MOV word ptr [ECX], DX
					MOV EAX, 0x0045a0a0 ; basic_string::assign
					CALL EAX

					; netsys
					LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x90]

					MOV ECX, EAX
					TEST ECX, ECX
					JZ loopEnd

					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x4]
					TEST EAX, EAX
					JNZ jumpAfterLoop

					loopEnd:
						ADD ESI, 0x3B
						INC EDI
						CMP ESI, NEW_PLAYER_OBJECT_SIZE
						JL loopBegin
						MOV EAX, 0x00c7ac94
						JMP EAX

				jumpAfterLoop:
					MOV EAX, 0x00c7ac84
					JMP EAX
			}
			INJECTION_CODE_END(UpdateUIForRankedFirstLoopEnd)

			if (!InsertJumpWithData(start, end, 0x0087ac20, "ExtendedSetupWin::UpdateUIForRanked"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateUIForRankedSecondLoopFirstPatchStart, UpdateUIForRankedSecondLoopFirstPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, EDI
				CMP byte ptr [EAX + 0x34], 0x0
				MOV ECX, 0x00c7acc2
				JMP ECX
			}
			INJECTION_CODE_END(UpdateUIForRankedSecondLoopFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087acb7, "ExtendedSetupWin::UpdateUIForRanked"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateUIForRankedSecondLoopSecondPatchStart, UpdateUIForRankedSecondLoopSecondPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV EBX, dword ptr [EBP - 0x20]
				MOV ECX, EBX
				PUSH dword ptr [EDI + EAX + 0x30]
				PUSH dword ptr [EBP - 0x10]
				MOV EAX, 0x00c7aaa0 ; SetupWin::update_ui_player_elo
				CALL EAX
				
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, dword ptr [EBP - 0x18]
				ADD ECX, dword ptr [EDI + EAX + 0x30]
				MOV dword ptr [EBP - 0x18], ECX
				MOV ECX, 0x00c7add3
				JMP ECX
			}
			INJECTION_CODE_END(UpdateUIForRankedSecondLoopSecondPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087ad70, "ExtendedSetupWin::UpdateUIForRanked"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateUIForRankedSecondLoopThirdPatchStart, UpdateUIForRankedSecondLoopThirdPatchEnd)
		__asm
		{
			CMP EDI, NEW_PLAYER_OBJECT_SIZE
		}
		INJECTION_CODE_END(UpdateUIForRankedSecondLoopThirdPatchEnd)
		
		return WriteBytesToData(start, end, _codeBase + 0x0087adf0);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnButtonClickedConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(OnButtonClickedStart, OnButtonClickedEnd)
		__asm
		{
			loopBegin:
				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				TEST ECX, ECX
				JZ netSysAndLocalPlayerNotSet

				MOV ECX, dword ptr [ECX + 0x28]
				JZ netSysAndLocalPlayerNotSet

				MOV EAX, dword ptr [ECX]
				LEA EDX, [EBP - 0x5C]
				PUSH EDX
				CALL dword ptr [EAX + 0x24]

				OR EBX, 0x1
				MOV dword ptr [EBP - 0x10], EBX
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				LEA ECX, [ESI + ECX]
				MOV EAX, 0x00466ee0 ; basic_string::compare
				CALL EAX

				MOV byte ptr [EBP + 0xB], 0x1
				TEST EAX, EAX
				JZ dontSetSomeBoolToFalse

				netSysAndLocalPlayerNotSet:
					MOV byte ptr [EBP + 0xB], 0x0

				dontSetSomeBoolToFalse:
					TEST BL, 0x1
					JZ dontDeallocateString
					
					AND EBX, 0xFFFFFFFE
					LEA ECX, [EBP - 0x5C]
					MOV dword ptr [EBP - 0x10], EBX
					MOV EAX, 0x00459c80 ; basic_string::~basic_string
					CALL EAX

				dontDeallocateString:
					CMP byte ptr [EBP + 0xB], 0x0
					JZ endOfLoop

					LOAD_CONNECTION_DATA(EAX)
					ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					CMP byte ptr [ESI + EAX + 0x3A], 0x0
					JZ endOfLoop

					MOV EAX, dword ptr [EDI]
					MOV ECX, EDI
					PUSH -0x1
					CALL dword ptr [EAX + 0x31C]

				endOfLoop:
					ADD ESI, 0x3B
					CMP ESI, NEW_PLAYER_OBJECT_SIZE
					JL loopBegin

			MOV ECX, 0x00c81cf6
			JMP ECX
		}
		INJECTION_CODE_END(OnButtonClickedEnd)

		return InsertJumpWithData(start, end, 0x00881c70, "ExtendedSetupWin::OnButtonClicked");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnDownloadScoresFunction()
	{
		INJECTION_CODE_BEGIN(OnDownloadScoresStart, OnDownloadScoresEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH -0x1
			PUSH 0x1194774
			MOV EAX, FS:[0x0]
			PUSH EAX
			MOV dword ptr FS:[0x0], ESP
			SUB ESP, 0x30
			PUSH EBX
			PUSH ESI
			PUSH EDI

			MOV dword ptr [EBP - 0x24], ECX
			CMP byte ptr [EBP + 0x14], 0x0
			JNZ LAB_00c7e5b2

			XOR ESI, ESI
			XOR EBX, EBX
			MOV dword ptr [EBP - 0x18], ESI
			MOV dword ptr [EBP + 0x14], EBX
			
			LAB_00c7e3e0:
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, EBX
				CMP byte ptr [EAX + 0x34], 0x0
				JNZ LAB_00c7e58e

				SUB ESP, 0x18
				XOR EDX, EDX
				MOV ECX, ESP
				MOV dword ptr [EBP - 0x20], ESP
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]
				MOV ECX, EAX
				TEST ECX, ECX
				JZ LAB_00c7e58e

				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x10]
				TEST EAX, EAX
				JNZ LAB_00c7e58e

				MOV ESI, dword ptr [EBP + 0xC]
				MOV dword ptr [EBP - 0x14], 0x640
				MOV EAX, dword ptr [ESI]
				MOV dword ptr [EBP - 0x10], EAX
				CMP EAX, ESI
				JZ LAB_00c7e576

				ADD EBX, 0x18
				MOV dword ptr [EBP - 0x1C], EBX

				LAB_00c7e460:
					XOR ECX, ECX
					MOV dword ptr [EBP - 0x2C], 0x0
					PUSH -0x1
					ADD EAX, 0x10
					MOV word ptr [EBP - 0x3C], CX
					PUSH ECX
					MOV dword ptr [EBP - 0x28], 0x0
					LEA ECX, [EBP - 0x3C]
					PUSH EAX
					MOV dword ptr [EBP - 0x20], EAX
					MOV dword ptr [EBP - 0x28], 0x7
					MOV dword ptr [EBP - 0x2C], 0x0
					MOV EAX, 0x0045a0a0 ; basic_string::assign
					CALL EAX

					LOAD_CONNECTION_DATA(ECX)
					ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					ADD ECX, EBX
					CMP dword ptr [ECX + 0x14], 0x8
					JC LAB_00c7e4aa

					MOV EAX, dword ptr [ECX]
					JMP LAB_00c7e4ac

					LAB_00c7e4aa:
						MOV EAX, ECX

					LAB_00c7e4ac:
						CMP dword ptr [EBP - 0x28], 0x8
						LEA EDX, [EBP - 0x3C]
						MOV EBX, dword ptr [ECX + 0x10]
						MOV EDI, EBX
						CMOVNC EDX, dword ptr [EBP - 0x3C]
						CMP dword ptr [EBP - 0x2C], EBX
						CMOVC EDI, dword ptr [EBP - 0x2C]
						TEST EDI, EDI

					LAB_00c7e4c5:
						JNZ LAB_00c7e520
						XOR EAX, EAX

					LAB_00c7e4c9:
						TEST EAX, EAX
						JNZ LAB_00c7e4d6
						MOV EAX, dword ptr [EBP - 0x2C]
						CMP EAX, EBX
						JC LAB_00c7e4d6
						JBE LAB_00c7e53b

					LAB_00c7e4d6:
						MOV EAX, dword ptr [EBP - 0x28]
						CMP EAX, 0x8
						JC LAB_00c7e4f1
						INC EAX
						PUSH 0x2
						PUSH EAX
						PUSH dword ptr [EBP - 0x3C]
						MOV EAX, 0x00458340 ; std::_Deallocate
						CALL EAX
						
						ADD ESP, 0xC

					LAB_00c7e4f1:
						XOR EAX, EAX
						MOV dword ptr [EBP - 0x28], 0x7
						LEA ECX, [EBP - 0x10]
						MOV dword ptr [EBP - 0x2C], 0x0
						MOV word ptr [EBP - 0x3C], AX
						MOV EAX, 0x00461ff0 ; tree_iterator::operator++
						CALL EAX

						MOV EAX, dword ptr [EBP - 0x10]
						CMP EAX, ESI
						JZ LAB_00c7e573
						JMP LAB_00c7e460

					LAB_00c7e520:
						MOV CX, word ptr [EDX]
						CMP CX, word ptr [EAX]
						JNZ LAB_00c7e533

						ADD EDX, 0x2
						ADD EAX, 0x2
						SUB EDI, 0x1
						JMP LAB_00c7e4c5

					LAB_00c7e533:
						SBB EAX, EAX
						AND EAX, 0xFFFFFFFE
						INC EAX
						JMP LAB_00c7e4c9

					LAB_00c7e53b:
						MOV EAX, dword ptr [EBP - 0x20]
						MOV EAX, dword ptr [EAX + 0x18]
						MOV dword ptr [EBP - 0x14], EAX
						MOV EAX, dword ptr [EBP - 0x28]
						CMP EAX, 0x8
						JC LAB_00c7e55f

						INC EAX
						PUSH 0x2
						PUSH EAX
						PUSH dword ptr [EBP - 0x3C]
						MOV EAX, 0x00458340 ; std::_Deallocate
						CALL EAX
						ADD ESP, 0xC

					LAB_00c7e55f:
						XOR EAX, EAX
						MOV dword ptr [EBP - 0x28], 0x7
						MOV dword ptr [EBP - 0x2C], 0x0
						MOV word ptr [EBP - 0x3C], AX

					LAB_00c7e573:
						MOV EBX, dword ptr [EBP + 0x14]

					LAB_00c7e576:
						LOAD_CONNECTION_DATA(ECX)
						ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
						MOV EAX, dword ptr [EBP - 0x14]
						MOV ESI, dword ptr [EBP - 0x18]
						PUSH 0x0
						PUSH ESI
						MOV dword ptr [EBX + ECX + 0x30], EAX
						SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
						MOV EAX, 0x0100a220 ; ConnectionData::send_player
						CALL EAX

				LAB_00c7e58e:
					ADD EBX, 0x3B
					INC ESI
					MOV dword ptr [EBP - 0x18], ESI
					MOV dword ptr [EBP + 0x14], EBX
					CMP EBX, NEW_PLAYER_OBJECT_SIZE
					JL LAB_00c7e3e0

					MOV ECX, dword ptr [EBP - 0x24]
					ADD ECX, 0xFFFFFb58
					MOV EAX, 0x00c7ab90 ; SetupWin::update_ui_for_ranked
					CALL EAX

			LAB_00c7e5b2:
				LEA ECX, [EBP + 0xC]
				MOV EAX, dword ptr [EBP + 0xC]
				PUSH EAX
				PUSH dword ptr [EAX]
				LEA EAX, [EBP + 0x14]
				PUSH EAX
				MOV EAX, 0x00461a40 ; _Tree::erase
				CALL EAX

				MOV EAX, dword ptr [EBP + 0xC]
				TEST EAX, EAX
				JZ LAB_00c7e5e3

				PUSH EAX
				MOV EAX, 0x011d58cc
				CALL dword ptr [EAX]

				ADD ESP, 0x4

			LAB_00c7e5e3:
				MOV ECX, dword ptr [EBP - 0xC]
				POP EDI
				POP ESI
				MOV dword ptr FS:[0x0], ECX
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x10
		}
		INJECTION_CODE_END(OnDownloadScoresEnd)

		return InsertJumpWithData(start, end, 0x0087e3a0, "ExtendedSetupWin::OnDownloadScores");
	}

	INJECTION_FUNCTION bool Injector::InsertNewConnectionDataDeletePlayerHandling()
	{
		INJECTION_CODE_BEGIN(DeletePlayerStart, DeletePlayerEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EDI)
			ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN

			LAB_0100b4c1:
				CMP dword ptr [EBP + 0x1C], 0x8
				LEA ECX, [EBP + 0x8]
				CMOVNC ECX, dword ptr [EBP + 0x8]
				CMP dword ptr [EDI + 0x14], 0x8
				JC LAB_0100b4d6

				MOV EDX, dword ptr [EDI]
				JMP LAB_0100b4d8

				LAB_0100b4d6:
					MOV EDX, EDI

				LAB_0100b4d8:
					MOV EBX, dword ptr [EDI + 0x10]
					MOV ESI, dword ptr [EBP + 0x18]
					CMP EBX, ESI
					CMOVC ESI, EBX
					TEST ESI, ESI

				LAB_0100b4e5:
					JNZ LAB_0100b5c8
					XOR EAX, EAX

				LAB_0100b4ed:
					TEST EAX, EAX
					JNZ LAB_0100b4fe
					MOV EAX, dword ptr [EBP + 0x18]
					CMP EBX, EAX
					JC LAB_0100b4fe
					JBE LAB_0100b5ef

				LAB_0100b4fe:
					MOV EBX, dword ptr [EBP - 0x18]
					LEA EAX, [EBP + 0x8]
					PUSH EAX
					MOV ECX, EBX
					MOV EAX, 0x00466ee0 ; basic_string::compare
					CALL EAX
					TEST EAX, EAX
					JZ LAB_0100b5f2

					MOV DL, byte ptr [EBP - 0xD]

				LAB_0100b517:
					MOV EAX, dword ptr [EBP - 0x14]
					ADD EBX, 0x18
					INC EAX
					MOV dword ptr [EBP - 0x18], EBX
					ADD EDI, 0x3B
					MOV dword ptr [EBP - 0x14], EAX
					CMP EAX, NEW_PLAYER_COUNT
					JL LAB_0100b4c1

					MOV EAX, 0x0100b52c
					JMP EAX

			LAB_0100b5c8:
				MOV AX, word ptr [EDX]
				CMP AX, word ptr [ECX]
				JNZ LAB_0100b5de

				ADD EDX, 0x2
				ADD ECX, 0x2
				SUB ESI, 0x1
				JMP LAB_0100b4e5

			LAB_0100b5de:
				MOVZX EAX, word ptr [EDX]
				CMP AX, word ptr [ECX]
				SBB EAX, EAX
				AND EAX, 0xFFFFFFFE
				INC EAX
				JMP LAB_0100b4ed

			LAB_0100b5ef:
				MOV EBX, dword ptr [EBP - 0x18]

			LAB_0100b5f2:
				PUSH 0x0
				PUSH 0x131d094
				MOV ECX, EBX
				MOV EAX, 0x00459fa0 ; basic_string::assign
				CALL EAX

				PUSH 0x3B
				PUSH 0x0
				PUSH EDI
				MOV EAX, 0x00c1b270 ; _memset
				CALL EAX

				MOV ECX, dword ptr [EBP - 0x1C]
				ADD ESP, 0xC
				MOV byte ptr [EDI + 0x34], 0x4
				PUSH 0x1
				PUSH dword ptr [EBP - 0x14]
				MOV EAX, 0x0100a220 ; ConnectionData::send_player
				CALL EAX

				MOV DL, 0x1
				MOV byte ptr [EBP - 0xD], DL
				JMP LAB_0100b517
		}
		INJECTION_CODE_END(DeletePlayerEnd)

		return InsertJumpWithData(start, end, 0x00c0b4c1, "ExtendedConnectionData::DeletePlayer");
	}

	INJECTION_FUNCTION bool Injector::InsertNewProcessComboChangeConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(ProcessComboChangeFirstPatchStart, ProcessComboChangeFirstPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD EAX, ECX
				MOV ECX, ESP
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				MOV EAX, 0x00462a70 ; basic_string::basic_string()
				CALL EAX

				MOV ECX, 0x00c75c1f
				JMP ECX
			}
			INJECTION_CODE_END(ProcessComboChangeFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00875c13, "ExtendedSetupWin::ProcessComboChange", JumpType::USE_ECX))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSecondPatchStart, ProcessComboChangeSecondPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD EAX, ECX
				MOV ECX, ESP
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				MOV EAX, 0x00462a70 ; basic_string::basic_string()
				CALL EAX

				MOV ECX, 0x00c75cae
				JMP ECX
			}
			INJECTION_CODE_END(ProcessComboChangeSecondPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00875ca2, "ExtendedSetupWin::ProcessComboChange", JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeThirdPatchStart, ProcessComboChangeThirdPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				IMUL ECX, EBX, 0x3B
				MOV dword ptr [EBP - 0x14], ESP
				ADD EAX, ECX
				MOV dword ptr [EBP + 0x10], ECX
				MOV ECX, ESP
				PUSH EAX
				MOV EAX, 0x00462a70 ; basic_string::basic_string
				CALL EAX

				; netsys
				LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x90]

				TEST EAX, EAX
				JZ jumpBack

				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, dword ptr [EBP + 0x10]
				MOV byte ptr [ECX + EAX + 0x34], 0x0

				MOV ECX, dword ptr [EBP - 0xC]
				MOV dword ptr FS:[0x0], ECX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x10

				jumpBack:
					MOV EAX, 0x00c75d62
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeThirdPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00875d07, "ExtendedSetupWin::ProcessComboChange"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFourthPatchStart, ProcessComboChangeFourthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD EAX, ECX
				MOV ECX, ESP
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				MOV EAX, 0x00462a70 ; basic_string::basic_string()
				CALL EAX

				MOV ECX, 0x00c75d89
				JMP ECX
			}
			INJECTION_CODE_END(ProcessComboChangeFourthPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00875d7d, "ExtendedSetupWin::ProcessComboChange", JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFifthPatchStart, ProcessComboChangeFifthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH 0x0
				PUSH 0x0
				MOV byte ptr [ECX + 0x37], 0x0
				SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV EBX, 0x0100a220 ; ConnectionData::send_player
				CALL EBX

				MOV EBX, 0x00c76465
				JMP EBX
			}
			INJECTION_CODE_END(ProcessComboChangeFifthPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00876452, "ExtendedSetupWin::ProcessComboChange", JumpType::USE_ECX))
			{
				return false;
			}
		}
		
		INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSixthPatchStart, ProcessComboChangeSixthPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(ECX)
			ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			PUSH 0x0
			PUSH 0x0
			MOV byte ptr [ECX + 0x37], 0x0
			SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			MOV EBX, 0x0100a220 ; ConnectionData::send_player
			CALL EBX

			MOV EBX, 0x00c76402
			JMP EBX
		}
		INJECTION_CODE_END(ProcessComboChangeSixthPatchEnd)

		return InsertJumpWithData(start, end, 0x008763ef, "ExtendedSetupWin::ProcessComboChange", JumpType::USE_ECX);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateInterfaceConnectionDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(UpdateInterfaceFinalSlotTypeOffsetStuffStart, UpdateInterfaceFinalSlotTypeOffsetStuffEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				XOR EAX, EAX

				; We want to loop over all slots here and check if it is either a player or open
				; to count the number of players in this lobby. In the game, this is hardcoded as
				; eight separate checks, but no

				PUSH EBX ; push ebx to preserve its current value (hopefully)
				XOR EBX, EBX

				loopBegin:
					MOV DL, byte ptr [ECX + EBX + 0x34] ; check slot_type at player_data_server[i]
					TEST DL, DL
					JZ isPlayerSlot

					CMP DL, 0x4
					JNZ loopEnd ; slot is not open

					isPlayerSlot:
						INC EAX

					loopEnd:
						ADD EBX, 0x3B
						CMP EBX, NEW_PLAYER_OBJECT_SIZE
						JL loopBegin

				POP EBX ; pop ebx to restore its value
				MOV ECX, 0x00c73901
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceFinalSlotTypeOffsetStuffEnd)

			if (!InsertJumpWithData(start, end, 0x0087387b, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceSecondPatchStart, UpdateInterfaceSecondPatchEnd)
			__asm
			{
				LAB_00c73794:
					MOV ECX, dword ptr [ECX + 0x28]
					LEA EDX, [EBP + 0xFFFFFF70]
					PUSH EDX
					MOV EAX, dword ptr [ECX]
					MOV EAX, dword ptr [EAX + 0x24]
					CALL EAX

					LOAD_CONNECTION_DATA(EDI)
					ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					PUSH EAX
					LEA ECX, [EDI + ESI]
					MOV EAX, 0x00466ee0 ; basic_string::compare
					CALL EAX

					MOV dword ptr [EBP - 0x28], EAX
					MOV ECX, dword ptr [EBP - 0x7C]
					CMP ECX, 0x8
					JC LAB_00c737e8

					PUSH 0x2
					INC ECX
					PUSH ECX
					PUSH dword ptr [EBP + 0xFFFFFF70]
					MOV EDI, 0x00458340 ; std::_Deallocate
					CALL EDI

					LOAD_CONNECTION_DATA(EDI)
					ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					ADD ESP, 0xC
					MOV EAX, dword ptr [EBP - 0x28]

					LAB_00c737e8:
						XOR ECX, ECX
						MOV dword ptr [EBP - 0x7C], 0x7
						MOV dword ptr [EBP - 0x80], 0x0
						MOV word ptr [EBP + 0xFFFFFF70], CX
						TEST EAX, EAX
						JZ LAB_00c7381c

						INC dword ptr [EBP + 0x8]

						ADD ESI, 0x3B
						CMP ESI, NEW_PLAYER_OBJECT_SIZE
						JGE LAB_00c73841

						; netsys
						LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
						JMP LAB_00c73794

					LAB_00c7381c:
						MOV EDX, dword ptr [EBP + 0x8]
						IMUL ECX, EDX, 0x3B
						CMP byte ptr [EDI + ECX + 0x3A], 0x0
						JNZ LAB_00c73841

						MOV byte ptr [EDI + ECX + 0x3A], 0x1
						MOV AL, [0x017a0fa0]
						PUSH 0x0
						MOV byte ptr [EDI + ECX + 0x35], AL
						LOAD_CONNECTION_DATA(ECX)
						PUSH EDX
						MOV EAX, 0x0100a220 ; ConnectionData::send_player
						CALL EAX

				LAB_00c73841:
					MOV EAX, 0x00c73841
					JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceSecondPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00873794, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceThirdPatchStart, UpdateInterfaceThirdPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, ESI
				MOVZX EDI, byte ptr [EAX + 0x3A]
				CMP EDI, 0x1
				MOV EAX, 0x00c73606
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceThirdPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x008735f8, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceFourthPatchStart, UpdateInterfaceFourthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				MOV EAX, [ECX + CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN]
				MOV byte ptr [EAX + 0x3A], 0x1
				PUSH 0x0
				PUSH 0x0
				MOV EAX, 0x0100a220 ; ConnectionData::send_player
				CALL EAX

				MOV EAX, 0x00c73752
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceFourthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x0087373f, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceFifthPatchStart, UpdateInterfaceFifthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				SUB ESP, 0x18
				ADD EAX, ECX
				MOV dword ptr [EBP + -0x28], ESP
				MOV ECX, ESP
				PUSH -0x1
				PUSH EDX
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], EDX
				PUSH EAX
				MOV word ptr [ECX], DX
				MOV EAX, 0x0045a0a0 ; basic_string::assign
				CALL EAX

				MOV EAX, 0x00c73381
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceFifthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x0087335c, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceSixthPatchStart, UpdateInterfaceSixthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, dword ptr [EBP + 0x8]
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH EAX
				MOV EAX, 0x00466ee0 ; basic_string::compare
				CALL EAX

				MOV ECX, 0x00c73412
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceSixthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00873403, "ExtendedSetupWin::UpdateInterface", JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceSeventhPatchStart, UpdateInterfaceSeventhPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				MOV ECX, dword ptr [EBP + 0x8]
				MOV EDX, dword ptr [EDI]
				CMP byte ptr [ECX + EAX + 0x3A], 0x0

				MOV ECX, 0x00c73470
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceSeventhPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00873461, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineFirstCmpPatchStart, SingleLineFirstCmpPatchEnd)
			__asm
			{
				CMP ECX, NEW_PLAYER_OBJECT_SIZE
			}
			INJECTION_CODE_END(SingleLineFirstCmpPatchEnd)
		
			if (!WriteBytesToData(start, end, _codeBase + 0x008734a9))
			{
				std::cerr << "Could not write CMP statement in ExtendedSetupWin::UpdateInterface" << std::endl;
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceEighthPatchStart, UpdateInterfaceEighthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				CMP byte ptr [EAX + 0x3A], 0x0
				MOV EAX, 0x00c731f5
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceEighthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x008731ec, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceNinthPatchStart, UpdateInterfaceNinthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				CMP byte ptr [EAX + 0x3A], 0x0
				MOV ECX, 0x00c72dae
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceNinthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00872da5, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceEleventhPatchStart, UpdateInterfaceEleventhPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				CMP byte ptr [EAX + 0x3A], 0x0
				MOV ECX, 0x00c7230a
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceEleventhPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00872301, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceTwelfthPatchStart, UpdateInterfaceTwelfthPatchEnd)
			__asm
			{
				MOV dword ptr [ECX + 0x14], 0x7
				MOV dword ptr [ECX + 0x10], 0x0
				PUSH EDX

				LOAD_CONNECTION_DATA(EDX)
				ADD EDX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				IMUL EAX, EDI, 0x3B
				ADD EDX, EAX
				PUSH EDX
				XOR EDX, EDX

				MOV EAX, 0x00c7141c
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceTwelfthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00871406, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineSecondCmpPatchStart, SingleLineSecondCmpPatchEnd)
			__asm
			{
				CMP EDI, NEW_PLAYER_COUNT
			}
			INJECTION_CODE_END(SingleLineSecondCmpPatchEnd)
		
			if (!WriteBytesToData(start, end, _codeBase + 0x008722dc))
			{
				std::cerr << "Could not write CMP statement in ExtendedSetupWin::UpdateInterface" << std::endl;
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceThirteenthPatchStart, UpdateInterfaceThirteenthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				CMP byte ptr [EAX + 0x3A], 0x0
				MOV EAX, 0x00c72198
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceThirteenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x0087218f, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceFourteenthPatchStart, UpdateInterfaceFourteenthPatchEnd)
			__asm
			{
				PUSH EAX
				LOAD_CONNECTION_DATA(ECX)
				ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				LEA ECX, [EDI + ECX]
				MOV EAX, 0x00c72069
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceFourteenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x0087205f, "ExtendedSetupWin::UpdateInterface", JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineThirdCmpPatchStart, SingleLineThirdCmpPatchEnd)
			__asm
			{
				CMP EDI, NEW_PLAYER_OBJECT_SIZE
			}
			INJECTION_CODE_END(SingleLineThirdCmpPatchEnd)
		
			if (!WriteBytesToData(start, end, _codeBase + 0x008720dc))
			{
				std::cerr << "Could not write CMP statement in ExtendedSetupWin::UpdateInterface" << std::endl;
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceFivteenthPatchStart, UpdateInterfaceFivteenthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EAX)
				ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				ADD EAX, ECX
				MOV ECX, EAX
				MOV dword ptr [EBP + 0x8], EAX
				MOV EAX, 0x00c71f38
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceFivteenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00871f2c, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceSixteenthPatchStart, UpdateInterfaceSixteenthPatchEnd)
			__asm
			{
				LOAD_CONNECTION_DATA(EDI)
				ADD EDI, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
				PUSH dword ptr [EBP + 0x8]
				ADD EDI, EDX
				PUSH 0x0
				MOV EAX, 0x00c712de
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceSixteenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x008712d1, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceSeventeenthPatchStart, UpdateInterfaceSeventeenthPatchEnd)
			__asm
			{
				LAB_00c71290:
					LOAD_CONNECTION_DATA(EAX)
					ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					CMP byte ptr [EDI + EAX + 0x37], 0x8
					JZ LAB_00c712ac

					LEA EAX, [ECX + 0x28]
					MOV ECX, ESI
					PUSH 0x8
					PUSH EAX
					MOV EAX, 0x00c772e0 ; SetupWin::request_combo_change
					CALL EAX

					MOV ECX, dword ptr [EBP - 0x1C]

					LAB_00c712ac:
						INC ECX
						ADD EDI, 0x3B
						MOV dword ptr [EBP - 0x1C], ECX
						CMP EDI, NEW_PLAYER_OBJECT_SIZE
						JL LAB_00c71290

				MOV ECX, 0x00c712bb
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceSeventeenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00871290, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceEighteenthPatchStart, UpdateInterfaceEighteenthPatchEnd)
			__asm
			{
				LAB_00c71240:
					LOAD_CONNECTION_DATA(EAX)
					ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					MOV AL, byte ptr [EDI + EAX + 0x37]
					CMP AL, 0x8
					JZ LAB_00c71259

					CMP AL, 0x5
					JZ LAB_00c71259

					CMP AL, 0x2
					JZ LAB_00c71259

					CMP AL, 0x3
					JNZ LAB_00c7126f

					LAB_00c71259:
						XOR EAX, EAX
						TEST ECX, ECX
						SETNZ AL
						PUSH EAX
						LEA EAX, [ECX + 0x28]
						MOV ECX, ESI
						PUSH EAX
						MOV EAX, 0x00c772e0
						CALL EAX
						MOV ECX, dword ptr [EBP - 0x1C]

					LAB_00c7126f:
						INC ECX
						ADD EDI, 0x3B
						MOV dword ptr [EBP - 0x1C], ECX
						CMP EDI, NEW_PLAYER_OBJECT_SIZE
						JL LAB_00c71240

						MOV ECX, 0x00c712bb
						JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceEighteenthPatchEnd)
				
			if (!InsertJumpWithData(start, end, 0x00871240, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceNineteenthPatchStart, UpdateInterfaceNineteenthPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
			XOR EDX, EDX
			MOV AL, byte ptr [EAX + 0x3A]
			TEST AL, AL
			SETZ DL
			TEST AL, AL
			JZ LAB_00c71033
			
			AND dword ptr [ECX + 0x314], 0xFFFFFFFE

			LAB_00c71033:
				PUSH EDX
				MOV EDI, 0x0110fb60 ; Window::set_enabled
				CALL EDI

				MOV EDI, 0x00c71039
				JMP EDI
		}
		INJECTION_CODE_END(UpdateInterfaceNineteenthPatchEnd)
				
		return InsertJumpWithData(start, end, 0x00871019, "ExtendedSetupWin::UpdateInterface");
	}

	INJECTION_FUNCTION bool Injector::InsertNewNetSysReceiveDataConnectionDataHandling()
	{
		INJECTION_CODE_BEGIN(NetSysReceiveDataStart, NetSysReceiveDataEnd)
		__asm
		{
			MOV EAX, dword ptr [ECX]
			LEA EDX, [EBP - 0x10]
			PUSH EDX                 ; NewValue
			LEA EDX, [EBX + CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN]
			PUSH EDX
			LEA EDX, [EBX + CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN]
			PUSH EDX
			CALL dword ptr [EAX + 0x8]
			MOV ECX, 0x004f5b1a
			JMP ECX
		}
		INJECTION_CODE_END(NetSysReceiveDataEnd)

		return InsertJumpWithData(start, end, 0x000f5b09, "NetSys::ReceiveData");
	}

#pragma endregion INJECTION_CODE

	bool Injector::HandleConnectionData()
	{
		return 
			InsertNewConnectionDataConstructor()
				&& InsertNewConnectionDataCloseFunction()
				&& InsertNewConnectionDataDestructor()
				&& InsertNewConnectionDataUnreadyAllPlayers()
				&& InsertNewConnectionDataGetPlayerIndex()
				&& InsertNewConnectionDataInitFunction()
				&& InsertNewRandomLambdaConnectionDataHandling()
				&& InsertNewRandomBoolLambdaConnectionDataHandling()
				&& InsertNewRandomIntLambdaConnectionDataHandling()
				&& InsertNewLambdaDataHandlingForStartGame()
				&& InsertNewCheckForUwpPlayersConnectionDataHandling()
				&& InsertNewCheckAllReadyConnectionDataHandling()
				&& InsertNewValidateStartConnectionDataHandling()
				&& InjectSetupWinAddChatConnectionDataHandling()
				&& InjectSetupWinUpdateLobbyNumPlayersConnectionDataHandling()
				&& InsertNewSetupWinOnCheckClickedFunction()
				&& InjectSetupWinStartGameSuccessModifications()
				&& InsertNewSetupWinOnTimerConnectionDataHandling()
				&& InsertNewSendPlayerConnectionDataHandling()
				&& InsertNewAddPlayerConnectionDataHandling()
				&& InsertNewSetupWinRequestComboChangeConnectionDataHandling()
				&& InsertNewSetupWinValidateModStartConnectionDataHandling()
				&& InsertNewSetupWinSetupGameConnectionDataHandling()
				// crashes on lobby create if the lobby is ranked
				// && InsertNewDownloadAllPlayerScoresConnectionDataHandling()
				&& InsertNewSetupWinOnFindLeaderboardConnectionDataHandling()
				&& InsertNewSetupWinCheckAllConnectedConnectionDataHandling()
				&& InsertNewSetupWinMakeComboChangeConnectionDataHandling()
				&& InsertNewSetupWinOnComboRedrawConnectionDataHandling()
				// crashes the game
				// && InsertNewSetupWinUpdateUiForRankedConnectionDataHandling()
				&& InsertNewSetupWinOnButtonClickedConnectionDataHandling()
				&& InsertNewSetupWinOnDownloadScoresFunction()
				&& InsertNewConnectionDataDeletePlayerHandling()
				&& InsertNewProcessComboChangeConnectionDataHandling()
				&& InsertNewSetupWinUpdateInterfaceConnectionDataHandling()
				&& InsertNewNetSysReceiveDataConnectionDataHandling();
	}
}
