#pragma once

#include <iostream>
#include <string>
#include <Windows.h>
#include <winreg.h>

#include "DataSetup.h"
#include "ProcessManagement.h"
#include "resource.h"
#include "InjectionBase.h"

#define START_GAME true

constexpr auto MOD_EXECUTABLE_NAME = "mpexpanded.exe";
constexpr auto STEAM_REGPATH = "SOFTWARE\\Valve\\Steam";
constexpr auto SECTION_NAME = ".modtext";
constexpr auto IMAGE_IS_64_BIT = -3L;
constexpr auto INVALID_DOS_FILE = -4L;
constexpr auto FILE_UNREADABLE = -5L;
constexpr auto FILE_SIZE_READ_FAILED = -6L;
constexpr auto FILE_OPEN_FAILED = -7L;

namespace
{
	DWORD Align(DWORD size, DWORD align, DWORD address)
	{
		if (!(size % align))
		{
			return address + size;
		}

		return address + (size / align + 1) * align;
	}

	int FindSteamGameDirectory(std::string* gamePath)
	{
		HKEY key;
		LONG errorCode = RegOpenKeyExA(HKEY_LOCAL_MACHINE, STEAM_REGPATH, 0, KEY_READ | KEY_WOW64_64KEY, &key);
		if (errorCode != ERROR_SUCCESS)
		{
			errorCode = RegOpenKeyExA(HKEY_LOCAL_MACHINE, STEAM_REGPATH, 0, KEY_READ | KEY_WOW64_32KEY, &key);
			if (errorCode != ERROR_SUCCESS)
			{
				return errorCode;
			}
		}

		char buffer[512]{ 0 };
		DWORD size = sizeof(buffer);
		errorCode = RegQueryValueExA(key, "InstallPath", 0, 0, (LPBYTE)&buffer, &size);
		if (errorCode != ERROR_SUCCESS)
		{
			RegCloseKey(key);
			return errorCode;
		}

		RegCloseKey(key);
		*gamePath = std::string(buffer);
		*gamePath += "\\steamapps\\common\\Rise of Nations\\";
		return ERROR_SUCCESS;
	}

	bool CopyRequiredFile(std::string baseFilePath, int fileName, int fileType)
	{
		auto backupFilePath = baseFilePath + ".orig";
		if (!MoveFileA(baseFilePath.c_str(), backupFilePath.c_str()))
		{
			int code = GetLastError();
			if (code != ERROR_ALREADY_EXISTS)
			{
				std::cerr << "Could not move file to backup: " << GetLastError() << std::endl;;
				return false;
			}
		}

		std::string data;
		auto fileContent = MPExpanded::GetResourceAsString(fileName, fileType, &data);

		DeleteFileA(baseFilePath.c_str());
		HANDLE requiredFile = CreateFileA(baseFilePath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
		if (requiredFile == INVALID_HANDLE_VALUE)
		{
			std::cerr << "Could not create code file " << fileName << std::endl;
			return false;
		}

		SetFilePointer(requiredFile, data.size(), NULL, FILE_BEGIN);
		SetEndOfFile(requiredFile);
		SetFilePointer(requiredFile, 0, NULL, FILE_BEGIN);

		DWORD writeCount;
		WriteFile(requiredFile, data.c_str(), data.size(), &writeCount, NULL);
		if (writeCount != data.size())
		{
			std::cerr << "Write data length does not match content" << std::endl;
		}

		CloseHandle(requiredFile);
		return writeCount == data.size();
	}

	bool CleanupRequiredFile(std::string basePath)
	{
		DeleteFileA(basePath.c_str());
		return MoveFileA((basePath + ".orig").c_str(), basePath.c_str());
	}
} // namespace

namespace MPExpanded
{
	const std::string& GetSteamGamePath()
	{
		static std::string gamePath = "";
		if (gamePath != "")
		{
			return gamePath;
		}
		
		int code = FindSteamGameDirectory(&gamePath);
		if (code != ERROR_SUCCESS)
		{
			std::cerr << "Could not find steam game path: " << code << std::endl;
			gamePath = "";
			return gamePath;
		}

		return gamePath;
	}

	void CopyRequiredFiles()
	{
		auto& basePath = GetSteamGamePath();
		CopyRequiredFile(basePath + "Data\\setupwin.xml", SETUPWIN_XML, XMLFILE);
	}

	void CleanupRequiredFiles()
	{
		auto& basePath = GetSteamGamePath();
		CleanupRequiredFile(basePath + "Data\\setupwin.xml");
	}

	int AddPEModDataSection(std::string* executablePath)
	{
		const DWORD newSectionSize = 0x00400000;

		std::string folderPath = GetSteamGamePath();
		std::string gamePath = folderPath + "riseofnations.exe";
		HANDLE file = CreateFileA(gamePath.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE)
		{
			return FILE_OPEN_FAILED;
		}

		*executablePath = folderPath + MOD_EXECUTABLE_NAME;
		DeleteFileA(executablePath->c_str());
		HANDLE modFile = CreateFileA(executablePath->c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, file);
		if (modFile == INVALID_HANDLE_VALUE)
		{
			CloseHandle(file);
			return FILE_OPEN_FAILED;
		}

		DWORD fileSize = GetFileSize(file, NULL);
		if (!fileSize)
		{
			CloseHandle(file);
			CloseHandle(modFile);
			return FILE_SIZE_READ_FAILED;
		}

		BYTE* fileBuffer = new BYTE[fileSize];
		DWORD readLength;
		if (!ReadFile(file, fileBuffer, fileSize, &readLength, NULL))
		{
			delete[] fileBuffer;
			CloseHandle(file);
			CloseHandle(modFile);
			return FILE_UNREADABLE;
		}

		// DOS Header is located at the first byte of the PE file
		PIMAGE_DOS_HEADER dos = (PIMAGE_DOS_HEADER)fileBuffer;
		if (dos->e_magic != IMAGE_DOS_SIGNATURE)
		{
			// Invalid PE File
			delete[] fileBuffer;
			CloseHandle(file);
			CloseHandle(modFile);
			return INVALID_DOS_FILE;
		}

		PIMAGE_NT_HEADERS ntHeader = (PIMAGE_NT_HEADERS)(fileBuffer + dos->e_lfanew);
		if (ntHeader->FileHeader.Machine != IMAGE_FILE_MACHINE_I386)
		{
			delete[] fileBuffer;
			CloseHandle(file);
			CloseHandle(modFile);
			return IMAGE_IS_64_BIT;
		}

		PIMAGE_SECTION_HEADER sectionHeader = IMAGE_FIRST_SECTION(ntHeader);
		WORD sCount = ntHeader->FileHeader.NumberOfSections;

		ZeroMemory(&sectionHeader[sCount], sizeof(IMAGE_SECTION_HEADER));
		// 8 bytes for section size since that is the max allowed size
		CopyMemory(&sectionHeader[sCount].Name, SECTION_NAME, 8);

		//lets insert all the required information about our new PE section
		sectionHeader[sCount].Misc.VirtualSize = Align(newSectionSize, ntHeader->OptionalHeader.SectionAlignment, 0);
		sectionHeader[sCount].VirtualAddress = Align(sectionHeader[sCount - 1].Misc.VirtualSize, ntHeader->OptionalHeader.SectionAlignment, sectionHeader[sCount - 1].VirtualAddress);
		sectionHeader[sCount].SizeOfRawData = Align(newSectionSize, ntHeader->OptionalHeader.FileAlignment, 0);
		sectionHeader[sCount].PointerToRawData = Align(sectionHeader[sCount - 1].SizeOfRawData, ntHeader->OptionalHeader.FileAlignment, sectionHeader[sCount - 1].PointerToRawData);
		sectionHeader[sCount].Characteristics = 0xE00000E0;

		// Disable ASLR so addresses are always identical
		ntHeader->OptionalHeader.DllCharacteristics &= ~IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE;

		// Move file pointer to new size
		SetFilePointer(modFile, sectionHeader[sCount].PointerToRawData + sectionHeader[sCount].SizeOfRawData, NULL, FILE_BEGIN);
		// Resize mod file
		SetEndOfFile(modFile);

		ntHeader->OptionalHeader.SizeOfImage = sectionHeader[sCount].VirtualAddress + sectionHeader[sCount].Misc.VirtualSize;
		ntHeader->FileHeader.NumberOfSections += 1;

		SetFilePointer(modFile, 0, NULL, FILE_BEGIN);
		WriteFile(modFile, fileBuffer, fileSize, &readLength, NULL);
		delete[] fileBuffer;
		CloseHandle(file);
		CloseHandle(modFile);
		return ERROR_SUCCESS;
	}

	bool InjectCode(std::string& filePath)
	{
		HANDLE file = CreateFileA(filePath.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		DWORD fileSize = GetFileSize(file, NULL);
		byte* fileBytes = new byte[fileSize];
		DWORD readLength;
		if (!ReadFile(file, fileBytes, fileSize, &readLength, NULL))
		{
			std::cerr << "Read file failed: " << GetLastError() << std::endl;
			delete[] fileBytes;
			CloseHandle(file);
			return false;
		}

		PIMAGE_DOS_HEADER dos = (PIMAGE_DOS_HEADER)fileBytes;
		PIMAGE_NT_HEADERS nt = (PIMAGE_NT_HEADERS)(fileBytes + dos->e_lfanew);
		PIMAGE_SECTION_HEADER codeHeader = IMAGE_FIRST_SECTION(nt);
		PIMAGE_SECTION_HEADER modDataHeader = codeHeader + (nt->FileHeader.NumberOfSections - 1);

		Injector injector(nt->OptionalHeader.ImageBase, codeHeader->PointerToRawData - codeHeader->VirtualAddress, modDataHeader->PointerToRawData, modDataHeader->VirtualAddress, fileBytes);
		if (!injector.HandleConnectionData())
		{
			std::cerr << "Could not insert connection data" << std::endl;
			CloseHandle(file);
			return false;
		}

		DWORD getColorFuncPtr;
		if (!injector.HandleNewColors(&getColorFuncPtr))
		{
			std::cerr << "Could not insert color data" << std::endl;
			CloseHandle(file);
			return false;
		}
		
		if (!injector.HandleSetupWin(getColorFuncPtr))
		{
			std::cerr << "Could not insert setup win data" << std::endl;
			CloseHandle(file);
			return false;
		}

		SetFilePointer(file, 0, NULL, FILE_BEGIN);
		WriteFile(file, fileBytes, fileSize, &readLength, NULL);
		delete[] fileBytes;
		CloseHandle(file);
		return true;
	}

	bool InitLoader()
	{
		auto& gamePath = GetSteamGamePath();
		if (gamePath == "")
		{
			return false;
		}

		std::string executablePath;
		int code = AddPEModDataSection(&executablePath);
		if (code != ERROR_SUCCESS)
		{
			std::cerr << "Could not add PE mod section: " << code << std::endl;;
			return false;
		}

		std::cout << "Created PE expanded executable at " << executablePath << std::endl;
		CopyRequiredFiles();
		std::cout << "Required files copied!" << std::endl;

		std::cout << "Injecting code..." << std::endl;
		if (!InjectCode(executablePath))
		{
			std::cerr << "Unable to inject code!" << std::endl;
			return false;
		}

		std::cout << "Starting game process..." << std::endl;
		return !START_GAME || CreateGameProcessAndWaitForExit(gamePath, executablePath);
	}

	bool RunCleanup()
	{
		std::cout << "Cleaning up required files..." << std::endl;
		CleanupRequiredFiles();

		auto gamePath = GetSteamGamePath() + MOD_EXECUTABLE_NAME;
		// DeleteFileA(gamePath.c_str());
		return true;
	}
} // namespace MPExpander
