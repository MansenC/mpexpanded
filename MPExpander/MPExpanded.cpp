#include <iostream>
#include <string>

#include "DataSetup.h"
#include "RoNLoader.h"

using namespace ::MPExpanded;

int RunInjector();

int main()
{
	auto mutex = CreateMutex(NULL, true, L"02143d8a-8b5d-4d16-82b8-31adffbec62f");
	if (!mutex || GetLastError() == ERROR_ALREADY_EXISTS)
	{
		std::cerr << "Only one instance of this program can run at the same time";
		return -1;
	}

	int code;
	try
	{
		code = RunInjector();
	}
	catch (const std::exception& exception)
	{
		std::cerr << exception.what();
		code = -2;
	}

	ReleaseMutex(mutex);
	return 0;
}

int RunInjector()
{
	if (!InitLoader())
	{
		return 1;
	}

	std::cout << "Everything done, cleaning up" << std::endl;
	return !RunCleanup();
}

/*

Let's say we have ADDR 0x42069 holding a 4-byte pointer to a value:
MOV ECX, 0x42069
MOV ECX, dword ptr [ECX]
ECX now holds the base address of the value. Moving 0x1 into the value at offset 0x4 for example:
MOV dword ptr [ECX + 0x4], 0x1

Let's now say that [ECX + 0x0] is a pointer
MOV EAX, dword ptr [ECX]
We now have the base address of that stored value in EAX
MOV dword ptr [EAX + 0x4], 0x2

An example scenario code would be:

struct base
{
	base* otherBase;
	int value;
};

static base* staticRef = new base();

void someFunc()
{
	base* ref = staticRef;
	ref->value = 0x1;

	// let's assume that ref->otherBase is allocated already
	ref = ref->otherBase;
	ref->value = 0x2;
}

*/
