#pragma once

#include <iostream>
#include <string>
#include <Windows.h>

namespace MPExpanded
{
	bool CreateGameProcessAndWaitForExit(const std::string& executionPath, const std::string& exePath)
	{
		STARTUPINFO startupInfo;
		PROCESS_INFORMATION processInfo;

		ZeroMemory(&startupInfo, sizeof(STARTUPINFO));
		startupInfo.cb = sizeof(STARTUPINFO);
		ZeroMemory(&processInfo, sizeof(PROCESS_INFORMATION));

		auto wExecPath = std::wstring(executionPath.begin(), executionPath.end());
		auto wExePath = std::wstring(exePath.begin(), exePath.end());

		LPWSTR processArgs = _wcsdup(L"+skipIntro");
		if (!CreateProcessW(
			wExePath.c_str(),
			processArgs,
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			wExecPath.c_str(),
			&startupInfo,
			&processInfo))
		{
			std::cerr << "Could not create process: " << GetLastError() << std::endl;
			return false;
		}

		WaitForSingleObject(processInfo.hProcess, INFINITE);
		CloseHandle(processInfo.hProcess);
		CloseHandle(processInfo.hThread);
		return true;
	}
}

