#pragma once

#include <string>
#include <Windows.h>

constexpr auto NULL_HANDLE_NOT_FOUND = -1L;
constexpr auto FIND_RESOURCE_FAILED = -2L;
constexpr auto LOAD_RESOURCE_FAILED = -3L;

namespace MPExpanded
{
	int LoadResourceFile(int name, int type, DWORD& size, const char*& data)
	{
		HMODULE handle = GetModuleHandle(NULL);
		if (handle == INVALID_HANDLE_VALUE)
		{
			return NULL_HANDLE_NOT_FOUND;
		}

		HRSRC rc = FindResource(handle, MAKEINTRESOURCE(name), MAKEINTRESOURCE(type));
		if (rc == NULL)
		{
			return FIND_RESOURCE_FAILED;
		}

		HGLOBAL rcData = LoadResource(handle, rc);
		if (rcData == NULL)
		{
			return LOAD_RESOURCE_FAILED;
		}

		size = SizeofResource(handle, rc);
		data = static_cast<const char*>(LockResource(rcData));
		return ERROR_SUCCESS;
	}

	int GetResourceAsString(int name, int type, std::string* str)
	{
		DWORD size = 0;
		const char* data = NULL;
		int code = LoadResourceFile(name, type, size, data);
		if (code == ERROR_SUCCESS)
		{
			*str = std::string(data, size);
		}

		return code;
	}
} // namespace MPExpanded
