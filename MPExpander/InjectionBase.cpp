#include "InjectionBase.h"
#include <iostream>

namespace MPExpanded
{
	bool Injector::AsmToBytes(const DWORD start, const DWORD end, byte*& outBytes, DWORD* outSize) const
	{
		*outSize = end - start;
		outBytes = (byte*)malloc(*outSize);
		if (outBytes == nullptr)
		{
			return false;
		}

		memcpy(outBytes, (byte*)start, *outSize);
		return true;
	}

	bool Injector::WriteBytesToData(const DWORD start, const DWORD end, const DWORD writeOffset)
	{
		byte* asmBytes;
		DWORD size;
		if (!AsmToBytes(start, end, asmBytes, &size))
		{
			return false;
		}

		for (unsigned int i = 0; i < size; i++)
		{
			_data[i + writeOffset] = asmBytes[i];
		}

		free(asmBytes);
		return true;
	}

	void Injector::WriteDirectBytesToData(byte*& bytes, const DWORD size, const DWORD writeOffset)
	{
		memcpy(_data + writeOffset, bytes, size);
		free(bytes);
	}

	bool Injector::FixCodeLocations(const DWORD start, const DWORD end, DWORD* newValues, byte*& outBytes, DWORD* outSize) const
	{
		if (!AsmToBytes(start, end, outBytes, outSize))
		{
			return false;
		}

		DWORD* currentAddrValue;
		unsigned int currentReplacerIndex = 0;
		for (unsigned int i = 0; i < *outSize - 3; i++)
		{
			currentAddrValue = (DWORD*)(outBytes + i);
			if (*currentAddrValue != REPLACE_VALUE_INDICATOR)
			{
				continue;
			}

			DWORD* currentValue = (DWORD*)(outBytes + i);
			*currentValue = newValues[currentReplacerIndex];
			currentReplacerIndex++;
		}

		return true;
	}
	
	bool Injector::FixAllCodeLocations(
		const DWORD start,
		const DWORD end,
		byte*& outBytes,
		DWORD* outSize,
		const std::map<const DWORD, const DWORD> replaceIndicators) const
	{
		if (!AsmToBytes(start, end, outBytes, outSize))
		{
			return false;
		}

		DWORD* currentAddrValue;
		unsigned int currentReplacerIndex = 0;
		for (unsigned int i = 0; i < *outSize - 3; i++)
		{
			currentAddrValue = (DWORD*)(outBytes + i);
			const auto foundValue = replaceIndicators.find(*currentAddrValue);
			if (foundValue == replaceIndicators.end())
			{
				continue;
			}

			DWORD* currentValue = (DWORD*)(outBytes + i);
			*currentValue = foundValue->second;
			currentReplacerIndex++;
		}

		return true;
	}

	INJECTION_FUNCTION bool Injector::WriteJumpRelocation(const DWORD jmpLocation, const DWORD insertLocation, JumpType type)
	{
		DWORD codeStart = 0, codeEnd = 0;
		switch (type)
		{
		case JumpType::USE_EAX: {
			INJECTION_CODE_BEGIN(InsertJumpRelocationEAXStart, InsertJumpRelocationEAXEnd)
			__asm
			{
				MOV EAX, REPLACE_VALUE_INDICATOR ; jmpLocation
				JMP EAX
			}
			INJECTION_CODE_END(InsertJumpRelocationEAXEnd)

			codeStart = start;
			codeEnd = end;
			break;
		}
		case JumpType::USE_ECX: {
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(InsertJumpRelocationECXStart, InsertJumpRelocationECXEnd)
			__asm
			{
				MOV ECX, REPLACE_VALUE_INDICATOR ; jmpLocation
				JMP ECX
			}
			INJECTION_CODE_END(InsertJumpRelocationECXEnd)

			codeStart = start;
			codeEnd = end;
			break;
		}
		}
		
		DWORD newLocations[1] { jmpLocation };
		byte* asmBytes;
		DWORD size;
		if (!FixCodeLocations(codeStart, codeEnd, newLocations, asmBytes, &size))
		{
			return false;
		}

		WriteDirectBytesToData(asmBytes, size, _codeBase + insertLocation);
		return true;
	}

	bool Injector::InsertJumpWithData(const DWORD start, const DWORD end, const DWORD insertLocation, const std::string& functionName, JumpType type)
	{
		DWORD writeLength = (end - start) + INJECTOR_MOD_DATA_FUNCTION_OFFSET;
		memset(_data + _modDataWriteBase + _currentDataPtr, 0xCCCCCCCC, writeLength);
		if (!WriteBytesToData(start, end, _modDataWriteBase + _currentDataPtr))
		{
			std::cerr << "Write of " << functionName << " failed!" << std::endl;
			return false;
		}

		DWORD jmpLocation = _binaryOffset + _modDataVirtualBase + _currentDataPtr;
		_currentDataPtr += writeLength;
		std::cout << "Created " << functionName << "@" << std::hex << jmpLocation << std::endl;

		if (!WriteJumpRelocation(jmpLocation, insertLocation, type))
		{
			std::cerr << "Write of " << functionName << " failed!" << std::endl;
			return false;
		}

		std::cout << "Written " << functionName << " JMP relocation @" << std::hex << insertLocation << std::endl;
		return true;
	}

	bool Injector::InsertJumpWithFunctionCall(
		const DWORD start,
		const DWORD end,
		const DWORD insertLocation,
		const std::string& functionName,
		const DWORD functionLocation,
		JumpType type)
	{
		return InsertJumpWithFunctionCalls(start, end, insertLocation, functionName, {{ REPLACE_VALUE_INDICATOR, functionLocation }}, type);
	}
	
	bool Injector::InsertJumpWithFunctionCalls(
		const DWORD start,
		const DWORD end,
		const DWORD insertLocation,
		const std::string& functionName,
		const std::map<const DWORD, const DWORD>& functionTable,
		JumpType type)
	{
		byte* asmBytes;
		DWORD size;
		if (!FixAllCodeLocations(start, end, asmBytes, &size, functionTable))
		{
			std::cerr << "Write of " << functionName << " failed!" << std::endl;
			return false;
		}
		
		DWORD writeLength = size + INJECTOR_MOD_DATA_FUNCTION_OFFSET;
		memset(_data + _modDataWriteBase + _currentDataPtr, 0xCCCCCCCC, writeLength);
		WriteDirectBytesToData(asmBytes, size, _modDataWriteBase + _currentDataPtr);

		DWORD jmpLocation = _binaryOffset + _modDataVirtualBase + _currentDataPtr;
		_currentDataPtr += writeLength;
		std::cout << "Created " << functionName << "@" << std::hex << jmpLocation << std::endl;

		if (!WriteJumpRelocation(jmpLocation, insertLocation, type))
		{
			std::cerr << "Write of " << functionName << " failed!" << std::endl;
			return false;
		}

		std::cout << "Written " << functionName << " JMP relocation @" << std::hex << insertLocation << std::endl;
		return true;
	}
} // namespace MPExpanded