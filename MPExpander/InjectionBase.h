#pragma once

#include <Windows.h>
#include <string>
#include <map>

#include "resource.h"

#define INJECTION_CODE_BEGIN(StartLabel, EndLabel) \
DWORD start(0), end(0); \
__asm { \
	__asm MOV EAX, StartLabel \
	__asm MOV [start], EAX \
	__asm JMP EndLabel \
	__asm StartLabel: \
}

#define INJECTION_CODE_END(EndLabel) \
__asm { \
	__asm EndLabel: \
	__asm MOV EAX, EndLabel \
	__asm MOV [end], EAX \
} __pragma(warning( pop ))

// disable warnings for stack pointer stuff in code that isn't executed in this binary anyways
#define INJECTION_FUNCTION __pragma(warning( push )) __pragma(warning( disable : 4731 )) __pragma(warning( disable : 4733 ))

#define LOAD_STATIC_VALUE(Register, Value) __asm MOV Register, Value __asm MOV Register, dword ptr [Register]
#define LOAD_CONNECTION_DATA(Register) LOAD_STATIC_VALUE(Register, 0x014aea6c)

#define REPLACE_SINGLE_LINE(InsertLocation, LabelName, Code) { \
INJECTION_FUNCTION INJECTION_CODE_BEGIN(LabelName ## Start, LabelName ## End) \
__asm { Code } \
INJECTION_CODE_END(LabelName ## End) \
if (!WriteBytesToData(start, end, _codeBase + InsertLocation)) { return false; } }

#define REPLACE_PUSH_VALUE(InsertLocation, NewValue, LabelName) REPLACE_SINGLE_LINE(InsertLocation, LabelName, PUSH NewValue)
#define COMMA ,
#define REPLACE_CMP_STATEMENT(InsertLocation, LabelName, Address, Value) REPLACE_SINGLE_LINE(InsertLocation, LabelName, CMP Address COMMA Value)

// to bee or not to bee
#define REPLACE_VALUE_INDICATOR 0x2BEE0BEE
#define GET_COLOR_REPLACE_INDICATOR 0xF00D4BED
#define GET_COMBOBOX_REPLACE_INDICATOR 0xDEADBABE

#define INJECTOR_MOD_DATA_FUNCTION_OFFSET 0x16

// 0x148E. 0x0 means finding out where the program crashes by binary search in removing patches. POG
#define CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN 0x148E

#define USE_SIXTEEN_PLAYERS false
#if USE_SIXTEEN_PLAYERS

#define SETUPWIN_XML SETUPWIN_SIXTEEN_XML
#define NEW_PLAYER_COUNT 0x10
#define NEW_PLAYER_COUNT_MINUS_ONE 0xF
#define NEW_PLAYER_OBJECT_SIZE 0x3B0
#define CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN 0x183E
#define CONNECTION_DATA_ALLOCATED_SLOTS_BEGIN 0x1BEE
#define CONNECTION_DATA_TEAM_COLORS_BEGIN 0x1D6E
#define CONNECTION_DATA_SIZE 0x2476

#else

#define SETUPWIN_XML SETUPWIN_EIGHT_XML
#define NEW_PLAYER_COUNT 0x8
#define NEW_PLAYER_COUNT_MINUS_ONE 0x7
#define NEW_PLAYER_OBJECT_SIZE 0x1D8
// Testing means A87. Actual is 0x1666
#define CONNECTION_DATA_LAST_SENT_PLAYER_DATA_BEGIN 0x1666
// Testing means 0x1D8. Actual is 0x183E
#define CONNECTION_DATA_ALLOCATED_SLOTS_BEGIN 0x1D8
#define CONNECTION_DATA_TEAM_COLORS_BEGIN 0x18FE
#define CONNECTION_DATA_SIZE 0x2006

#endif

#define COLOR_TYPE_UNIT 0x4
#define COLOR_TYPE_BUILD 0x10
#define COLOR_TYPE_TEXT 0x1C
#define COLOR_TYPE_NEON 0x28
#define COLOR_TYPE_BORDER 0x34
#define COLOR_TYPE_IFACE 0x40

namespace MPExpanded
{
	class Injector
	{
	public:
		Injector(const DWORD binaryOffset, const DWORD codeBase, const DWORD modDataWriteBase, const DWORD modDataVirtualBase, byte*& data)
			: _binaryOffset(binaryOffset),
			  _codeBase(codeBase),
			  _modDataWriteBase(modDataWriteBase),
			  _modDataVirtualBase(modDataVirtualBase),
			  _data(data),
			  _currentDataPtr(0)
		{
		}

		bool HandleConnectionData();
		bool HandleNewColors(DWORD* getColorFuncPtr);
		bool HandleSetupWin(const DWORD getColorFuncPtr);

	private:
		enum class JumpType
		{
			USE_EAX,
			USE_ECX
		};

		bool AsmToBytes(const DWORD start, const DWORD end, byte*& outBytes, DWORD* outSize) const;
		bool WriteBytesToData(const DWORD start, const DWORD end, const DWORD writeOffset);
		void WriteDirectBytesToData(byte*& bytes, const DWORD size, const DWORD writeOffset);
		bool FixCodeLocations(
			const DWORD start,
			const DWORD end,
			DWORD* newValues,
			byte*& outBytes,
			DWORD* outSize) const;
		bool FixAllCodeLocations(
			const DWORD start,
			const DWORD end,
			byte*& outBytes,
			DWORD* outSize,
			const std::map<const DWORD, const DWORD> replaceIndicators) const;
		bool WriteJumpRelocation(const DWORD jmpLocation, const DWORD insertLocation, JumpType type = JumpType::USE_EAX);
		bool InsertJumpWithData(const DWORD start, const DWORD end, const DWORD insertLocation, const std::string& functionName, JumpType type = JumpType::USE_EAX);
		bool InsertJumpWithFunctionCall(
			const DWORD start,
			const DWORD end,
			const DWORD insertLocation,
			const std::string& functionName,
			const DWORD functionLocation,
			JumpType type = JumpType::USE_EAX);
		bool InsertJumpWithFunctionCalls(
			const DWORD start,
			const DWORD end,
			const DWORD insertLocation,
			const std::string& functionName,
			const std::map<const DWORD, const DWORD>& functionTable,
			JumpType type = JumpType::USE_EAX);

		// ConnectionDataInjector
		void ConnectionDataDebugPatch();
		bool InsertNewConnectionDataConstructor();
		bool InsertNewConnectionDataDestructor();
		bool InsertNewConnectionDataCloseFunction();
		bool InsertNewConnectionDataUnreadyAllPlayers();
		bool InsertNewConnectionDataGetPlayerIndex();
		bool InsertNewConnectionDataInitFunction();
		bool InsertNewRandomLambdaConnectionDataHandling();
		bool InsertNewRandomBoolLambdaConnectionDataHandling();
		bool InsertNewRandomIntLambdaConnectionDataHandling();
		bool InsertNewLambdaDataHandlingForStartGame();
		bool InsertNewCheckForUwpPlayersConnectionDataHandling();
		bool InsertNewCheckAllReadyConnectionDataHandling();
		bool InsertNewValidateStartConnectionDataHandling();
		bool InjectSetupWinAddChatConnectionDataHandling();
		bool InjectSetupWinUpdateLobbyNumPlayersConnectionDataHandling();
		bool InsertNewSetupWinOnCheckClickedFunction();
		bool InjectSetupWinStartGameSuccessModifications();
		bool InsertNewSetupWinOnTimerConnectionDataHandling();
		bool InsertNewSendPlayerConnectionDataHandling();
		bool InsertNewAddPlayerConnectionDataHandling();
		bool InsertNewSetupWinRequestComboChangeConnectionDataHandling();
		bool InsertNewSetupWinValidateModStartConnectionDataHandling();
		bool InsertNewSetupWinSetupGameConnectionDataHandling();
		bool InsertNewDownloadAllPlayerScoresConnectionDataHandling();
		bool InsertNewSetupWinOnFindLeaderboardConnectionDataHandling();
		bool InsertNewSetupWinCheckAllConnectedConnectionDataHandling();
		bool InsertNewSetupWinMakeComboChangeConnectionDataHandling();
		bool InsertNewSetupWinOnComboRedrawConnectionDataHandling();
		bool InsertNewSetupWinUpdateUiForRankedConnectionDataHandling();
		bool InsertNewSetupWinOnButtonClickedConnectionDataHandling();
		bool InsertNewSetupWinOnDownloadScoresFunction();
		bool InsertNewConnectionDataDeletePlayerHandling();
		bool InsertNewProcessComboChangeConnectionDataHandling();
		bool InsertNewSetupWinUpdateInterfaceConnectionDataHandling();
		bool InsertNewNetSysReceiveDataConnectionDataHandling();

		bool InsertNewAddPlayerAllocatedSlotsDataHandling();

		// Color stuff
		DWORD WriteGetColorFunction();
		bool InsertNewGetUnitColorHandling();
		bool InsertNewGetBuildColorHandling();
		bool InsertNewGetTextColorHandling();
		bool InsertNewGetNeonColorHandling();
		bool InsertNewGetBorderColorHandling();
		bool InsertNewGetIFaceColorHandling();

		// SetupWin
		bool RemoveFloodProtection();
		bool ReduceStartCountdown();
		DWORD WriteGetComboBoxAtFunction();
		bool InsertNewSetupWinSetupEloBoxesEloBoxHandling();
		bool InsertNewSetupWinUpdateUIPlayerEloEloBoxHandling();
		bool InsertNewSetupWinUpdateInterfaceEloBoxHandling();
		bool InsertNewSetupWinUpdateInterfaceReadyBoxesHandling();
		bool InsertNewSetupWinSetupChecksReadyBoxesHandling();
		bool InsertNewSetupWinOnCheckMoveReadyBoxesHandling();
		bool InsertNewSetupWinConstructorHandling();
		bool InsertNewSetupWinDestructorComboBoxHandling();
		bool InsertNewSetupWinUpdateInterfaceComboBoxHandling(DWORD getComboBoxFuncPtr);

		bool InsertNewSetupWinSetupGameComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinDoScenarioScriptSyncComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinDoScenarioChangesComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinValidateScenarioStartComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinValidateScenarioSelectComboBoxHandling();
		bool InsertNewSetupWinCheckScenarioSettingsComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinProcessComboChangeComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinGetComboValueComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinMakeComboChangeComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinValidateStartComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinProcessCheckChangeComboBoxHandling();
		bool InsertNewSetupWinUpdateUIForRankedComboBoxHandling();
		bool InsertNewSetupWinSetupForRankedComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinSetupCombosComboBoxHandling(DWORD getComboBoxFuncPtr, DWORD getColorFuncPtr);
		bool InsertNewSetupWinFiletypeComboComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinCategoryComboComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinOnPulldownChangeComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinOnComboMoveComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinExecComboBoxHandling(DWORD getComboBoxFuncPtr);
		bool InsertNewSetupWinResetGameComboBoxHandling(DWORD getComboBoxFuncPtr);

	private:
		const DWORD _binaryOffset;
		const DWORD _codeBase;
		const DWORD _modDataWriteBase;
		const DWORD _modDataVirtualBase;
		byte* _data;
		DWORD _currentDataPtr;
	};
} // namespace MPExpanded
