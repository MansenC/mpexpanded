#include "InjectionBase.h"
#include <iostream>

namespace MPExpanded
{
	INJECTION_FUNCTION DWORD Injector::WriteGetColorFunction()
	{
		// Param 0 = Return address, Param 1 = Category Type, Param 2 = Color index
		INJECTION_CODE_BEGIN(GetColorBegin, GetColorEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH ESI
			PUSH EDI

			LOAD_CONNECTION_DATA(ESI)
			ADD ESI, CONNECTION_DATA_TEAM_COLORS_BEGIN

			IMUL EDI, dword ptr [EBP + 0x10], 0x64
			ADD ESI, EDI

			MOV EDI, dword ptr [EBP + 0xC]
			LEA EAX, [ESI + EDI]
			MOVQ XMM0, qword ptr [EAX]
			MOV EDI, dword ptr [EAX + 0x8]
			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], EDI

			POP EDI
			POP ESI
			POP EBP
			RET 0xC
		}
		INJECTION_CODE_END(GetColorEnd)

		DWORD writeLength = (end - start) + INJECTOR_MOD_DATA_FUNCTION_OFFSET;
		memset(_data + _modDataWriteBase + _currentDataPtr, 0xCCCCCCCC, writeLength);
		if (!WriteBytesToData(start, end, _modDataWriteBase + _currentDataPtr))
		{
			std::cerr << "Write of GetColor(outColor, colorType, colorIndex) failed!" << std::endl;
			return -1;
		}

		DWORD writeLocation = _binaryOffset + _modDataVirtualBase + _currentDataPtr;
		_currentDataPtr += writeLength;
		std::cout << "Written void TeamColor::GetColor(outColor, colorType, colorIndex)@" << std::hex << writeLocation << std::endl;
		return writeLocation;
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetUnitColorHandling()
	{
		INJECTION_CODE_BEGIN(GetUnitPatchStart, GetUnitPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x4]
			MOV ECX, dword ptr [ECX + 0xC]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetUnitPatchEnd)

		return InsertJumpWithData(start, end, 0x00b7e860, "TeamColor::get_unit");
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetBuildColorHandling()
	{
		INJECTION_CODE_BEGIN(GetBuildPatchStart, GetBuildPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x10]
			MOV ECX, dword ptr [ECX + 0x18]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetBuildPatchEnd)

		return InsertJumpWithData(start, end, 0x00b7e340, "TeamColor::get_build");
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetTextColorHandling()
	{
		INJECTION_CODE_BEGIN(GetTextPatchStart, GetTextPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x1C]
			MOV ECX, dword ptr [ECX + 0x24]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetTextPatchEnd)

		return InsertJumpWithData(start, end, 0x00b7e9d0, "TeamColor::get_text");
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetNeonColorHandling()
	{
		INJECTION_CODE_BEGIN(GetNeonPatchStart, GetNeonPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x28]
			MOV ECX, dword ptr [ECX + 0x30]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetNeonPatchEnd)

		return InsertJumpWithData(start, end, 0x00b7e770, "TeamColor::get_neon");
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetBorderColorHandling()
	{
		INJECTION_CODE_BEGIN(GetBorderPatchStart, GetBorderPatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x34]
			MOV ECX, dword ptr [ECX + 0x3C]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetBorderPatchEnd)

		return InsertJumpWithData(start, end, 0x00b7e430, "TeamColor::get_border");
	}

	INJECTION_FUNCTION bool Injector::InsertNewGetIFaceColorHandling()
	{
		INJECTION_CODE_BEGIN(GetIFacePatchStart, GetIFacePatchEnd)
		__asm
		{
			LOAD_CONNECTION_DATA(EAX)
			ADD EAX, CONNECTION_DATA_TEAM_COLORS_BEGIN

			SUB ECX, 0x017229e0
			ADD ECX, EAX

			MOV EAX, dword ptr [EBP + 0x8]
			MOVQ XMM0, qword ptr [ECX + 0x40]
			MOV ECX, dword ptr [ECX + 0x48]
			MOVQ qword ptr [EAX], XMM0
			MOV dword ptr [EAX + 0x8], ECX
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetIFacePatchEnd)

		return InsertJumpWithData(start, end, 0x00b7eac0, "TeamColor::get_iface");
	}

	bool Injector::HandleNewColors(DWORD* getColorFuncPtr)
	{
		*getColorFuncPtr = WriteGetColorFunction();
		if (*getColorFuncPtr == 0x0)
		{
			return false;
		}

		return InsertNewGetUnitColorHandling()
				&& InsertNewGetBuildColorHandling()
				&& InsertNewGetTextColorHandling()
				&& InsertNewGetNeonColorHandling()
				&& InsertNewGetBorderColorHandling()
				&& InsertNewGetIFaceColorHandling();
	}
}
