#include "InjectionBase.h"
#include <stdio.h>

namespace MPExpanded
{
	bool Injector::InsertNewAddPlayerAllocatedSlotsDataHandling()
	{
		{
			INJECTION_CODE_BEGIN(AddPlayerAllocatedSlotsFirstPatchStart, AddPlayerAllocatedSlotsFirstPatchEnd)
			__asm
			{
				XOR EAX, EAX
				ADD ESI, CONNECTION_DATA_ALLOCATED_SLOTS_BEGIN
				MOV dword ptr [EBP - 0x2C], EAX

				LAB_0100aec3:
					CMP byte ptr [EDI], 0x4
					JNZ LAB_0100aedb

					PUSH 0x131d05c
					MOV ECX, ESI
					MOV EAX, 0x00463e10 ; basic_string::compare
					CALL EAX

					TEST EAX, EAX
					MOV EAX, dword ptr [EBP - 0x2C]
					JZ LAB_0100aeea

					LAB_0100aedb:
						INC EAX
						ADD EDI, 0x3B
						ADD ESI, 0x18
						MOV dword ptr [EBP - 0x2C], EAX
						CMP EAX, NEW_PLAYER_COUNT
						JL LAB_0100aec3

				LAB_0100aeea:
					MOV ESI, 0x0100aeea
					JMP ESI
			}
			INJECTION_CODE_END(AddPlayerAllocatedSlotsFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x00c0aeb8, "ExtendedConnectionData::add_player"))
			{
				return false;
			}
		}

		// TOOD not sure how the *0x8 should be handled here
		INJECTION_CODE_BEGIN(AddPlayerAllocatedSlotsSecondPatchStart, AddPlayerAllocatedSlotsSecondPatchEnd)
		__asm
		{
			CMP EAX, NEW_PLAYER_COUNT
			JZ tooManyPlayers

			LEA EAX, [EAX + EAX * 0x2]
			LEA EAX, [EAX + 0x3B]
			LEA ECX, [ESI + EAX * 0x8]
			LEA EAX, [EBX + 0x8]
			CMP ECX, EAX
			JZ backToNormalExecution

			PUSH -0x1
			PUSH 0x0
			PUSH EAX
			MOV EAX, 0x0045a0a0 ; basic_string::assign
			CALL EAX

			backToNormalExecution:
				MOV EAX, 0x0100b017
				JMP EAX

			tooManyPlayers:
				MOV EAX, 0x0100aefb
				JMP EAX
		}
		INJECTION_CODE_END(AddPlayerAllocatedSlotsSecondPatchEnd)

		return InsertJumpWithData(start, end, 0x00c0aef2, "ExtendedConnectionData::add_player");
	}
}
