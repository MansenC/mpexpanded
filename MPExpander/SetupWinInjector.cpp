#include <iostream>

#include "InjectionBase.h"

#if USE_SIXTEEN_PLAYERS

#define COMBO_BOX_COUNT 0x7E
#define TRIBE_COMBO_INDEX_START 0x10
#define COLOR_COMBO_INDEX_START 0x20
#define HANDICAP_COMBO_INDEX_START 0x30
#define DIFFICULTY_COMBO_INDEX_START 0x40
#define TEAM_COMBO_INDEX_START 0x50
#define PLAYER_COMBO_COUNT 0x60

#define TEAM_STYLES_COMBO 0x60
#define MAP_STYLES_COMBO 0x61
#define MAP_SIZES_COMBO 0x62
#define NUMBER_OF_PLAYERS_COMBO 0x63
#define MAX_OBSERVERS_COMBO 0x64
#define GAME_SPEEDS_COMBO 0x65
#define GAME_RULES_COMBO 0x66
#define UNKNOWN_COMBO 0x67
#define STARTING_TOWNS_COMBO 0x68
#define STARTING_RESOURCES_COMBO 0x69
#define STARTING_RESOURCES2_COMBO 0x6A
#define TECH_COSTS_COMBO 0x6B
#define REVEAL_MAPS_COMBO 0x6C
#define POP_LIMITS_COMBO 0x6D
#define RUSH_RULES_COMBO 0x6E
#define CANNON_TIMES_COMBO 0x6F
#define STARTING_TECHNOLOGIES_COMBO 0x70
#define STARTING_TECHNOLOGIES2_COMBO 0x71
#define ENDING_TECHNOLOGIES_COMBO 0x72
#define ELEMINATIONS_COMBO 0x73
#define VICTORIES_COMBO 0x74
#define WONDERWINS_COMBO 0x75
#define SCORES_COMBO 0x76
#define POPWINS_COMBO 0x77
#define TIME_LIMITS_COMBO 0x78
#define CHAIRS_COMBO 0x79
#define ECOWINS_COBMO 0x7A
#define SCENARIOS_COMBO 0x7B
#define SCRIPS_COMBO 0x7C
#define MODS_COMBO 0x7D

#else

#define COMBO_BOX_COUNT 0x4E
#define TRIBE_COMBO_INDEX_START 0x8
#define COLOR_COMBO_INDEX_START 0x10
#define HANDICAP_COMBO_INDEX_START 0x18
#define DIFFICULTY_COMBO_INDEX_START 0x20
#define TEAM_COMBO_INDEX_START 0x28
#define PLAYER_COMBO_COUNT 0x30

#define TEAM_STYLES_COMBO 0x30
#define MAP_STYLES_COMBO 0x31
#define MAP_SIZES_COMBO 0x32
#define NUMBER_OF_PLAYERS_COMBO 0x33
#define MAX_OBSERVERS_COMBO 0x34
#define GAME_SPEEDS_COMBO 0x35
#define GAME_RULES_COMBO 0x36
#define UNKNOWN_COMBO 0x37
#define STARTING_TOWNS_COMBO 0x38
#define STARTING_RESOURCES_COMBO 0x39
#define STARTING_RESOURCES2_COMBO 0x3A
#define TECH_COSTS_COMBO 0x3B
#define REVEAL_MAPS_COMBO 0x3C
#define POP_LIMITS_COMBO 0x3D
#define RUSH_RULES_COMBO 0x3E
#define CANNON_TIMES_COMBO 0x3F
#define STARTING_TECHNOLOGIES_COMBO 0x40
#define STARTING_TECHNOLOGIES2_COMBO 0x41
#define ENDING_TECHNOLOGIES_COMBO 0x42
#define ELEMINATIONS_COMBO 0x43
#define VICTORIES_COMBO 0x44
#define WONDERWINS_COMBO 0x45
#define SCORES_COMBO 0x46
#define POPWINS_COMBO 0x47
#define TIME_LIMITS_COMBO 0x48
#define CHAIRS_COMBO 0x49
#define ECOWINS_COBMO 0x4A
#define SCENARIOS_COMBO 0x4B
#define SCRIPS_COMBO 0x4C
#define MODS_COMBO 0x4D

#endif

namespace MPExpanded
{
	INJECTION_FUNCTION bool Injector::RemoveFloodProtection()
	{
		INJECTION_CODE_BEGIN(SetupWinRemoveFloodProtectionStart, SetupWinRemoveFloodProtectionEnd)
		__asm
		{
			MOV EAX, 0x00c82144
			JMP EAX
		}
		INJECTION_CODE_END(SetupWinRemoveFloodProtectionEnd)

		DWORD writeLocation = 0x00882135;
		if (WriteBytesToData(start, end, _codeBase + writeLocation))
		{
			std::cout << "Written RemoveFloodProtection hook at " << std::hex << writeLocation << std::endl;
			return true;
		}

		std::cerr << "RemoveFloodProtection failed to write" << std::endl;
		return true;
	}

	INJECTION_FUNCTION bool Injector::ReduceStartCountdown()
	{
		INJECTION_CODE_BEGIN(ReduceStartCountdownStart, ReduceStartCountdownEnd)
		__asm
		{
			MOV dword ptr [EDI + 0x1C], 0x64
		}
		INJECTION_CODE_END(ReduceStartCountdownEnd)

		DWORD writeLocation = 0x00883619;
		if (WriteBytesToData(start, end, _codeBase + writeLocation))
		{
			std::cout << "Written ReduceStartCountdown hook at " << std::hex << writeLocation << std::endl;
			return true;
		}

		std::cerr << "ReduceStartCountdown failed to write" << std::endl;
		return true;
	}

	INJECTION_FUNCTION DWORD Injector::WriteGetComboBoxAtFunction()
	{
		// This at ECX, Index at [EBP + 0x8]. Return is EAX and is the ADDRESS of the POINTER to the COMBO BOX
		// Currently this is structured as ID 0-95 being the player ui dropdowns and the other remaining 30 being the configuration thingys
		INJECTION_CODE_BEGIN(GetComboBoxAtStart, GetComboBoxAtEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			PUSH EDI

			LEA EDI, [ECX + 0x20AC]
			MOV EAX, dword ptr [EBP + 0x8]
#if USE_SIXTEEN_PLAYERS
			CMP EAX, 0x60
#else
			CMP EAX, 0x30
#endif
			JGE isConfig

			MOV EDI, dword ptr [EDI]
			LEA EAX, [EDI + EAX * 0x4]
			JMP doReturn

			isConfig:
#if USE_SIXTEEN_PLAYERS
				; Subtract the 48 from additionally created slots 
				SUB EAX, 0x30
#endif
				LEA EAX, [EDI + EAX * 0x4]

			doReturn:
				POP EDI
				POP EBP
				RET 0x4
		}
		INJECTION_CODE_END(GetComboBoxAtEnd)

		DWORD writeLength = (end - start) + INJECTOR_MOD_DATA_FUNCTION_OFFSET;
		memset(_data + _modDataWriteBase + _currentDataPtr, 0xCCCCCCCC, writeLength);
		if (!WriteBytesToData(start, end, _modDataWriteBase + _currentDataPtr))
		{
			std::cerr << "Write of GetComboBoxAt(index) failed!" << std::endl;
			return -1;
		}

		DWORD writeLocation = _binaryOffset + _modDataVirtualBase + _currentDataPtr;
		_currentDataPtr += writeLength;
		std::cout << "Written ComboBox* SetupWin::GetComboBoxAt(int)@" << std::hex << writeLocation << std::endl;
		return writeLocation;
	}
	
#pragma region ELO_BOXES
	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupEloBoxesEloBoxHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupWinSetupEloBoxesBegin, SetupWinSetupEloBoxesEnd)
			__asm
			{
				XOR EDI, EDI
				MOV ESI, dword ptr [EBX + 0x2230]
				MOV EAX, 0x00c7c625
				JMP EAX
			}
			INJECTION_CODE_END(SetupWinSetupEloBoxesEnd)

			if (!InsertJumpWithData(start, end, 0x0087c61d, "ExtendedSetupWin::SetupEloBoxes"))
			{
				return false;
			}
		}

		REPLACE_CMP_STATEMENT(0x0087c749, SingleLineCmpPatch, EDI, NEW_PLAYER_COUNT)
		return true;
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateUIPlayerEloEloBoxHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinUpdateUIPlayerEloStart, SetupWinUpdateUIPlayerEloEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			MOV EAX, FS:[0x0]
			PUSH -0x1
			PUSH 0x119454f
			PUSH EAX
			MOV EAX, dword ptr [EBP + 0x8]
			MOV dword ptr FS:[0x0], ESP
			SUB ESP, 0x14
			CMP EAX, NEW_PLAYER_COUNT
			JGE LAB_00c7ab74

			PUSH ESI
			MOV ESI, dword ptr [ECX + 0x2230]
			MOV ESI, dword ptr [ESI + EAX * 0x4]
			PUSH EDI
			MOV EDI, ESI
			MOV ECX, dword ptr [ESI + 0xBC0]
			TEST ECX, ECX
			JZ LAB_00c7aae2

			MOV EAX, 0x010e5420
			CALL EAX

			LAB_00c7aae2:
				MOV EAX, dword ptr [ESI + 0x27C]
				LEA ECX, [ESI + 0x27C]
				CALL dword ptr [EAX + 0x1C]

				MOV ECX, dword ptr [EBP + 0xC]
				MOV dword ptr [ESI + 0x508], 0x0
				MOV dword ptr [ESI + 0xBE4], 0xFFFFFFFF
				TEST ECX, ECX
				JLE LAB_00c7ab72

				XOR EAX, EAX
				MOV dword ptr [EBP - 0x14], EAX
				MOV dword ptr [EBP - 0x1A], EAX
				MOV dword ptr [EBP - 0x20], EAX
				MOV byte ptr [EBP - 0x17], AL
				MOV AL, [0x015ab035]
				MOV byte ptr [EBP - 0x15], AL
				XOR EAX, EAX
				MOV dword ptr [EBP - 0x10], EAX
				MOV word ptr [EBP - 0x1C], AX
				MOV dword ptr [EBP - 0x4], EAX
				PUSH ECX
				LEA ECX, [EBP - 0x20]
				MOV EAX, 0x010d8c90
				CALL EAX

				MOV ECX, dword ptr [EDI + 0xBC0]
				TEST ECX, ECX
				JZ LAB_00c7ab63

				PUSH 0x0
				PUSH 0x2
				PUSH 0x0
				PUSH 0x179d06c
				LEA EAX, [EBP - 0x20]
				PUSH EAX
				MOV EAX, 0x010e4f40
				CALL EAX

			LAB_00c7ab63:
				MOV dword ptr [EBP - 0x4], 0xFFFFFFFF
				LEA ECX, [EBP - 0x20]
				MOV EAX, 0x010d8a50
				CALL EAX

			LAB_00c7ab72:
				POP EDI
				POP ESI

			LAB_00c7ab74:
				MOV ECX, dword ptr [EBP - 0xC]
				MOV dword ptr FS:[0x0], ECX
				MOV ESP, EBP
				POP EBP
				RET 0x8
		}
		INJECTION_CODE_END(SetupWinUpdateUIPlayerEloEnd)

		return InsertJumpWithData(start, end, 0x0087aaa0, "ExtendedSetupWin::UpdateUIPlayerElo");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateInterfaceEloBoxHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupWinUpdateInterfaceEloBoxHandlingFirstStart, SetupWinUpdateInterfaceEloBoxHandlingFirstEnd)
			__asm
			{
				MOV EAX, dword ptr [EBP - 0x14]
				MOV ECX, dword ptr [ESI + 0x2230]
				MOV ECX, dword ptr [ECX + EAX * 0x4]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				MOV EAX, 0x00c722af
				JMP EAX
			}
			INJECTION_CODE_END(SetupWinUpdateInterfaceEloBoxHandlingFirstEnd)

			if (!InsertJumpWithData(start, end, 0x008722a0, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupWinUpdateInterfaceEloBoxHandlingSecondStart, SetupWinUpdateInterfaceEloBoxHandlingSecondEnd)
		__asm
		{
			MOV EAX, dword ptr [EBP - 0x14]
			MOV ECX, dword ptr [ESI + 0x2230]
			MOV ECX, dword ptr [ECX + EAX * 0x4]
			MOV EAX, dword ptr [ECX]
			PUSH 0x0
			CALL dword ptr [EAX + 0x18]
			MOV ECX, 0x00c71ef5
			JMP ECX
		}
		INJECTION_CODE_END(SetupWinUpdateInterfaceEloBoxHandlingSecondEnd)

		return InsertJumpWithData(start, end, 0x00871ee4, "ExtendedSetupWin::UpdateInterface");
	}
#pragma endregion ELO_BOXES
	
#pragma region READY_BOXES

	// TODO at 00c7334e, 00c733be; These check the slot combo box in relation to the current check box!! THIS MUST BE CHANGED
	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateInterfaceReadyBoxesHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinUpdateInterfaceReadyBoxesStart, SetupWinUpdateInterfaceReadyBoxesEnd)
		__asm
		{
			TEST byte ptr [ESI + 0x1F14], 0x2
			MOV EDI, dword ptr [ESI + 0x2210]
			JNZ doCheckReadyBoxes

			MOV ECX, 0x00c73337
			JMP ECX

			doCheckReadyBoxes:
				MOV ESI, NEW_PLAYER_COUNT
				loopBegin:
					MOV ECX, dword ptr [EDI]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]
					LEA EDI, [EDI + 0x4]
					SUB ESI, 0x1
					JNZ loopBegin

					MOV ESI, dword ptr [EBP - 0x2C]
					MOV ECX, 0x00c734b5
					JMP ECX
		}
		INJECTION_CODE_END(SetupWinUpdateInterfaceReadyBoxesEnd)

		return InsertJumpWithData(start, end, 0x0087330a, "ExtendedSetupWin::UpdateInterface");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupChecksReadyBoxesHandling()
	{
		{
			INJECTION_CODE_BEGIN(SetupWinSetupChecksStart, SetupWinSetupChecksEnd)
			__asm
			{
				MOV EDI, dword ptr [EAX + 0x2210]
				MOV ECX, 0x00c7d700
				JMP ECX
			}
			INJECTION_CODE_END(SetupWinSetupChecksEnd)

			if (!InsertJumpWithData(start, end, 0x0087d6ee, "ExtendedSetupWin::SetupChecks", JumpType::USE_ECX))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SingleLineCmpPatchStart, SingleLineCmpPatchEnd)
		__asm
		{
			CMP EBX, NEW_PLAYER_COUNT
		}
		INJECTION_CODE_END(SingleLineCmpPatchEnd)
		
		return WriteBytesToData(start, end, _codeBase + 0x0087d815);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnCheckMoveReadyBoxesHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinOnCheckMoveStart, SetupWinOnCheckMoveEnd)
		__asm
		{
			MOV EAX, dword ptr [EDI + 0x2210]
			MOV EAX, dword ptr [EAX + ESI * 0x4]
			MOV ECX, dword ptr [EAX + 0x184]
			TEST ECX, ECX
			JZ LAB_00c7faa0

			MOV EAX, dword ptr [ECX]
			LEA EDX, [EBP - 0x14]
			PUSH EDX
			CALL dword ptr [EAX + 0xCC]

			MOV ECX, dword ptr [EDI + 0x2210]
			MOV ECX, dword ptr [ECX + ESI * 0x4]
			JMP LAB_00c7fb18

			LAB_00c7faa0:
				MOV ECX, dword ptr [EDI + 0x2210]
				MOV ECX, dword ptr [ECX + ESI * 0x4]
				XOR EAX, EAX
				MOV dword ptr [EBP - 0x14], EAX
				MOV dword ptr [EBP - 0x10], EAX

			LAB_00c7fb18:
				MOV EAX, 0x00c7fb18
				JMP EAX
		}
		INJECTION_CODE_END(SetupWinOnCheckMoveEnd)

		return InsertJumpWithData(start, end, 0x0087fa7a, "ExtendedSetupWin::OnCheckMove");
	}
#pragma endregion READY_BOXES

#pragma region COMBO_BOXES
	// TODO cleanup in ~SetupWin()
	INJECTION_FUNCTION bool Injector::InsertNewSetupWinConstructorHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinConstructorBegin, SetupWinConstructorEnd)
		__asm
		{
			; Call memset for the combo_boxes. Just for the sake of it
			MOV EAX, 0x00c1b270
			CALL EAX
			ADD ESP, 0xC

			MOV EDX, NEW_PLAYER_COUNT
			IMUL EDX, EDX, 0x4
			PUSH EDX
			MOV EAX, 0x011d58c4 ; malloc
			CALL dword ptr [EAX]
			ADD ESP, 0x4

			MOV dword ptr [EDI + 0x2210], EAX
			
			MOV EDX, NEW_PLAYER_COUNT
			IMUL EDX, EDX, 0x4
			PUSH EDX
			MOV EAX, 0x011d58c4 ; malloc
			CALL dword ptr [EAX]
			ADD ESP, 0x4

			MOV dword ptr [EDI + 0x2230], EAX

			MOV EDX, PLAYER_COMBO_COUNT
			IMUL EDX, EDX, 0x4
			PUSH EDX
			MOV EAX, 0x011d58c4 ; malloc
			CALL dword ptr [EAX]
			ADD ESP, 0x4

			MOV dword ptr [EDI + 0x20AC], EAX

			MOV EAX, 0x00c84542
			JMP EAX
		}
		INJECTION_CODE_END(SetupWinConstructorEnd)

		return InsertJumpWithData(start, end, 0x0088453a, "ExtendedSetupWin::ExtendedSetupWin()");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinDestructorComboBoxHandling()
	{
		INJECTION_CODE_BEGIN(SetupWinDestructorStart, SetupWinDestructorEnd)
		__asm
		{
			LEA EAX, [EDI + 0x2210]
			PUSH EAX
			MOV ECX, 0x011d58cc
			CALL dword ptr [ECX]
			ADD ESP, 0x4
			
			LEA EAX, [EDI + 0x2230]
			PUSH EAX
			MOV ECX, 0x011d58cc
			CALL dword ptr [ECX]
			ADD ESP, 0x4

			MOV dword ptr [EDI + 0x1ea8], 0x0
			MOV ECX, 0x0047f0ff
			JMP ECX
		}
		INJECTION_CODE_END(SetupWinDestructorEnd)

		return InsertJumpWithData(start, end, 0x0007f0f5, "ExtendedSetupWin::~ExtendedSetupWin()", JumpType::USE_ECX);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateInterfaceComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxFirstPatchStart, UpdateInterfaceComboBoxFirstPatchEnd)
			__asm
			{
				PUSH ECX

				MOV EAX, dword ptr [ESI + 0x2210]
				MOV ECX, EDI
				SUB ECX, EAX
				SHR ECX, 0x2
				PUSH ECX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				POP ECX
				CMP dword ptr [EAX + 0x568], EDX
				MOV EAX, 0x00c7335a
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087334e, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxSecondPatchStart, UpdateInterfaceComboBoxSecondPatchEnd)
			__asm
			{
				PUSH ECX

				MOV EAX, dword ptr [ESI + 0x2210]
				MOV ECX, EDI
				SUB ECX, EAX
				SHR ECX, 0x2
				PUSH ECX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				POP ECX
				CMP dword ptr [EAX + 0x568], EDX
				MOV EAX, 0x00c733cb
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008733be, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfacePreLoopPatchOneStart, UpdateInterfacePreLoopPatchTwoEnd)
			__asm
			{
				XOR ECX, ECX
				XOR EDX, EDX
				MOV EAX, REPLACE_VALUE_INDICATOR
				MOV dword ptr [EBP - 0x14], ECX
				MOV dword ptr [EBP - 0x1C], EDX
				MOV dword ptr [EBP - 0x10], EAX

				MOV EAX, 0x00c712d1
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfacePreLoopPatchTwoEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008712bb, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxThirdPatchStart, UpdateInterfaceComboBoxThirdPatchEnd)
			__asm
			{
				MOVZX EAX, byte ptr [EDI + 0x34]
				PUSH EAX
				PUSH ECX
				MOV ECX, ESI
				MOV EAX, 0x00c75a70
				CALL EAX

				PUSH dword ptr [EBP - 0x14]
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				PUSH dword ptr [EBP + 0x8]
				MOVZX EAX, byte ptr [EDI + 0x35]
				MOV ECX, ESI
				PUSH 0x0
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TRIBE_COMBO_INDEX_START
				PUSH EAX
				MOV EAX, 0x00c75a70
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TRIBE_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				PUSH dword ptr [EBP + 0x8]
				MOVZX EAX, byte ptr [EDI + 0x36]
				MOV ECX, ESI
				PUSH 0x0
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV EAX, 0x00c75a70
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				PUSH dword ptr [EBP + 0x8]
				MOVZX EAX, byte ptr [EDI + 0x38]
				MOV ECX, ESI
				PUSH 0x0
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, HANDICAP_COMBO_INDEX_START
				PUSH EAX
				MOV EAX, 0x00c75a70
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, HANDICAP_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				PUSH dword ptr [EBP + 0x8]
				MOVZX EAX, byte ptr [EDI + 0x39]
				MOV ECX, ESI
				PUSH 0x0
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV EAX, 0x00c75a70
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				PUSH dword ptr [EBP + 0x8]
				MOVZX EAX, byte ptr [EDI + 0x37]
				MOV ECX, ESI
				PUSH 0x0
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV EAX, 0x00c75a70
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				CALL dword ptr [EBP - 0x10]

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				PUSH 0x0
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [EBP - 0x14]
				MOV EDX, dword ptr [EBP - 0x1C]
				INC ECX
				ADD EDX, 0x3B
				MOV dword ptr [EBP - 0x1C], EDX
				MOV dword ptr [EBP - 0x14], ECX
				CMP ECX, NEW_PLAYER_COUNT
				MOV EAX, 0x00c713ce
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxThirdPatchEnd)

			if (!InsertJumpWithData(start, end, 0x008712de, "ExtendedSetupWin::UpdateInterface"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxFourthPatchStart, UpdateInterfaceComboBoxFourthPatchEnd)
			__asm
			{
				PUSH EDI
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV dword ptr [EBP - 0x24], EAX
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX + 0x568]

				CMP EAX, 0x5
				JZ LAB_00c71840

				CMP EAX, 0x4
				JZ LAB_00c71840

				MOV EAX, dword ptr [ECX]
				PUSH 0x0
				CALL dword ptr [EAX + 0x18]
				
				PUSH EDI
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX
				
				MOV EAX, EDI
				ADD EAX, TRIBE_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				PUSH ECX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				POP ECX
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX
				
				MOV EAX, EDI
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				PUSH ECX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				POP ECX
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX
				
				MOV EAX, EDI
				ADD EAX, HANDICAP_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV EAX, EDI
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				PUSH ECX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				POP ECX
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX
				
				MOV EAX, EDI
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				PUSH ECX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				POP ECX
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV EAX, 0x00c7152d
				JMP EAX

				LAB_00c71840:
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]

					MOV EAX, EDI
					ADD EAX, TRIBE_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX
					
					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]
					
					MOV EAX, EDI
					ADD EAX, COLOR_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX
					
					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]
					
					MOV EAX, EDI
					ADD EAX, HANDICAP_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX
					
					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]
					
					MOV EAX, EDI
					ADD EAX, DIFFICULTY_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX
					
					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]
					
					MOV EAX, EDI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX
					
					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]

					MOV EAX, 0x00c722d8
					JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxFourthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00871475, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxFifthPatchStart, UpdateInterfaceComboBoxFifthPatchEnd)
			__asm
			{
				PUSH EDI
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDI, EAX
				CMP dword ptr [EBP - 0x10], 0x0
				MOV EAX, 0x00c7189b
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxFifthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087188e, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxSixthPatchStart, UpdateInterfaceComboBoxSixthPatchEnd)
			__asm
			{
				PUSH ECX
				PUSH dword ptr [EBP - 0x14]
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDI, EAX
				POP ECX

				MOV EAX, 0x00c71da1
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxSixthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00871d96, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxSeventhPatchStart, UpdateInterfaceComboBoxSeventhPatchEnd)
			__asm
			{
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TRIBE_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, HANDICAP_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]

				MOV EAX, 0x00c72292
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxSeventhPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087224a, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxEighthPatchStart, UpdateInterfaceComboBoxEighthPatchEnd)
			__asm
			{
				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TRIBE_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV dword ptr [EBP - 0x38], EAX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV dword ptr [EBP - 0x24], EAX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV dword ptr [EBP - 0x20], EAX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, dword ptr [EDI]
				CMP dword ptr [EAX + 0x568], 0x0
				MOV EAX, dword ptr [EBP - 0x14]
				JNZ LAB_00c7215a

				ADD EAX, HANDICAP_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX
				
				MOV ECX, dword ptr [EAX]
				MOV dword ptr [EBP - 0x30], EAX
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, dword ptr [EBP - 0x14]
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]

				MOV EAX, 0x00c71ed6
				JMP EAX

				LAB_00c7215a:
					ADD EAX, HANDICAP_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]

					MOV EAX, dword ptr [EBP - 0x14]
					ADD EAX, DIFFICULTY_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV ECX, dword ptr [EAX]
					MOV dword ptr [EBP + 0x8], EAX
					PUSH 0x0
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x18]

					MOV ECX, 0x00c7217e
					JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxEighthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00871e58, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxNinthPatchStart, UpdateInterfaceComboBoxNinthPatchEnd)
			__asm
			{
				PUSH COLOR_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV dword ptr [EBP - 0x54], 0x8
				MOV dword ptr [EBP - 0x50], EDX
				MOV EDI, 0x00c72034
				JMP EDI
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxNinthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00872024, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxTenthPatchStart, UpdateInterfaceComboBoxTenthPatchEnd)
			__asm
			{
				MOV EDX, dword ptr [EBP + 0x8]
				MOV dword ptr [EBP - 0x58], EDX

				MOV EAX, EDX
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, dword ptr [EBP - 0x2C]
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV ECX, dword ptr [EAX + 0x568]
				MOV dword ptr [EBP - 0x54], ECX

				MOV EAX, EDX
				ADD EAX, COLOR_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, dword ptr [EBP - 0x2C]
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV EAX, dword ptr [EAX + 0x568]
				MOV dword ptr [EBP - 0x50], EAX
				MOV EAX, 0x00c720d3
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxTenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008720ae, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxEleventhPatchStart, UpdateInterfaceComboBoxEleventhPatchEnd)
			__asm
			{
				MOV EDI, COMBO_BOX_COUNT
				MOV dword ptr [EBP + 0x8], REPLACE_VALUE_INDICATOR

				LAB_00c70e60:
					PUSH EDI
					MOV ECX, ESI
					CALL dword ptr [EBP + 0x8]

					MOV ECX, dword ptr [EAX]
					PUSH 0x1
					MOV EAX, 0x0110fb60
					CALL EAX

					SUB EDI, 0x1
					JNZ LAB_00c70e60
						
				PUSH 0x0
				MOV ECX, ESI
				CALL dword ptr [EBP + 0x8]

				MOV ECX, EAX
				PUSH 0x0
				MOV ECX, dword ptr [ECX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]
						
				PUSH TRIBE_COMBO_INDEX_START
				MOV ECX, ESI
				CALL dword ptr [EBP + 0x8]

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]
						
				PUSH COLOR_COMBO_INDEX_START
				MOV ECX, ESI
				CALL dword ptr [EBP + 0x8]

				MOV ECX, dword ptr [EAX]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]
						
				PUSH TEAM_COMBO_INDEX_START
				MOV ECX, ESI
				CALL dword ptr [EBP + 0x8]

				MOV ECX, dword ptr [EAX]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, dword ptr [ESI + 0x2184]
				CMP dword ptr [eAX + 0x568], 0xA
				JNZ LAB_00c70ec3

				LOAD_CONNECTION_DATA(EAX)
				CMP byte ptr [EAX + 0xA46], 0x0
				JA LAB_00c70ed0

				LAB_00c70ec3:
					PUSH TEAM_COMBO_INDEX_START
					MOV ECX, ESI
					CALL dword ptr [EBP + 0x8]

					MOV ECX, dword ptr [EAX]
					PUSH 0x1
					MOV EAX, 0x0110fb60
					CALL EAX

				LAB_00c70ed0:
					PUSH DIFFICULTY_COMBO_INDEX_START
					MOV ECX, ESI
					CALL dword ptr [EBP + 0x8]

					MOV ECX, dword ptr [EAX]
					MOV EAX, dword ptr [ECX]
					CALL dword ptr [EAX + 0x1C]

					MOV dword ptr [EBP - 0x34], 0x1
					MOV EDI, REPLACE_VALUE_INDICATOR
					MOV dword ptr [EBP + 0x8], 0x1

					LAB_00c70ef0:
						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, COLOR_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x1C]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, HANDICAP_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x1C]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, DIFFICULTY_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x1C]

						MOV EAX, dword ptr [ESI + 0x2178]
						MOV ECX, dword ptr [EBP + 0x8]
						CMP ECX, dword ptr [EAX + 0x568]

						PUSH dword ptr [EBP + 0x8]
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						JLE LAB_00c70f74

						CALL dword ptr [EAX + 0x1C]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, TRIBE_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x1C]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, TEAM_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x1C]
						JMP LAB_00c70fc1

					LAB_00c70f74:
						PUSH 0x0
						CALL dword ptr [EAX + 0x18]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, TRIBE_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						PUSH 0x0
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x18]

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, TEAM_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						PUSH 0x0
						MOV EAX, dword ptr [ECX]
						CALL dword ptr [EAX + 0x18]

						MOV EAX, dword ptr [ESI + 0x2184]
						CMP dword ptr [EAX + 0x568], 0xA
						JNZ LAB_00c70faa

						LOAD_CONNECTION_DATA(EAX)
						CMP byte ptr [EAX + 0xA46], 0x0
						JA LAB_00c70fbe

					LAB_00c70faa:
						PUSH dword ptr [EBP + 0x8]
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						PUSH 0x1
						MOV EAX, 0x0110fb60
						CALL EAX

						MOV EAX, dword ptr [EBP + 0x8]
						ADD EAX, TEAM_COMBO_INDEX_START
						PUSH EAX
						MOV ECX, ESI
						CALL EDI

						MOV ECX, dword ptr [EAX]
						PUSH 0x1
						MOV EAX, 0x0110fb60
						CALL EAX

					LAB_00c70fbe:
						INC dword ptr [EBP - 0x34]

					LAB_00c70fc1:
						MOV EAX, dword ptr [EBP + 0x8]
						INC EAX
						MOV dword ptr [EBP + 0x8], EAX
						CMP EAX, NEW_PLAYER_COUNT
						JL LAB_00c70ef0

				PUSH HANDICAP_COMBO_INDEX_START
				MOV ECX, ESI
				CALL EDI

				MOV ECX, dword ptr [EAX]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV EAX, 0x00c7232a
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxEleventhPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00870e48, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxTwelfthPatchStart, UpdateInterfaceComboBoxTwelfthPatchEnd)
			__asm
			{
				MOV dword ptr [EBP + 0x8], NEW_PLAYER_COUNT

				LAB_00c72350:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, dword ptr [EBP + 0x8]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EDI, EAX
					MOV EAX, dword ptr [EDI]
					CMP dword ptr [EAX + 0x568], 0x0
					JZ LAB_00c72393

					LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
					MOV EAX, dword ptr [ECX]
					MOV EAX, dword ptr [EAX + 0x10]
					CALL EAX
					TEST AL, AL
					JZ LAB_00c7238a

					LOAD_CONNECTION_DATA(EAX)
					ADD EAX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
					CMP byte ptr [EAX + 0x3A], 0x0
					JNZ LAB_00c7238a
					
					MOV EAX, dword ptr [ESI + 0x216C]
					CMP dword ptr [EAX + 0x568], 0xD
					JZ LAB_00c7238a

					PUSH 0x1
					JMP LAB_00c7238c

					LAB_00c7238a:
						PUSH 0x0

					LAB_00c7238c:
						MOV ECX, dword ptr [EDI]
						MOV EAX, 0x0110fb60
						CALL EAX

					LAB_00c72393:
						SUB dword ptr [EBP + 0x8], 0x1
						JNZ LAB_00c72350

				MOV ECX, 0x00c7239c
				JMP ECX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxTwelfthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00872342, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxThirteenthPatchStart, UpdateInterfaceComboBoxThirteenthPatchEnd)
			__asm
			{
				MOV EDI, REPLACE_VALUE_INDICATOR
				MOV ESI, NEW_PLAYER_COUNT

				LAB_00c72470:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, ESI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					CALL EDI

					MOV ECX, dword ptr [EAX]
					PUSH 0x0
					MOV EAX, 0x0110fb60
					CALL EAX

					SUB ESI, 0x1
					JNZ LAB_00c72470

				MOV EAX, 0x00c72481
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxThirteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00872461, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxFourteenthPatchStart, UpdateInterfaceComboBoxFourteenthPatchEnd)
			__asm
			{
				MOV ESI, NEW_PLAYER_COUNT

				LAB_00c724d0:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, ESI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, dword ptr [EBP - 0x2C]
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EDI, EAX
					MOV ECX, dword ptr [EDI]
					PUSH 0x2
					MOV EAX, 0x00561850
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x3
					MOV EAX, 0x00561850
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x8
					MOV EAX, 0x00561850
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x5
					MOV EAX, 0x00561850
					CALL EAX

					SUB ESI, 0x1
					JNZ LAB_00c724d0

				MOV EAX, 0x00c724fc
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxFourteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008724c5, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxFifteenthPatchStart, UpdateInterfaceComboBoxFifteenthPatchEnd)
			__asm
			{
				MOV ESI, NEW_PLAYER_COUNT

				LAB_00c72497:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, ESI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, dword ptr [EBP - 0x2C]
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EDI, EAX
					MOV ECX, dword ptr [EDI]
					PUSH 0x2
					MOV EAX, 0x00561800
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x3
					MOV EAX, 0x00561800
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x8
					MOV EAX, 0x00561800
					CALL EAX
						
					MOV ECX, dword ptr [EDI]
					PUSH 0x5
					MOV EAX, 0x00561800
					CALL EAX

					SUB ESI, 0x1
					JNZ LAB_00c72497

				MOV EAX, 0x00c724fc
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxFifteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087248c, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxSixteenthPatchStart, UpdateInterfaceComboBoxSixteenthPatchEnd)
			__asm
			{
				MOV EDI, NEW_PLAYER_COUNT

				LAB_00c72522:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EDI
					ADD EAX, TRIBE_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV ECX, dword ptr [EAX]
					PUSH 0x0
					MOV EAX, 0x0110fb60
					CALL EAX

					SUB EDI, 0x1
					JNZ LAB_00c72522

				MOV EAX, 0x00c72533
				JMP EAX
			}
			INJECTION_CODE_END(UpdateInterfaceComboBoxSixteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00872517, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(UpdateInterfaceComboBoxSeventeenthPatchStart, UpdateInterfaceComboBoxSeventeenthPatchEnd)
		__asm
		{
			LAB_00c73060:
				MOV word ptr [EBP + ECX * 0x2 - 0x304], CX
				INC ECX
				CMP ECX, COMBO_BOX_COUNT
				JL LAB_00c73060

			MOV AX, word ptr [EBP - 0x286]
			MOV CX, word ptr [EBP - 0x298]
			MOV word ptr [EBP - 0x298], AX
			MOV word ptr [EBP - 0x286], CX
			XOR EAX, EAX
			MOV dword ptr [EBP - 0x54], EAX

			LAB_00c73090:
				MOVSX EAX, word ptr [EBP + EAX * 0x2 - 0x304]

				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDI, EAX
				MOV ECX, dword ptr [EAX]

				TEST ECX, ECX
				JZ LAB_00c73157

				MOV EAX, 0x01110190
				CALL EAX

				TEST EAX, EAX
				JZ LAB_00c73157

				MOV ECX, dword ptr [EDI]
				LEA EDX, [ESI + 0x2A58]
				MOV EAX, dword ptr [ECX + 0x74]
				CMP EAX, EDX
				JNZ LAB_00c7310c

				MOV AX, word ptr [EBP - 0x68]
				CWDE
				PUSH EAX
				MOV AX, word ptr [EBP - 0x6C]
				CWDE
				PUSH EAX
				MOV EAX, 0x0110fad0
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x66]
				SUB EAX, dword ptr [EBP - 0x50]
				MOV ECX, dword ptr [EDI]
				PUSH 0x0
				CWDE
				PUSH 0x0
				MOV EDX, dword ptr [ECX]
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x6A]
				SUB EAX, dword ptr [EBP - 0x30]
				CWDE
				PUSH EAX
				CALL dword ptr [EDX + 0x20]

				MOV EAX, dword ptr [EBP - 0x28]
				ADD word ptr [EBP - 0x68], AX
				ADD word ptr [EBP - 0x66], AX
				MOV EAX, dword ptr [EBP - 0x68]
				MOV dword ptr [EBP - 0x50], EAX
				JMP LAB_00c73157

			LAB_00c7310c:
				LEA EDX, [ESI + 0x2EF0]
				CMP EAX, EDX
				JNZ LAB_00c73157

				MOV AX, word ptr [EBP - 0x40]
				CWDE
				PUSH EAX
				MOV AX, word ptr [EBP - 0x44]
				CWDE
				PUSH EAX
				MOV EAX, 0x0110fad0
				CALL EAX

				MOV EAX, dword ptr [EBP - 0x3E]
				SUB EAX, dword ptr [EBP + 0x8]
				MOV ECX, dword ptr [EDI]
				PUSH 0x0
				CWDE
				PUSH 0x0
				MOV EDX, dword ptr [ECX]
				PUSH EAX
				MOV EAX, dword ptr [EBP - 0x42]
				SUB EAX, dword ptr [EBP - 0x58]
				CWDE
				PUSH EAX
				CALL dword ptr [EDX + 0x20]

				MOV EDX, dword ptr [EBP - 0x5C]
				ADD word ptr [EBP - 0x40], DX
				ADD word ptr [EBP - 0x3E], DX
				MOV EAX, dword ptr [EBP - 0x40]
				MOV dword ptr [EBP + 0x8], EAX

			LAB_00c73157:
				MOV EAX, dword ptr [EBP - 0x54]
				INC EAX
				MOV dword ptr [EBP - 0x54], EAX
				CMP EAX, COMBO_BOX_COUNT
				JL LAB_00c73090

			MOV EAX, 0x00c73167
			JMP EAX
		}
		INJECTION_CODE_END(UpdateInterfaceComboBoxSeventeenthPatchEnd)

		return InsertJumpWithFunctionCall(start, end, 0x00873020, "ExtendedSetupWin::UpdateInterface", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupGameComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			// TODO relies on unpatched GameAccess
			INJECTION_CODE_BEGIN(SetupGameFirstPatchStart, SetupGameFirstPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, EBX
				MOV EBX, REPLACE_VALUE_INDICATOR

				LEA EDX, [ESI + 0x74]
				MOV EDI, NEW_PLAYER_COUNT

				LAB_00c70204:
					XOR EAX, EAX
					MOV word ptr [EDX], AX
						
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EDI
					ADD EAX, TRIBE_COMBO_INDEX_START
					PUSH EAX
					CALL EBX

					MOV EAX, dword ptr [EAX]
					MOV AL, byte ptr [EAX + 0x568]

					MOV byte ptr [EDX + 0x2], AL
						
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EDI
					ADD EAX, COLOR_COMBO_INDEX_START
					PUSH EAX
					CALL EBX

					MOV EAX, dword ptr [EAX]
					MOV AL, byte ptr [EAX + 0x568]

					MOV byte ptr [EDX + 0x3], AL
						
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EDI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					CALL EBX

					MOV EAX, dword ptr [EAX]
					MOV AL, byte ptr [EAX + 0x568]

					MOV byte ptr [EDX + 0x4], AL
					TEST byte ptr [ECX + 0x1F14], 0x2
					JZ LAB_00c70239

					XOR EAX, EAX
					JMP LAB_00c70242

					LAB_00c70239:
						MOV EAX, NEW_PLAYER_COUNT
						SUB EAX, EDI
						ADD EAX, HANDICAP_COMBO_INDEX_START
						PUSH EAX
						CALL EBX

						MOV EAX, dword ptr [EAX]
						MOV EAX, dword ptr [EAX + 0x568]

					LAB_00c70242:
						MOV byte ptr [EDX + 0x5], AL

						MOV EAX, NEW_PLAYER_COUNT
						SUB EAX, EDI
						ADD EAX, DIFFICULTY_COMBO_INDEX_START
						PUSH EAX
						CALL EBX

						MOV EAX, dword ptr [EAX]
						MOV AL, byte ptr [EAX + 0x568]

						MOV byte ptr [EDX + 0x8], AL
						ADD EDX, 0x8C
						SUB EDI, 0x1
						JNZ LAB_00c70204

				POP EBX
				MOV EDI, 0x00c7025f
				JMP EDI
			}
			INJECTION_CODE_END(SetupGameFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008701f6, "ExtendedSetupWin::SetupGame", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			// TODO relies on unpatched GameAccess
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameSecondPatchStart, SetupGameSecondPatchEnd)
			__asm
			{
				XOR ESI, ESI
				LOAD_STATIC_VALUE(EDX, 0x014aea2c)
				MOV dword ptr [EBP - 0x10], ESI

				PUSH HANDICAP_COMBO_INDEX_START
				MOV EBX, ECX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV AL, byte ptr [EAX + 0x568]
				MOV byte ptr [EDX + 0x2B], AL

				MOV ECX, EBX
				XOR EBX, EBX

				LAB_00c70360:
					MOV EAX, dword ptr [EDI + 0x2184]
					CMP dword ptr [EAX + 0x568], 0xA
					JZ LAB_00c7037a

					IMUL EAX, EBX, 0x8C
					MOV byte ptr [EAX + EDX + 0x7C], 0x2

					LAB_00c7037a:
						IMUL ECX, EBX, 0x8C
						ADD ECX, EDX
						MOV byte ptr [ECX + 0x7A], BL
						MOV dword ptr [ECX + 0xC0], 0x0
						TEST EBX, EBX
						JNZ LAB_00c703b2

						PUSH ECX
						PUSH 0x0
						MOV ECX, EDI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						POP ECX
						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], EBX
						JNZ LAB_00c703b2

						OR word ptr [EDX + 0x74], 0x7
						MOV dword ptr [EBP - 0x14], 0x1
						JMP LAB_00c70426

					LAB_00c703b2:
						MOVZX EAX, byte ptr [EDX + 0x27]
						INC EAX
						CMP EBX, EAX
						JGE LAB_00c70426

						PUSH ECX
						PUSH EBX
						MOV ECX, EDI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						POP ECX
						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], 0x0
						JNZ LAB_00c703d0

						OR word ptr [ECX + 0x74], 0x6

					LAB_00c703d0:
						PUSH ECX
						PUSH EBX
						MOV ECX, EDI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						POP ECX
						MOV EAX, dword ptr [EAX]
						MOV EAX, dword ptr [EAX + 0x568]
						CMP EAX, 0x2
						JNZ LAB_00c703e9

						MOV EAX, 0x1000
						JMP LAB_00c703f3

					LAB_00c703e9:
						CMP EAX, 0x3
						JNZ LAB_00c703f7

						MOV EAX, 0x2000

					LAB_00c703f3:
						OR word ptr [ECX + 0x74], AX

					LAB_00c703f7:
						OR word ptr [ECX + 0x74], 0x1
						MOV EAX, dword ptr [EDI + 0x2184]
						CMP dword ptr [EAX + 0x568], 0xA
						JZ LAB_00c70426

						MOVZX EAX, byte ptr [EDX + 0x77]
						CMP ESI, EAX
						MOV EAX, dword ptr [EBP - 0x10]
						JNZ LAB_00c7041d

						CMP dword ptr [EBP - 0x14], 0x0
						JZ LAB_00c7041d

						INC EAX

					LAB_00c7041d:
						MOV byte ptr [ECX + 0x77], AL
						INC EAX
						MOV dword ptr [EBP - 0x10], EAX
						MOV ESI, EAX

					LAB_00c70426:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c70360

				MOV ECX, 0x00c70430
				JMP ECX
			}
			INJECTION_CODE_END(SetupGameSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087033e, "ExtendedSetupWin::SetupGame", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		/*
		
		dword ptr [EBP - 0x10] contains counter
		dword ptr [EBP - 0x1C] contains this setupwin
		dword ptr [EBP - 0x24] contains current combobox
		
		*/
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameThirdPatchStart, SetupGameThirdPatchEnd)
			__asm
			{
				PUSH ECX
				PUSH dword ptr [EBP - 0x10]
				MOV ECX, dword ptr [EBP - 0x1C]
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				POP ECX

				MOV EDI, EAX
				MOV dword ptr [EBP - 0x24], EDI
				MOV EAX, dword ptr [EAX]
				MOV EAX, dword ptr [EAX + 0x568]
				TEST EAX, EAX
				JZ LAB_00c7054f

				CMP EAX, 0x2
				JZ LAB_00c7054f

				CMP EAX, 0x3
				JZ LAB_00c7054f

				CMP EAX, 0x1
				JNZ LAB_00c70555

				LAB_00c7054f:
					OR word ptr [EBX + EDX + 0x74], 0x1

				LAB_00c70555:
					MOV EAX, 0x00c70555
					JMP EAX
			}
			INJECTION_CODE_END(SetupGameThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00870534, "ExtendedSetupWin::SetupGame", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameFourthPatchStart, SetupGameFourthPatchEnd)
			__asm
			{
				PUSH ECX
				MOV EAX, dword ptr [EBP - 0x10]
				ADD EAX, DIFFICULTY_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, dword ptr [EBP - 0x1C]
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				POP ECX
				MOV EAX, dword ptr [EAX]
				MOV AL, byte ptr [EAX + 0x568]
				OR word ptr [EBX + ECX + 0x74], 0x4
				MOV byte ptr [EBX + ECX + 0x7C], AL
				MOV EAX, 0x00c7088c
				JMP EAX
			}
			INJECTION_CODE_END(SetupGameFourthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00870876, "ExtendedSetupWin::SetupGame", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupGameFifthPatchStart, SetupGameFifthPatchEnd)
		__asm
		{
			PUSH ECX
			MOV EAX, dword ptr [EBP - 0x10]
			ADD EAX, HANDICAP_COMBO_INDEX_START
			PUSH EAX
			MOV ECX, dword ptr [EBP - 0x1C]
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			POP ECX
			MOV EAX, dword ptr [EAX]
			MOV AL, byte ptr [EAX + 0x568]
			MOV byte ptr [EBX + ECX + 0x7C], AL
			MOV EAX, 0x00c708d2
			JMP EAX
		}
		INJECTION_CODE_END(SetupGameFifthPatchEnd)

		return InsertJumpWithFunctionCall(start, end, 0x008708c0, "ExtendedSetupWin::SetupGame", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinDoScenarioScriptSyncComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(DoScenarioScriptSyncStart, DoScenarioScriptSyncEnd)
		__asm
		{
			XOR EDX, EDX
			XOR EBX, EBX
			MOV ECX, EDI
			MOV ESI, REPLACE_VALUE_INDICATOR

			PUSH EBX
			CALL ESI

			MOV EAX, dword ptr [EAX]
			CMP dword ptr [EAX + 0x568], EDX
			SETZ CL

			INC EBX

			loopBegin:
				PUSH EBX
				CALL ESI

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x0
				JNZ loopEnd

				INC EDX

				loopEnd:
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL loopBegin

			MOV dword ptr [EBP - 0x10], EDX
			MOV EAX, dword ptr [EDI + 0x2184]
			CMP dword ptr [EAX + 0x568], 0xA
			MOV EAX, 0x00c73f85
			JMP EAX
		}
		INJECTION_CODE_END(DoScenarioScriptSyncEnd)

		return InsertJumpWithFunctionCall(start, end, 0x00873edf, "ExtendedSetupWin::DoScenarioScriptSync", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinDoScenarioChangesComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(Start, End)
		__asm
		{
			PUSH EBX
			PUSH EDI
			XOR EDX, EDX
			XOR EBX, EBX
			MOV ECX, ESI
			MOV EDI, REPLACE_VALUE_INDICATOR

			PUSH EBX
			CALL EDI

			MOV EAX, dword ptr [EAX]
			CMP dword ptr [EAX + 0x568], EDX
			SETZ CL

			INC EBX

			loopBegin:
				PUSH EBX
				CALL EDI

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x0
				JNZ loopEnd

				INC EDX

				loopEnd:
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL loopBegin

			MOV dword ptr [EBP - 0x10], EDX
			POP EDI
			POP EBX
			MOV EAX, 0x00c7532c
			JMP EAX
		}
		INJECTION_CODE_END(End)

		return InsertJumpWithFunctionCall(start, end, 0x00875295, "ExtendedSetupWin::DoScenarioChanges", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinValidateScenarioStartComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(ValidateScenarioStartStart, ValidateScenarioStartEnd)
		__asm
		{
			MOV ESI, REPLACE_VALUE_INDICATOR

			LAB_00c74c80:
				PUSH EDI
				MOV ECX, EBX
				CALL ESI

				MOV EBX, dword ptr [EAX]
				CMP dword ptr [EBX + 0x568], 0x4
				JNZ LAB_00c74c98

				PUSH 0x1
				PUSH EDI
				MOV EAX, 0x00c772e0
				CALL EAX
				MOV ECX, dword ptr [EBP - 0x10]

				LAB_00c74c98:
					MOV EAX, EDI
					ADD EAX, COLOR_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, EBX
					CALL ESI

					MOV EAX, dword ptr [EAX]
					MOV EDX, dword ptr [EAX + 0x568]
					CMP EDX, NEW_PLAYER_COUNT
					JNZ LAB_00c74cc9

					MOV EAX, dword ptr [EBP - 0x5C]
					CMP EAX, dword ptr [EBP - 0x58]
					JL LAB_00c74cbb

					MOVSX EAX, word ptr [EBP - 0x54]
					LEA ECX, [EBP - 0x60]
					PUSH EAX
					MOV EAX, dword ptr [EBP - 0x60]
					CALL dword ptr [EAX]

				LAB_00c74cbb:
					MOV EAX, dword ptr [EBP - 0x5C]
					MOV ECX, dword ptr [EBP - 0x50]
					MOV dword ptr [ECX + EAX * 0x4], EDI
					INC dword ptr [EBP - 0x5C]
					JMP LAB_00c74cda

				LAB_00c74cc9:
					CMP dword ptr [EBX + 0x568], 0x5
					JZ LAB_00c74cda

					BTS ECX, EDX
					MOV dword ptr [EBP - 0x10], ECX

				LAB_00c74cda:
					LOAD_CONNECTION_DATA(EAX)
					INC EDI
					MOV ECX, dword ptr [EBP - 0x10]
					MOVZX EAX, byte ptr [EAX + 0xA46]
					CMP EDI, EAX
					JL LAB_00c74c80

			MOV ESI, 0x00c74cf1
			JMP ESI
		}
		INJECTION_CODE_END(ValidateScenarioStartEnd)

		return InsertJumpWithFunctionCall(start, end, 0x00874c74, "ExtendedSetupWin::ValidateScenarioStart", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinValidateScenarioSelectComboBoxHandling()
	{
		INJECTION_CODE_BEGIN(Start, End)
		__asm
		{
		}
		INJECTION_CODE_END(End)

		return InsertJumpWithData(start, end, 0x0, "ExtendedSetupWin::");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinCheckScenarioSettingsComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		// TODO a lot of unpatched code for scenarios
		INJECTION_CODE_BEGIN(CheckScenarioSettingsStart, CheckScenarioSettingsEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP
			LOAD_STATIC_VALUE(EAX, 0x014aea2c)
			PUSH EDI
			MOV EDI, ECX
			CMP byte ptr [EAX + 0x4A4], 0x0
			JNZ LAB_00c75a60

			MOV EAX, dword ptr [EDI + 0x2184]
			PUSH EBX
			PUSH ESI
			CMP dword ptr [EAX + 0x568], 0xA
			JNZ LAB_00c759a9

			LOAD_CONNECTION_DATA(EAX)
			CMP byte ptr [EAX + 0xA46], 0x0
			JBE LAB_00c759a9

			MOV EBX, dword ptr [EBP + 0x8]
			MOV AL, byte ptr [EAX + EBX + 0xA6B]
			TEST AL, AL
			JNZ LAB_00c75777

			LEA ESI, [EBX + 0x82B]
			MOV ECX, dword ptr [EDI + ESI * 0x4]
			LEA ESI, [EDI + ESI * 0x4]
			MOV EAX, dword ptr [ECX]
			CALL dword ptr [EAX + 0x1C]

			MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
			MOV EAX, dword ptr [ECX]
			CALL dword ptr [EAX + 0x1C]

			MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
			MOV EAX, dword ptr [ECX]
			CALL dword ptr [EAX + 0x1C]

			MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20CC]
			MOV EAX, dword ptr [ECX]
			CALL dword ptr [EAX + 0x1C]

			JMP LAB_00c75949

			LAB_00c75777:
				PUSH 0x0
				CMP AL, 0x1
				JNZ LAB_00c758a6

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20AC]
				LEA ESI, [EDI + EBX * 0x4]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [ESI + 0x20AC]
				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [ESI + 0x20AC]
				PUSH 0x5
				MOV EAX, 0x00561800
				CALL EAX

				MOV ECX, dword ptr [ESI + 0x20AC]
				PUSH 0x6
				MOV EAX, 0x00561800
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20CC]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				LOAD_CONNECTION_DATA(EAX)
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20CC]
				TEST byte ptr [EAX + 0xA4A], 0x2
				JZ LAB_00c757f3

				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				PUSH 0x1
				JMP LAB_00c757fc

			LAB_00c757f3:
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX

				PUSH 0x0

			LAB_00c757fc:
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				LOAD_CONNECTION_DATA(EAX)
				XOR ESI, ESI
				ADD EAX, 0xA4B
				MOV dword ptr [EBP + 0x8], EAX

			LAB_00c75833:
				CMP byte ptr [EAX + ESI], 0x1
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
				PUSH ESI
				JZ LAB_00c75848

				MOV EAX, 0x005617b0
				CALL EAX
				JMP LAB_00c7584d

			LAB_00c75848:
				MOV EAX, 0x00561760
				CALL EAX

			LAB_00c7584d:
				MOV EAX, dword ptr [EBP + 0x8]
				INC ESI
				CMP ESI, 0x8
				JL LAB_00c75833

			MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
			PUSH 0x8
			MOV EAX, 0x00561760
			CALL EAX

			TEST byte ptr [EDI + 0x1F14], 0x2
			JNZ LAB_00c75893

			LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
			TEST ECX, ECX
			JZ LAB_00c75882

			MOV EAX, dword ptr [ECX]
			MOV EAX, dword ptr [EAX + 0x10]
			CALL EAX

			TEST AL, AL
			JNZ LAB_00c75893

			LAB_00c75882:
				LEA ESI, [EBX + 0x82B]
				MOV ECX, dword ptr [EDI + ESI * 0x4]
				LEA ESI, [EDI + ESI * 0x4]
				JMP LAB_00c75942

			LAB_00c75893:
				LEA ESI, [EBX + 0x82B]
				MOV ECX, dword ptr [EDI + ESI * 0x4]
				LEA ESI, [EDI + ESI * 0x4]
				PUSH 0x1
				JMP LAB_00c75944

			LAB_00c758a6:
				LEA ESI, [EBX + 0x82B]
				MOV ECX, dword ptr [EDI + ESI * 0x4]
				LEA ESI, [EDI + ESI * 0x4]
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [ESI]
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20CC]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				LOAD_CONNECTION_DATA(EAX)
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x212C]
				TEST byte ptr [EAX + 0xA4A], 0x2
				JZ LAB_00c7590a

				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				PUSH 0x1
				JMP LAB_00c75921

			LAB_00c7590a:
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x214C]
				PUSH 0x0
				MOV EAX, 0x0110fb60
				CALL EAX

				PUSH 0x0

			LAB_00c75921:
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20CC]
				MOV EAX, 0x0110fb60
				CALL EAX

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x20EC]

			LAB_00c75942:
				PUSH 0x0

			LAB_00c75944:
				MOV EAX, 0x0110fb60
				CALL EAX

			LAB_00c75949:
				MOV EAX, dword ptr [ESI]
				MOV EAX, dword ptr [EAX + 0x568]
				CMP EAX, 0x1
				JZ LAB_00c75973

				CMP EAX, 0x3
				JZ LAB_00c75973

				CMP EAX, 0x2
				JZ LAB_00c75973

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x212C]
				CALL dword ptr [EAX + 0x1C]
				POP ESI
				POP EBX
				POP EDI
				POP EBP
				RET 0x4

			LAB_00c75973:
				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x212C]
				PUSH 0x0
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				LOAD_CONNECTION_DATA(EAX)
				CMP byte ptr [EAX + EBX * 0x4 + 0xA6B], 0x1
				JNZ LAB_00c75a5e

				MOV ECX, dword ptr [EDI + EBX * 0x4 + 0x212C]
				PUSH 0x1
				MOV EAX, 0x0110fb60
				CALL EAX

				POP ESI
				POP EBX
				POP EDI
				POP EBP
				RET 0x4

			; Patched from here on!
			LAB_00c759a9:
				MOV ESI, dword ptr [EBP + 0x8]
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				PUSH 0x0
				MOV ECX, dword ptr [EAX]
				MOV EBX, EAX
				MOV dword ptr [EBP + 0x8], EBX
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x18]

				MOV ECX, dword ptr [EBX]
				PUSH 0x5
				MOV EAX, 0x00561850
				CALL EAX

				MOV ECX, dword ptr [EBX]
				PUSH 0x6
				MOV EAX, 0x00561850
				CALL EAX

				XOR EBX, EBX
				LEA EAX, [ESI + COLOR_COMBO_INDEX_START]
				PUSH EAX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ESI, EAX

			LAB_00c759e0:
				MOV ECX, dword ptr [ESI]
				PUSH EBX
				MOV EAX, 0x00561760
				CALL EAX

				INC EBX
				CMP EBX, 0x8
				JL LAB_00c759e0

			MOV ECX, dword ptr [ESI]
			PUSH 0x8
			MOV EAX, 0x005617b0
			CALL EAX

			TEST byte ptr [EDI + 0x1F14], 0x2
			JNZ LAB_00c75a21

			LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
			TEST ECX, ECX
			JZ LAB_00c75a1d

			MOV EAX, dword ptr [ECX]
			MOV EAX, dword ptr [EAX + 0x10]
			CALL EAX

			TEST AL, AL
			JNZ LAB_00c75a21

			LAB_00c75a1d:
				PUSH 0x0
				JMP LAB_00c75a23

			LAB_00c75a21:
				PUSH 0x1

			LAB_00c75a23:
				MOV ECX, dword ptr [EBP + 0x8]
				MOV ECX, dword ptr [ECX]
				MOV EAX, 0x0110fb60
				CALL EAX

				TEST byte ptr [EDI + 0x1F14], 0x2
				JZ LAB_00c75a5e

				XOR ESI, ESI

			LAB_00c75a40:
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x5
				JNZ LAB_00c75a55

				PUSH 0x0
				PUSH ESI
				MOV EAX, 0x00c76e90
				CALL EAX

			LAB_00c75a55:
				INC ESI
				CMP ESI, NEW_PLAYER_COUNT
				JL LAB_00c75a40

			LAB_00c75a5e:
				POP ESI
				POP EBX

			LAB_00c75a60:
				POP EDI
				POP EBP
				RET 0x4
		}
		INJECTION_CODE_END(CheckScenarioSettingsEnd)

		return InsertJumpWithFunctionCall(start, end, 0x008756f0, "ExtendedSetupWin::CheckScenarioSettings", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinProcessComboChangeComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(ProcessComboChangeFirstPatchStart, ProcessComboChangeFirstPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV EAX, dword ptr [EAX + 0x568]
				CMP EAX, EDI
				PUSH 0x00c75aa8
				RET
			}
			INJECTION_CODE_END(ProcessComboChangeFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00875a99, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSecondPatchStart, ProcessComboChangeSecondPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x0
				MOV EAX, 0x00c75d70
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00875d62, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeThirdPatchStart, ProcessComboChangeThirdPatchEnd)
			__asm
			{
				MOV dword ptr [EBP + 0xC], REPLACE_VALUE_INDICATOR

				LAB_00c75b55:
					; Tribe
					LEA EAX, [EBX + TRIBE_COMBO_INDEX_START]
					INC EAX
					PUSH EAX
					MOV ECX, ESI
					CALL dword ptr [EBP + 0xC]

					MOV EAX, dword ptr [EAX]
					PUSH dword ptr [EAX + 0x568]

					LEA EDI, [EBX + TRIBE_COMBO_INDEX_START]
					LEA EAX, [EDI - DIFFICULTY_COMBO_INDEX_START]
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					; Team
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					INC EAX
					PUSH EAX
					MOV ECX, ESI
					CALL dword ptr [EBP + 0xC]

					MOV EAX, dword ptr [EAX]
					PUSH dword ptr [EAX + 0x568]
						
					PUSH EDI
					MOV EAX, 0x00c76e90
					CALL EAX

					; Slot
					LEA EAX, [EBX + 0x1]
					PUSH EAX
					MOV ECX, ESI
					CALL dword ptr [EBP + 0xC]

					MOV EAX, dword ptr [EAX]
					PUSH dword ptr [EAX + 0x568]

					PUSH EBX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT_MINUS_ONE
					JL LAB_00c75b55

				MOV EAX, 0x00c75ba5
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00875b49, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			// TODO CHECK LOGIC
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFourthPatchStart, ProcessComboChangeFourthPatchEnd)
			__asm
			{
				MOV EBX, NEW_PLAYER_COUNT
				SUB EBX, 0x2

				LEA EAX, [EBX - 0x1]
				PUSH EAX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x4
				JNZ LAB_00c7618d

				MOV EDI, REPLACE_VALUE_INDICATOR

				LAB_00c76160:
					PUSH EBX
					MOV ECX, ESI
					CALL EDI

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					CMP EAX, 0x1
					JZ LAB_00c76177
						
					CMP EAX, 0x2
					JZ LAB_00c76177
						
					CMP EAX, 0x3
					JNZ LAB_00c76181

					LAB_00c76177:
						PUSH 0x4
						PUSH EBX
						MOV ECX, ESI
						MOV EAX, 0x00c76e90
						CALL EAX

					LAB_00c76181:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c76160

				MOV EDI, dword ptr [EBP + 0xC]
				LAB_00c7618d:
					MOV EAX, 0x00c7618d
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeFourthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876146, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFifthPatchStart, ProcessComboChangeFifthPatchEnd)
			__asm
			{
				XOR EBX, EBX
				LAB_00c76270:
					PUSH 0x8
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76270

				MOV EAX, 0x00c76283
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeFifthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876269, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSixthPatchStart, ProcessComboChangeSixthPatchEnd)
			__asm
			{
				PUSH TEAM_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x0
				JZ LAB_00c76465

				MOV dword ptr [EAX + 0x568], 0x0
				TEST byte ptr [ESI + 0x1F14], 0x2
				JNZ LAB_00c76465
				
				PUSH TEAM_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				ADD ECX, 0x28C
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]

				MOV ECX, 0x00c76452
				JMP ECX

				LAB_00c76465:
					MOV EAX, 0x00c76465
					CALL EAX
			}
			INJECTION_CODE_END(ProcessComboChangeSixthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087641f, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSeventhPatchStart, ProcessComboChangeSeventhPatchEnd)
			__asm
			{
				MOV EBX, 0x1

				LAB_00c76470:
					PUSH 0x1
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76470

				MOV EAX, 0x00c76283
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeSeventhPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876465, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeEighthPatchStart, ProcessComboChangeEighthPatchEnd)
			__asm
			{
				PUSH TEAM_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], 0x0
				JZ LAB_00c76402

				MOV dword ptr [EAX + 0x568], 0x0
				TEST byte ptr[ESI + 0x1F14], 0x2
				JNZ LAB_00c76402
				
				PUSH TEAM_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				ADD ECX, 0x28C
				MOV EAX, dword ptr [ECX]
				CALL dword ptr [EAX + 0x1C]
				
				MOV ECX, 0x00c763ef
				JMP ECX

				LAB_00c76402:
					MOV EAX, 0x00c76402
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeEighthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008763bc, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeNinthPatchStart, ProcessComboChangeNinthPatchEnd)
			__asm
			{
				MOV EBX, 0x1

				LAB_00c76407:
					PUSH 0x1
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76407

				MOV EAX, 0x00c76283
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeNinthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876402, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeTenthPatchStart, ProcessComboChangeTenthPatchEnd)
			__asm
			{
				MOV EBX, NEW_PLAYER_COUNT
				SUB EBX, 0x2
				MOV EDI, REPLACE_VALUE_INDICATOR

				LAB_00c76096:
					PUSH EBX
					MOV ECX, ESI
					CALL EDI

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					TEST EAX, EAX
					JZ LAB_00c760b6
					
					CMP EAX, 0x3
					JZ LAB_00c760b6

					CMP EAX, 0x2
					JZ LAB_00c760b6

					PUSH 0x1
					PUSH EBX
					MOV ECX, ESI
					MOV EAX, 0x00c76e90
					CALL EAX

					LAB_00c760b6:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c76096

				MOV EDI, dword ptr [EBP + 0xC]
				MOV EAX, 0x00c760c2
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeTenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087608d, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeEleventhPatchStart, ProcessComboChangeEleventhPatchEnd)
			__asm
			{
				MOV EBX, STARTING_TOWNS_COMBO
				
				LAB_00c76110:
					LEA EAX, [EBX - PLAYER_COMBO_COUNT]
					CMP EAX, 0x1D
					JA LAB_00c76488

					LEA ECX, [EBX - PLAYER_COMBO_COUNT]
					MOV EAX, 0x00c92bc0
					CALL EAX

					MOV ECX, EAX
					JMP LAB_00c7648a

					LAB_00c76488:
						XOR ECX, ECX

					LAB_00c7648a:
						LEA EAX, [ECX - 0x1]
						CMP EBX, ENDING_TECHNOLOGIES_COMBO
						CMOVNZ EAX, ECX
						MOV ECX, ESI
						PUSH EAX
						PUSH EBX
						MOV EAX, 0x00c76e90
						CALL EAX

						INC EBX
						CMP EBX, COMBO_BOX_COUNT
						JL LAB_00c76110

				MOV EAX, 0x00c764a6
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeEleventhPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087610b, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeTwelfthPatchStart, ProcessComboChangeTwelfthPatchEnd)
			__asm
			{
				XOR EBX, EBX

				LAB_00c764e0:
					CMP EDI, 0x7
					JNZ LAB_00c764ee

					CMP EDI, 0x6
					JL LAB_00c764ee

					PUSH 0x8
					JMP LAB_00c764f0

					LAB_00c764ee:
						PUSH 0x5

					LAB_00c764f0:
						LEA EAX, [EBX - TEAM_COMBO_INDEX_START]
						MOV ECX, ESI
						PUSH EAX
						MOV EAX, 0x00c76e90
						CALL EAX

						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c764e0

				MOV EAX, 0x00c76283
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeTwelfthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008764d8, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeThirteenthPatchStart, ProcessComboChangeThirteenthPatchEnd)
			__asm
			{
				XOR ECX, ECX
				MOV dword ptr [EBP + 0xC], ECX
				XOR EBX, EBX
				JMP LAB_00c767a3

				LAB_00c767a0:
					MOV ECX, dword ptr [EBP + 0xC]

				LAB_00c767a3:
					PUSH ECX
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					POP ECX
					MOV EAX, dword ptr [EAX]
					INC ECX
					CMP dword ptr [EAX + 0x568], EBX
					CMOVNZ ECX, dword ptr [EBP + 0xC]
					MOV dword ptr [EBP + 0xC], ECX
					CMP EDI, 0x9
					JZ LAB_00c767be
					CMP EDI, 0x8
					JNZ LAB_00c767ce

				LAB_00c767be:
					PUSH 0x8
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

				LAB_00c767ce:
					CMP EDI, 0xA
					JNZ LAB_00c767ee
						
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					CMP dword ptr [EAX + 0x568], 0x8
					JNZ LAB_00c767ee

					PUSH 0x5
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

				LAB_00c767ee:
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c767a0

				CMP dword ptr [EBP + 0xC], NEW_PLAYER_COUNT
				JNZ LAB_00c768b0

				XOR EBX, EBX

				comboColorLoopBegin:
					MOV EAX, EBX
					SHR EAX, 0x1
					PUSH EAX
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, 0x00c76e90
					CALL EAX

					CMP EBX, NEW_PLAYER_COUNT
					JL comboColorLoopBegin

				XOR EBX, EBX

				tribeComboLoopBegin:
					LEA EDX, [EBX + TRIBE_COMBO_INDEX_START]
					PUSH EDX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					PUSH dword ptr [EAX + 0x568]
					LEA EAX, [EDX + 0x1]
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					LEA EBX, [EBX + 0x2]
					CMP EBX, NEW_PLAYER_COUNT
					JL tribeComboLoopBegin

				LAB_00c768b0:
					MOV EAX, 0x00c768b0
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeThirteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876787, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFourteenthPatchStart, ProcessComboChangeFourteenthPatchEnd)
			__asm
			{
				PUSH EDI
				
				XORPS XMM0, XMM0
				MOVUPS xmmword ptr [EBP - 0x34], XMM0
				MOVUPS xmmword ptr [EBP - 0x24], XMM0

				PUSH COLOR_COMBO_INDEX_START
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				INC word ptr [EBP + EAX * 0x2 - 0x34]
				MOV EDI, dword ptr [ESI + 0x1F14]
				AND EDI, 0x2
				MOV EBX, 0x1

				fourteenFirstLoopBegin:
					TEST EDI, EDI
					JZ innerIf

					MOV EAX, dword ptr [ESI + 0x2178]
					CMP dword ptr [EAX + 0x568], EBX
					JL loopEnd
					JMP alsoElsePart

					innerIf:
						PUSH EBX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], 0x4
						JNZ loopEnd

					alsoElsePart:
						LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						MOV EAX, dword ptr [EAX + 0x568]
						INC word ptr [EBP + EAX * 0x2 - 0x34]

					loopEnd:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL fourteenFirstLoopBegin

				POP EDI
				MOV EBX, 0x1
				XOR EAX, EAX
				CMP word ptr [EBP - 0x34], 0x1
				SETG AL

				fourteenSomeOtherLoop:
					CMP word ptr [EBP + EBX * 0x2 - 0x34], 0x1
					LEA ECX, [EAX + 0x1]
					CMOVLE ECX, EAX
					MOV EAX, ECX
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL fourteenSomeOtherLoop
						
				TEST EAX, EAX
				JZ alsoLAB_00c768b0

				XOR EBX, EBX

				LAB_00c76770:
					PUSH EBX
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76770

				alsoLAB_00c768b0:
					MOV EAX, 0x00c768b0
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeFourteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876587, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeFifteenthPatchStart, ProcessComboChangeFifteenthPatchEnd)
			__asm
			{
				XORPS XMM0, XMM0
				MOVUPS xmmword ptr [EBP - 0x34], XMM0
				MOVUPS xmmword ptr [EBP - 0x24], XMM0
				XOR EBX, EBX
				MOV EDX, REPLACE_VALUE_INDICATOR

				fifteenColorLoopStart:
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					CMP EAX, NEW_PLAYER_COUNT
					JGE fifteenColorLoopEnd

					INC word ptr [EBP + EAX * 0x2 - 0x34]

					fifteenColorLoopEnd:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL fifteenColorLoopStart
				
				MOV EBX, dword ptr [EBP + 0x8]
				MOV EAX, dword ptr [ESI + 0x216C]
				MOV ECX, dword ptr [EAX + 0x568]
				MOV EAX, dword ptr [ESI + 0x2184]
				CMP dword ptr [EAX + 0x568], 0xA
				JZ LAB_00c76ad8

				CMP ECX, 0x8
				JZ LAB_00c76a03

				CMP ECX, 0x9
				JZ LAB_00c76a03

				CMP ECX, 0xA
				JZ LAB_00c76a03

				CMP ECX, 0xB
				JZ LAB_00c76a03

				CMP ECX, 0xC
				JNZ LAB_00c76ad8

				LAB_00c76a03:
					CMP EDI, NEW_PLAYER_COUNT
					JGE LAB_00c76ae1

					CMP word ptr [EBP + EDI * 0x2 - 0x34], 0x1
					JLE LAB_00c76ae1

					LEA ECX, [EBX - COLOR_COMBO_INDEX_START]
					MOV dword ptr [EBP + 0xC], 0x0
					MOV dword ptr [EBP + 0x14], ECX
					XOR EDX, EDX

					LAB_00c76a30:
						LEA EAX, [EDX + COLOR_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], EDI
						JNZ LAB_00c76a6f
							
						LEA EAX, [EBX - COLOR_COMBO_INDEX_START]
						CMP EDX, EAX

						PUSH EDX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						XOR ECX, ECX
						SETNZ CL
						CMP dword ptr [EAX + 0x568], 0x4
						MOV EAX, 0x1
						LEA ECX, [ECX * 0x2 + 0x2]
						CMOVGE ECX, EAX
						CMP ECX, dword ptr [EBP + 0xC]
						JLE LAB_00c76a6c

						MOV dword ptr [EBP + 0xC], ECX
						MOV ECX, EDX
						MOV dword ptr [EBP + 0x14], ECX
						JMP LAB_00c76a6f

					LAB_00c76a6c:
						MOV ECX, dword ptr [EBP + 0x14]

					LAB_00c76a6f:
						INC EDX
						CMP EDX, NEW_PLAYER_COUNT
						JL LAB_00c76a30

					XOR EBX, EBX

					LEA EAX, [ECX + TRIBE_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV EDX, dword ptr [EAX + 0x568]
					MOV dword ptr [EBP + 0x14], EDX

					LAB_00c76aa0:
						LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], EDI
						JNZ LAB_00c76ac7

						LEA EAX, [EBX + TRIBE_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, ESI
						MOV EAX, REPLACE_VALUE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], EDX
						JZ LAB_00c76ac7

						PUSH EDX
						LEA EAX, [EBX + TRIBE_COMBO_INDEX_START]
						MOV ECX, ESI
						PUSH EAX
						MOV EAX, 0x00c76e90
						CALL EAX

						MOV EDX, dword ptr [EBP + 0x14]

						LAB_00c76ac7:
							INC EBX
							CMP EBX, NEW_PLAYER_COUNT
							JL LAB_00c76aa0

					MOV EBX, dword ptr [EBP + 0x8]
					JMP LAB_00c76ae1

				LAB_00c76ad8:
					PUSH EDI
					PUSH EBX
					MOV ECX, ESI
					MOV EAX, 0x00c76e90
					CALL EAX

				LAB_00c76ae1:
					MOV EAX, 0x00c76ae1
					JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeFifteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876914, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSixteenthPatchStart, ProcessComboChangeSixteenthPatchEnd)
			__asm
			{
				XOR EDX, EDX
				MOV dword ptr [EBP + 0x14], EDX

				PUSH EBX
				MOV ECX, ESI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV EAX, dword ptr [EAX + 0x568]
				MOV dword ptr [EBP + 0xC], EAX
				LEA ECX, [EBX - COLOR_COMBO_INDEX_START]

				LAB_00c76ba3:
					CMP EDX, ECX
					JZ LAB_00c76bd3

					LEA EAX, [EDX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, ESI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					CMP EAX, dword ptr [EBP + 0xC]
					JNZ LAB_00c76bcd

					PUSH EDI
					LEA EAX, [EDX + TRIBE_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					MOV EDX, dword ptr [EBP + 0x14]

					LAB_00c76bcd:
						LEA ECX, [EBX - COLOR_COMBO_INDEX_START]

					LAB_00c76bd3:
						INC EDX
						MOV dword ptr [EBP + 0x14], EDX
						CMP EDX, NEW_PLAYER_COUNT
						JL LAB_00c76ba3

				MOV EAX, 0x00c76be2
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeSixteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876b92, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeSeventeenthPatchStart, ProcessComboChangeFirstSeventeenthPatchEnd)
			__asm
			{
				MOV EBX, STARTING_TOWNS_COMBO

				LAB_00c76bf0:
					LEA EAX, [EBX - PLAYER_COMBO_COUNT]
					CMP EAX, 0x1D
					JA LAB_00c76c04

					LEA ECX, [EBX - PLAYER_COMBO_COUNT]
					MOV EAX, 0x00c92bc0
					CALL EAX

					MOV ECX, EAX
					JMP LAB_00c76c06

					LAB_00c76c04:
						XOR ECX, ECX

					LAB_00c76c06:
						LEA EAX, [ECX - 0x1]
						CMP EBX, ENDING_TECHNOLOGIES_COMBO
						CMOVNZ EAX, ECX
						MOV ECX, ESI
						PUSH EAX
						PUSH EBX
						MOV EAX, 0x00c76e90
						CALL EAX

						INC EBX
						CMP EBX, COMBO_BOX_COUNT
						JL LAB_00c76bf0

				MOV EAX, 0x00c76c1e
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeFirstSeventeenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876beb, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeEighteenthPatchStart, ProcessComboChangeEighteenthPatchEnd)
			__asm
			{
				XOR EBX, EBX
				LAB_00c76c40:
					PUSH 0x8
					LEA EAX, [EBX - TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76c40

				MOV EAX, 0x00c76c53
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeEighteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876c3d, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeNineteenthPatchStart, ProcessComboChangeNineteenthPatchEnd)
			__asm
			{
				XOR EBX, EBX
				LAB_00c76c74:
					PUSH 0x8
					LEA EAX, [EBX - TEAM_COMBO_INDEX_START]
					MOV ECX, ESI
					PUSH EAX
					MOV EAX, 0x00c76e90
					CALL EAX

					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL LAB_00c76c74

				MOV EAX, 0x00c76c87
				JMP EAX
			}
			INJECTION_CODE_END(ProcessComboChangeNineteenthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876c72, "ExtendedSetupWin::ProcessComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		REPLACE_PUSH_VALUE(0x00876028, RUSH_RULES_COMBO, ProcessComboChangeRushRules)
		REPLACE_PUSH_VALUE(0x00876517, NUMBER_OF_PLAYERS_COMBO, ProcessComboChangeNOP7)
		REPLACE_PUSH_VALUE(0x008762c9, NUMBER_OF_PLAYERS_COMBO, ProcessComboChangeNOP5)
		REPLACE_PUSH_VALUE(0x008768fb, WONDERWINS_COMBO, ProcessComboChangeWonderWins1)
		REPLACE_PUSH_VALUE(0x008768d9, WONDERWINS_COMBO, ProcessComboChangeWonderWins2)
		REPLACE_PUSH_VALUE(0x008768e9, WONDERWINS_COMBO, ProcessComboChangeWonderWins3)
		REPLACE_PUSH_VALUE(0x008768f2, WONDERWINS_COMBO, ProcessComboChangeWonderWins4)
		REPLACE_PUSH_VALUE(0x00876b00, VICTORIES_COMBO, ProcessComboChangeVictories)
		REPLACE_PUSH_VALUE(0x00876b21, GAME_RULES_COMBO, ProcessComboChangeGameRules)
		REPLACE_PUSH_VALUE(0x00876b2c, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles)
		REPLACE_PUSH_VALUE(0x00876b51, GAME_RULES_COMBO, ProcessComboChangeGameRules2)
		REPLACE_PUSH_VALUE(0x00876b5c, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles2)
		REPLACE_PUSH_VALUE(0x00876c34, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles3)
		REPLACE_PUSH_VALUE(0x00876c69, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles4)
			
		REPLACE_PUSH_VALUE(0x00876e07, WONDERWINS_COMBO, ProcessComboChangeWonderWins5)
		REPLACE_PUSH_VALUE(0x00876e10, SCRIPS_COMBO, ProcessComboChangeScripts1)
		REPLACE_PUSH_VALUE(0x00876dd1, WONDERWINS_COMBO, ProcessComboChangeWonderWins6)
		REPLACE_PUSH_VALUE(0x00876dda, SCRIPS_COMBO, ProcessComboChangeScripts2)
		REPLACE_PUSH_VALUE(0x00876de7, WONDERWINS_COMBO, ProcessComboChangeWonderWins7)
		REPLACE_PUSH_VALUE(0x00876df0, SCRIPS_COMBO, ProcessComboChangeScripts3)
		REPLACE_PUSH_VALUE(0x00876df6, WONDERWINS_COMBO, ProcessComboChangeWonderWins8)
		REPLACE_PUSH_VALUE(0x00876dff, SCRIPS_COMBO, ProcessComboChangeScripts4)
		REPLACE_PUSH_VALUE(0x00876c99, RUSH_RULES_COMBO, ProcessComboChangeRushRules2)
		REPLACE_PUSH_VALUE(0x00876ca4, STARTING_RESOURCES_COMBO, ProcessComboChangeStartingResources1)
		REPLACE_PUSH_VALUE(0x00876cad, VICTORIES_COMBO, ProcessComboChangeVictories2)
		REPLACE_PUSH_VALUE(0x00876cb8, ELEMINATIONS_COMBO, ProcessComboChangeEleminations1)
		REPLACE_PUSH_VALUE(0x00876cc1, STARTING_RESOURCES_COMBO, ProcessComboChangeStartingResources2)
		REPLACE_PUSH_VALUE(0x00876cf4, STARTING_TOWNS_COMBO, ProcessComboChangeStartingTowns)
		REPLACE_PUSH_VALUE(0x00876cff, STARTING_RESOURCES_COMBO, ProcessComboChangeStartingResources3)
		REPLACE_PUSH_VALUE(0x00876d08, VICTORIES_COMBO, ProcessComboChangeVictories3)
		REPLACE_PUSH_VALUE(0x00876d13, RUSH_RULES_COMBO, ProcessComboChangeRushRules3)
		REPLACE_PUSH_VALUE(0x00876d2b, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles5)
		REPLACE_PUSH_VALUE(0x00876d45, RUSH_RULES_COMBO, ProcessComboChangeRushRules4)
		REPLACE_PUSH_VALUE(0x00876d50, VICTORIES_COMBO, ProcessComboChangeVictories4)
		REPLACE_PUSH_VALUE(0x00876d5b, ELEMINATIONS_COMBO, ProcessComboChangeEleminations2)
		REPLACE_PUSH_VALUE(0x00876d73, TEAM_STYLES_COMBO, ProcessComboChangeTeamStyles6)
		REPLACE_PUSH_VALUE(0x00876d8d, RUSH_RULES_COMBO, ProcessComboChangeRushRules5)
		REPLACE_PUSH_VALUE(0x00876d98, VICTORIES_COMBO, ProcessComboChangeVictories5)
		REPLACE_PUSH_VALUE(0x00876da3, ELEMINATIONS_COMBO, ProcessComboChangeEleminations3)
		REPLACE_PUSH_VALUE(0x00876da9, TECH_COSTS_COMBO, ProcessComboChangeTechCosts)
		REPLACE_PUSH_VALUE(0x00876e1b, SCENARIOS_COMBO, ProcessComboChangeScenarios)
		REPLACE_PUSH_VALUE(0x00876cca, STARTING_RESOURCES_COMBO, ProcessComboChangeStartingResources4)
		REPLACE_PUSH_VALUE(0x00876cd5, STARTING_TECHNOLOGIES_COMBO, ProcessComboChangeStartingTechnologies)
		REPLACE_PUSH_VALUE(0x00876ce0, WONDERWINS_COMBO, ProcessComboChangeWonderWins9)
		REPLACE_PUSH_VALUE(0x00876ceb, POPWINS_COMBO, ProcessComboChangePopwins)

		REPLACE_CMP_STATEMENT(0x00875bf2, ProcessComboChangeCmpPatch1, EBX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_CMP_STATEMENT(0x00875b2a, ProcessComboChangeCmpPatch2, EBX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_CMP_STATEMENT(0x00875b44, ProcessComboChangeCmpPatch3, EBX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_CMP_STATEMENT(0x00875dea, ProcessComboChangeCmpPatch4, EBX, TEAM_STYLES_COMBO)
		REPLACE_CMP_STATEMENT(0x00875e02, ProcessComboChangeCmpPatch5, EBX, SCENARIOS_COMBO)
		REPLACE_CMP_STATEMENT(0x00875e21, ProcessComboChangeCmpPatch6, EBX, STARTING_TECHNOLOGIES_COMBO)
		REPLACE_CMP_STATEMENT(0x00875ee0, ProcessComboChangeCmpPatch7, EBX, ENDING_TECHNOLOGIES_COMBO)
		REPLACE_CMP_STATEMENT(0x00875f5b, ProcessComboChangeCmpPatch8, EBX, VICTORIES_COMBO)
		REPLACE_CMP_STATEMENT(0x0087602f, ProcessComboChangeCmpPatch9, EBX, TEAM_STYLES_COMBO)
		REPLACE_CMP_STATEMENT(0x00876902, ProcessComboChangeCmpPatch10, EBX, COLOR_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x0087690b, ProcessComboChangeCmpPatch11, EBX, HANDICAP_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00876ae1, ProcessComboChangeCmpPatch12, EBX, RUSH_RULES_COMBO)
		REPLACE_CMP_STATEMENT(0x00876b63, ProcessComboChangeCmpPatch13, EBX, TRIBE_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00876b68, ProcessComboChangeCmpPatch14, EBX, COLOR_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00876be2, ProcessComboChangeCmpPatch15, EBX, GAME_RULES_COMBO)
		REPLACE_CMP_STATEMENT(0x00875ae1, ProcessComboChangeCmpPatch16, EDI, TRIBE_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00875b0f, ProcessComboChangeCmpPatch17, EDI, TRIBE_COMBO_INDEX_START)
		return true;
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinGetComboValueComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(GetComboValueStart, GetComboValueEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP

			MOV EAX, dword ptr [EBP + 0x8]
			PUSH EAX
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			MOV EAX, dword ptr [EAX]
			MOV EAX, dword ptr [EAX + 0x568]
			POP EBP
			RET 0x4
		}
		INJECTION_CODE_END(GetComboValueEnd)

		return InsertJumpWithFunctionCall(start, end, 0x00876e70, "ExtendedSetupWin::GetComboValue", getComboBoxFuncPtr);
	}

	// Probably needs more patches for 16 players, who knows
	INJECTION_FUNCTION bool Injector::InsertNewSetupWinMakeComboChangeComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(MakeComboFirstPatchStart, MakeComboFirstPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				CMP dword ptr [EAX + 0x568], EBX
				JZ doReturn

				MOV dword ptr [EAX + 0x568], EBX
				TEST byte ptr [EDI + 0x1F14], 0x2
				MOV EAX, 0x00c76ed3
				JMP EAX

				doReturn:
					MOV EAX, 0x00c772c4
					JMP EAX
			}
			INJECTION_CODE_END(MakeComboFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876eb3, "ExtendedSetupWin::MakeComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(MakeComboSecondPatchStart, MakeComboSecondPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				ADD ECX, 0x28C

				MOV EAX, 0x00c76eef
				JMP EAX
			}
			INJECTION_CODE_END(MakeComboSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x00876ee2, "ExtendedSetupWin::MakeComboChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		REPLACE_CMP_STATEMENT(0x00876ed9, FirstSingleLineCmpPatch, ESI, PLAYER_COMBO_COUNT)
		REPLACE_CMP_STATEMENT(0x00876ef4, SecondSingleLineCmpPatch, ESI, TRIBE_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00876fe5, ThirdSingleLineCmpPatch, ESI, COLOR_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x00877016, FourthSingleLineCmpPatch, ESI, HANDICAP_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x008770e2, FifthSingleLineCmpPatch, ESI, DIFFICULTY_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x0087710e, SixthSingleLineCmpPatch, ESI, TEAM_COMBO_INDEX_START)
		return true;
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinValidateStartComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(ValidateStartStart, ValidateStartEnd)
		__asm
		{
			XORPS XMM0, XMM0
			MOVUPS xmmword ptr [ESP + 0x30], XMM0
			MOVUPS xmmword ptr [ESP + 0x40], XMM0
			MOVUPS xmmword ptr [ESP + 0x50], XMM0

			MOV EAX, dword ptr [EDI + 0x216C]
			MOV EAX, dword ptr [EAX + 0x568]
			MOV dword ptr [ESP + 0x24], EAX

			MOV ECX, EDI
			MOV dword ptr [ESP + 0x20], EDI
			MOV EDI, REPLACE_VALUE_INDICATOR

			XOR ESI, ESI
			LAB_00c774e0:
				TEST ESI, ESI
				JZ LAB_00c7751b

				TEST byte ptr [ECX + 0x1F14], 0x2
				JZ LAB_00c77501

				MOV EAX, dword ptr [ECX + 0x2178]
				CMP ESI, dword ptr [EAX + 0x568]
				JG LAB_00c775fd
				JMP LAB_00c7751b

				LAB_00c77501:
					PUSH ESI
					CALL EDI

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]

					CMP EAX, 0x4
					JZ LAB_00c775fd

					CMP EAX, 0x5
					JZ LAB_00c775fd

				LAB_00c7751b:
					INC EBX
					MOV dword ptr [ESP + 0x10], EBX

					MOV EAX, ESI
					ADD EAX, TEAM_COMBO_INDEX_START
					PUSH EAX
					CALL EDI

					MOV EAX, dword ptr [EAX]
					MOV EBX, dword ptr [EAX + 0x568]

					CMP EBX, 0x5
					JNZ LAB_00c77537

					INC dword ptr [ESP + 0x18]
					JMP LAB_00c77566

				LAB_00c77537:
					CMP EBX, 0x8
					JNZ LAB_00c77555

					MOV EAX, dword ptr [ESP + 0xC]
					INC dword ptr [ESP + 0x14]
					INC EAX
					CMP dword ptr [ESP + 0x24], 0x7
					CMOVNZ EAX, dword ptr [ESP + 0xC]
					MOV dword ptr [ESP + 0xC], EAX
					JMP LAB_00c77566

				LAB_00c77555:
					INC dword ptr [ESP + EBX * 0x4 + 0x30]
					MOV EAX, dword ptr [ESP + EBX * 0x4 + 0x30]
					CMP EDX, EAX
					CMOVLE EDX, EAX
					MOV dword ptr [ESP + 0x1C], EDX

				LAB_00c77566:
					MOV EAX, ESI
					ADD EAX, COLOR_COMBO_INDEX_START
					PUSH EAX
					CALL EDI

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]

					MOV dword ptr [ESP + 0x28], EAX
					CMP EAX, NEW_PLAYER_COUNT
					JZ LAB_00c775f9

					INC word ptr [ESP + EAX * 0x2 + 0x40]
					CMP word ptr [ESP + EAX * 0x2 + 0x40], 0x2
					JL LAB_00c775f9
					XOR EDX, EDX

				LAB_00c77590:
					CMP EDX, ESI
					JZ LAB_00c775e8

					TEST EDX, EDX
					JZ LAB_00c775c3

					TEST byte ptr [ECX + 0x1F14], 0x2
					JZ LAB_00c775b1

					MOV EAX, dword ptr [ECX + 0x2178]
					CMP EDX, dword ptr [EAX + 0x568]
					JG LAB_00c775e8
					JMP LAB_00c775c3

					LAB_00c775b1:
						PUSH EDX
						CALL EDI

						MOV EAX, dword ptr [EAX]
						MOV EAX, dword ptr [EAX + 0x568]

						CMP EAX, 0x4
						JZ LAB_00c775e8

						CMP EAX, 0x5
						JZ LAB_00c775e8

					LAB_00c775c3:
						MOV EAX, EDX
						ADD EAX, COLOR_COMBO_INDEX_START
						PUSH EAX
						CALL EDI

						MOV EAX, dword ptr [EAX]
						MOV ECX, dword ptr [ESP + 0x28]
						CMP dword ptr [EAX + 0x568], ECX

						MOV ECX, dword ptr [ESP + 0x2C]
						JNZ LAB_00c775e8
							
						MOV EAX, EDX
						ADD EAX, TEAM_COMBO_INDEX_START
						PUSH EAX
						CALL EDI

						MOV EAX, dword ptr [EAX]
						CMP dword ptr [EAX + 0x568], EBX

						JNZ LAB_00c7769a

					LAB_00c775e8:
						INC EDX
						CMP EDX, NEW_PLAYER_COUNT
						JL LAB_00c77590

						MOV EDX, dword ptr [ESP + 0x1C]

				LAB_00c775f9:
					MOV EBX, dword ptr [ESP + 0x10]

				LAB_00c775fd:
					INC ESI
					CMP ESI, NEW_PLAYER_COUNT
					JL LAB_00c774e0

			MOV EDI, dword ptr [ESP + 0x20]
			MOV ECX, 0x00c7760e
			JMP ECX

			LAB_00c7769a:
				CMP dword ptr [EBP + 0x8], 0x0
				JNZ LAB_00c776cf

				PUSH 0x0
				MOV ECX, 0x179dcc0
				MOV EAX, 0x00e7d5b0
				CALL EAX

				LOAD_STATIC_VALUE(EAX, 0x01535540)
				ADD EAX, 0x10504
				PUSH EAX
				MOV ECX, 0x179dcc0
				MOV EAX, 0x00566dc0
				CALL EAX

				PUSH 0x0
				PUSH 0x0
				MOV ECX, 0x179dcc0
				MOV EAX, 0x00567cc0
				CALL EAX

			LAB_00c776cf:
				XOR EAX, EAX
				POP EDI
				POP ESI
				POP EBX
				MOV ESP, EBP
				POP EBP
				RET 0x8
		}
		INJECTION_CODE_END(ValidateStartEnd)

		return InsertJumpWithFunctionCall(start, end, 0x008774ad, "ExtendedSetupWin::ValidateStart", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinProcessCheckChangeComboBoxHandling()
	{
		{
			INJECTION_CODE_BEGIN(ProcessComboChangeTeamPatchStart, ProcessComboChangeTeamPatchEnd)
			__asm
			{
				XOR ESI, ESI
				LEA EBX, [EDI + 0x214C]

				LAB_00c782b0:
					MOV EAX, dword ptr [EBX]
					CMP dword ptr [EAX + 0x568], NEW_PLAYER_COUNT

					JZ LAB_00c782c8
					PUSH 0x5
					LEA EAX, [ESI + TEAM_COMBO_INDEX_START]
					MOV ECX, EDI
					PUSH EAX
					MOV EAX, 0x00c772e0
					CALL EAX

				LAB_00c782c8:
					INC ESI
					ADD EBX, 0x4
					CMP ESI, NEW_PLAYER_COUNT
					JL LAB_00c782b0

					POP EDI
					POP ESI
					POP EBX
					POP EBP
					RET 0xC
			}
			INJECTION_CODE_END(ProcessComboChangeTeamPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087829e, "ExtendedSetupWin::ProcessComboChange"))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeNationFirstPatchStart, ProcessComboChangeNationFirstPatchEnd)
			__asm
			{
				XOR ESI, ESI

				LAB_00c782e3:
					PUSH 0x18
					LEA EAX, [ESI + TRIBE_COMBO_INDEX_START]
					MOV ECX, EDI
					PUSH EAX
					MOV EAX, 0x00c772e0
					CALL EAX

					INC ESI
					CMP ESI, NEW_PLAYER_COUNT
					JL LAB_00c782e3

					POP EDI
					POP ESI
					POP EBX
					POP EBP
					RET 0xC
			}
			INJECTION_CODE_END(ProcessComboChangeNationFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x008782e1, "ExtendedSetupWin::ProcessComboChange"))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(ProcessComboChangeNationSecondPatchStart, ProcessComboChangeNationSecondPatchEnd)
		__asm
		{
			LAB_00c78310:
				MOV EAX, dword ptr [EDI + 0x2184]
				CMP dword ptr [EAX + 0x568], 0xA
				JNZ LAB_00c7832d

				LOAD_CONNECTION_DATA(EAX)
				CMP byte ptr [EAX + 0xA46], 0x0
				JA LAB_00c7833a

				LAB_00c7832d:
					PUSH 0x18
					LEA EAX, [ESI + TRIBE_COMBO_INDEX_START]
					MOV ECX, EDI
					PUSH EAX
					MOV EAX, 0x00c772e0
					CALL EAX

				LAB_00c7833a:
					INC ESI
					CMP ESI, NEW_PLAYER_COUNT
					JL LAB_00c78310

			POP EDI
			POP ESI
			POP EBX
			POP EBP
			RET 0xC
		}
		INJECTION_CODE_END(ProcessComboChangeNationSecondPatchEnd)

		return InsertJumpWithData(start, end, 0x00878310, "ExtendedSetupWin::ProcessComboChange");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinUpdateUIForRankedComboBoxHandling()
	{
		INJECTION_CODE_BEGIN(Start, End)
		__asm
		{
		}
		INJECTION_CODE_END(End)

		return InsertJumpWithData(start, end, 0x0, "ExtendedSetupWin::");
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupForRankedComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(SetupForRankedStart, SetupForRankedEnd)
		__asm
		{
			PUSH EAX

			MOV EAX, dword ptr [EBP - 0xC]
			SUB EAX, NEW_PLAYER_COUNT
			ADD EAX, TEAM_COMBO_INDEX_START
			PUSH EAX
			MOV ECX, dword ptr [EBP + -0x14]
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			MOV EBX, EAX
			POP EAX
			CMP EAX, 0x1
			XOR ECX, ECX
			MOV EAX, 0x00c7b07a
			JMP EAX
		}
		INJECTION_CODE_END(SetupForRankedEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0087b075, "ExtendedSetupWin::SetupForRanked", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinSetupCombosComboBoxHandling(DWORD getComboBoxFuncPtr, DWORD getColorFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(SetupCombosFirstPatchStart, SetupCombosFirstPatchEnd)
			__asm
			{
				INC EBX
				CMP EBX, COMBO_BOX_COUNT
				MOV EAX, 0x00c7b6df
				JMP EAX
			}
			INJECTION_CODE_END(SetupCombosFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087b6d5, "ExtendedSetupWin::SetupCombos"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupCombosSecondPatchStart, SetupCombosSecondPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ESI, EAX
				MOV dword ptr [EBP - 0x10], ESI

				LOAD_STATIC_VALUE(EAX, 0x014aebb8)
				MOV ECX, 0x17bf410
				MOV EAX, dword ptr [EAX + 0x10]
				CMP EBX, TRIBE_COMBO_INDEX_START
				JGE LAB_00c7b530

				ADD EAX, 0x62C
				PUSH EAX
				MOV EAX, 0x010da9c0
				CALL EAX

				PUSH EBX
				JMP LAB_00c7b5a8

				LAB_00c7b530:
					CMP EBX, COLOR_COMBO_INDEX_START
					JGE LAB_00c7b545

					ADD EAX, 0x13858
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - TRIBE_COMBO_INDEX_START]
					JMP LAB_00c7b5a7

				LAB_00c7b545:
					CMP EBX, HANDICAP_COMBO_INDEX_START
					JGE LAB_00c7b55a

					ADD EAX, 0x2490
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - COLOR_COMBO_INDEX_START]
					JMP LAB_00c7b5a7

				LAB_00c7b55a:
					CMP EBX, DIFFICULTY_COMBO_INDEX_START
					JGE LAB_00c7b56f

					ADD EAX, 0x1D90C
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - HANDICAP_COMBO_INDEX_START]
					JMP LAB_00c7b5a7

				LAB_00c7b56f:
					CMP EBX, TEAM_COMBO_INDEX_START
					JGE LAB_00c7b584

					ADD EAX, 0x8124
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - DIFFICULTY_COMBO_INDEX_START]
					JMP LAB_00c7b5a7

				LAB_00c7b584:
					CMP EBX, PLAYER_COMBO_COUNT
					JGE LAB_00c7b599

					ADD EAX, 0x2594
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - TEAM_COMBO_INDEX_START]
					JMP LAB_00c7b5a7

				LAB_00c7b599:
					ADD EAX, 0x10B44
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					LEA EAX, [EBX - PLAYER_COMBO_COUNT]

				LAB_00c7b5a7:
					PUSH EAX

				LAB_00c7b5a8:
					MOV ECX, 0x00c7b5a8
					JMP ECX
			}
			INJECTION_CODE_END(SetupCombosSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b510, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SetupComboThirdPatchStart, SetupComboThirdPatchEnd)
			__asm
			{
				MOV ESI, REPLACE_VALUE_INDICATOR
				MOV EBX, COMBO_BOX_COUNT

				LAB_00c7b6f0:
					MOV EAX, EBX
					DEC EAX
					PUSH EAX
					MOV ECX, EDI
					CALL ESI

					MOV ECX, dword ptr [EAX]
					TEST ECX, ECX
					JZ LAB_00c7b755

					LEA EAX, [EDI + 0x2EF0]
					CMP dword ptr [ECX + 0x74], EAX
					JNZ LAB_00c7b709

					MOV EAX, dword ptr [EDI + 0x2258]
					JMP LAB_00c7b70f

					LAB_00c7b709:
						MOV EAX, dword ptr [EDI + 0x225C]

					LAB_00c7b70f:
						TEST EAX, EAX
						JZ LAB_00c7b732

						CMP dword ptr [EAX], 0x0
						JZ LAB_00c7b71e

						MOV dword ptr [ECX + 0x1D0], EAX

					LAB_00c7b71e:
						MOV dword ptr [ECX + 0x1D8], 0x0
						MOV dword ptr [ECX + 0x1D4], 0x0

					LAB_00c7b732:
						TEST byte ptr [EDI + 0x1F14], 0x2
						JNZ LAB_00c7b755

						PUSH ECX
						LOAD_STATIC_VALUE(ECX, 0x0171c2e8)
						MOV EAX, dword ptr [ECX]
						MOV EAX, dword ptr [EAX + 0x10]
						CALL EAX
						TEST AL, AL
						POP ECX
						JNZ LAB_00c7b755

						PUSH 0x0
						MOV EAX, 0x0110fb60
						CALL EAX

					LAB_00c7b755:
						SUB EBX, 0x1
						JNZ LAB_00c7b6f0

				MOV EAX, 0x00c7b760
				JMP EAX
			}
			INJECTION_CODE_END(SetupComboThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b6eb, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeFirstPatchStart, SlotTypeFirstPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ESI, EAX
				LOAD_STATIC_VALUE(ECX, 0x017a767c)
				MOV EAX, 0x00c7b784
				JMP EAX
			}
			INJECTION_CODE_END(SlotTypeFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b775, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeSecondPatchStart, SlotTypeSecondPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ESI, EAX
				LOAD_STATIC_VALUE(ECX, 0x017a767c)
				MOV EAX, 0x00c7b8b6
				JMP EAX
			}
			INJECTION_CODE_END(SlotTypeSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b8a7, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeThirdPatchStart, SlotTypeThirdPatchEnd)
			__asm
			{
				PUSH EBX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV dword ptr [EAX + 0x568], EBX
				MOV EAX, 0x00c7b8ef
				JMP EAX
			}
			INJECTION_CODE_END(SlotTypeThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b8e3, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeFourthPatchStart, SlotTypeFourthPatchEnd)
			__asm
			{
				PUSH 0x0
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				ADD EAX, 0x1025C
				PUSH EAX
				MOV EAX, 0x00561310
				CALL EAX

				MOV EAX, 0x00c7b92a
				JMP EAX
			}
			INJECTION_CODE_END(SlotTypeFourthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b919, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeFifthPatchStart, SlotTypeFifthPatchEnd)
			__asm
			{
				PUSH EAX
				PUSH 0x0
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				POP EAX
				ADD EAX, 0x10270
				MOV dword ptr [EBP - 0x4], 0x00c7b9a1
				JMP dword ptr [EBP - 0x4]
			}
			INJECTION_CODE_END(SlotTypeFifthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087b957, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(SlotTypeSixthPatchStart, SlotTypeSixthPatchEnd)
			__asm
			{
				TEST byte ptr [EDI + 0x1F14], 0x2
				JNZ LAB_00c7babf

				LOAD_STATIC_VALUE(EAX, 0x014aea2c)
				CMP byte ptr [EAX + 0x24], 0x7
				JNZ LAB_00c7babf

				MOV EBX, NEW_PLAYER_COUNT
				MOV EAX, EBX
				SHR EAX, 0x2
				SUB EBX, EAX

				teamLoopBegin:
					PUSH EBX
					MOV ECX, EDI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV ECX, dword ptr [EAX + 0x568]
					CMP ECX, 0x1
					JL setValue

					CMP ECX, 0x3
					JLE teamLoopEnd

					setValue:
						MOV dword ptr [EAX + 0x568], 0x1

					teamLoopEnd:
						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL teamLoopBegin

				LAB_00c7babf:
					MOV EBX, 0x00c7babf
					JMP EBX
			}
			INJECTION_CODE_END(SlotTypeSixthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087ba6b, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(TribePatchStart, TribePatchEnd)
			__asm
			{
				XOR EBX, EBX
				MOV dword ptr [EBP - 0x10], EBX

				LAB_00c7bad0:
					LEA EAX, [EBX + TRIBE_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, EDI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV dword ptr [EBP - 0x20], EAX

					LOAD_STATIC_VALUE(EAX, 0x01535540)
					LEA ECX, [EBP - 0x48]
					ADD EAX, 0x102D4
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH 0x18
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EBP - 0x20]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX
					
					LOAD_STATIC_VALUE(EAX, 0x01535540)
					LEA ECX, [EBP - 0x48]
					ADD EAX, 0x102E8
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH 0x19
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EBP - 0x20]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX
					
					LOAD_STATIC_VALUE(EAX, 0x01535540)
					LEA ECX, [EBP - 0x48]
					ADD EAX, 0x102FC
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH 0x1A
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EBP - 0x20]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX
					
					LOAD_STATIC_VALUE(EAX, 0x01535540)
					LEA ECX, [EBP - 0x48]
					ADD EAX, 0x10310
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH 0x1B
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EBP - 0x20]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX
					
					LOAD_STATIC_VALUE(EAX, 0x01535540)
					LEA ECX, [EBP - 0x48]
					ADD EAX, 0x10324
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH 0x1C
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EBP - 0x20]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX

					XOR EBX, EBX
					XOR ESI, ESI

					LAB_00c7bc50:
						LOAD_STATIC_VALUE(ECX, 0x01768734)
						LEA EAX, [EBP - 0x34]
						ADD ECX, ESI
						PUSH ECX
						PUSH EAX
						MOV EAX, 0x00dabed0
						CALL EAX

						MOV byte ptr [EBP - 0x4], 0x4
						LEA ECX, [EBP - 0x48]
						PUSH EAX
						MOV EAX, 0x010da9c0
						CALL EAX

						MOV byte ptr [EBP - 0x4], 0x0
						LEA ECX, [EBP - 0x34]
						MOV EAX, 0x010d8a50
						CALL EAX

						MOV ECX, 0x017a7674
						MOVQ XMM0, qword ptr [ECX]
						SUB ESP, 0x14
						LOAD_STATIC_VALUE(ECX, 0x017a767c)
						MOV EAX, ESP
						SUB ESP, 0xC
						MOVQ qword ptr [EAX], XMM0
						MOV dword ptr [EAX + 0x8], ECX
						MOV EAX, ESP
						PUSH EBX
						MOVQ qword ptr [EAX], XMM0
						MOV dword ptr [EAX + 0x8], ECX
						LEA EAX, [EBP - 0x48]
						PUSH EAX
						MOV ECX, dword ptr [EBP - 0x20]
						MOV EAX, 0x00561310
						CALL EAX

						ADD ESI, 0x5F0
						INC EBX
						CMP ESI, 0x8E80
						JL LAB_00c7bc50

					MOV EBX, dword ptr [EBP - 0x10]
					TEST EBX, EBX
					JNZ LAB_00c7bcdf

					TEST byte ptr [EDI + 0x1F14], 0x2
					JZ LAB_00c7bcdf

					LOAD_STATIC_VALUE(EAX, 0x014aea2c)
					MOVZX ECX, byte ptr [EAX + 0x76]
					JMP LAB_00c7bce4

					LAB_00c7bcdf:
						MOV ECX, 0x18

					LAB_00c7bce4:
						MOV EAX, dword ptr [EBP - 0x20]
						PUSH -0x1
						PUSH 0x5
						MOV dword ptr [EAX + 0x568], ECX
						MOV ECX, dword ptr [EBP - 0x20]
						MOV EAX, 0x005621c0
						CALL EAX

						INC EBX
						MOV dword ptr [EBP - 0x10], EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c7bad0

				MOV EAX, 0x00c7bd0e
				JMP EAX
			}
			INJECTION_CODE_END(TribePatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087babf, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ColorPatchStart, ColorPatchEnd)
			__asm
			{
				XOR EAX, EAX
				MOV dword ptr [EBP - 0x14], 0x0
				MOV dword ptr [EBP - 0x10], EAX

				LAB_00c7bd20:
					PUSH 0xA
					PUSH 0x400
					INC EAX
					PUSH 0x15ab048
					PUSH EAX
					MOV dword ptr [EBP - 0x20], EAX
					MOV ECX, 0x011d5828
					CALL dword ptr [ECX]

					MOV ECX, 0x15ab048
					ADD ESP, 0x10
					LEA EDX, [ECX + 0x2]

					LAB_00c7bd42:
						MOV AX, word ptr [ECX]
						ADD ECX, 0x2
						TEST AX, AX
						JNZ LAB_00c7bd42

					SUB ECX, EDX
					SAR ECX, 1
					PUSH -0x1
					PUSH ECX
					LEA ECX, [EBP - 0x48]
					MOV EAX, 0x010d1c30
					CALL EAX

					TEST EAX, EAX
					JNZ LAB_00c7bd78

					MOV ECX, dword ptr [EBP - 0x48]
					PUSH 0x15ab048
					MOV EAX, dword ptr [ECX + 0x8]
					INC EAX
					PUSH EAX
					PUSH dword ptr [ECX]
					MOV EAX, 0x011d5a88
					CALL dword ptr [EAX]

					ADD ESP, 0xC

					LAB_00c7bd78:
						MOV ESI, dword ptr [EBP - 0x14]
						XOR EBX, EBX

					LAB_00c7bd82:
						LEA EAX, [ESI + COLOR_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, EDI
						MOV EAX, GET_COMBOBOX_REPLACE_INDICATOR
						CALL EAX

						MOV EAX, dword ptr [EAX]
						OR dword ptr [EAX + 0x564], 0x2

						PUSH EBX
						PUSH COLOR_TYPE_TEXT
						LEA EAX, [EBP - 0x2C]
						PUSH EAX
						MOV EAX, GET_COLOR_REPLACE_INDICATOR
						CALL EAX
							
						MOV ECX, 0x01535aa0
						MOVQ XMM0, qword ptr [ECX]
						SUB ESP, 0x14
						LOAD_STATIC_VALUE(ECX, 0x01535aa8)
						MOV EDX, ESP
						SUB ESP, 0xC
						MOVQ qword ptr [EDX], XMM0
						MOVQ XMM0, qword ptr [EAX]
						MOV EAX, dword ptr [EAX + 0x8]
						MOV dword ptr [EDX + 0x8], ECX
						MOV ECX, ESP
						PUSH EBX
						MOVQ qword ptr [ECX], XMM0
						MOV dword ptr [ECX + 0x8], EAX
						LEA EAX, [EBP - 0x48]
						PUSH EAX

						LEA EAX, [ESI + COLOR_COMBO_INDEX_START]
						PUSH EAX
						MOV ECX, EDI
						MOV EAX, GET_COMBOBOX_REPLACE_INDICATOR
						CALL EAX

						MOV ECX, dword ptr [EAX]
						MOV EAX, 0x00561310
						CALL EAX

						INC EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c7bd82
							
					MOV EBX, dword ptr [EBP - 0x14]
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, EDI
					MOV EAX, GET_COMBOBOX_REPLACE_INDICATOR
					CALL EAX
							
					MOV EAX, dword ptr [EAX]
					LEA ECX, [EBP - 0x48]
					OR dword ptr [EAX + 0x564], 0x2
					LOAD_STATIC_VALUE(EAX, 0x014aebb8)
					MOV EAX, dword ptr [EAX + 0x10]
					ADD EAX, 0x260C
					PUSH EAX
					MOV EAX, 0x010da9c0
					CALL EAX

					MOV EAX, 0x017a7674
					MOVQ XMM0, qword ptr [EAX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(EDX, 0x017a767c)
							
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, EDI
					MOV EAX, GET_COMBOBOX_REPLACE_INDICATOR
					CALL EAX
							
					MOV ECX, dword ptr [EAX]
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], EDX
					MOV EAX, ESP
					PUSH 0x8
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], EDX
					LEA EAX, [EBP - 0x48]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX

					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					MOV ECX, EDI
					MOV EAX, GET_COMBOBOX_REPLACE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV ECX, dword ptr [EBP - 0x10]
					LOAD_STATIC_VALUE(EAX, 0x014aea2c)
					MOV AL, byte ptr [EAX + 0x24]
					SUB AL, 0x8
					CMP AL, 0x2
					JA LAB_00c7be5f

					SHR ECX, 1

					LAB_00c7be5f:
						INC EBX
						MOV dword ptr [EAX + 0x568], ECX
						MOV EAX, dword ptr [EBP - 0x20]
						MOV dword ptr [EBP - 0x10], EAX
						MOV dword ptr [EBP - 0x14], EBX
						CMP EBX, NEW_PLAYER_COUNT
						JL LAB_00c7bd20

				MOV EAX, 0x00c7be7a
				JMP EAX
			}
			INJECTION_CODE_END(ColorPatchEnd)

			if (!InsertJumpWithFunctionCalls(
				start,
				end,
				0x0087bd0e,
				"ExtendedSetupWin::SetupCombos",
				{
					{ GET_COLOR_REPLACE_INDICATOR, getColorFuncPtr },
					{ GET_COMBOBOX_REPLACE_INDICATOR, getComboBoxFuncPtr }
				}))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(HandicapPatchStart, HandicapPatchEnd)
			__asm
			{
				TEST byte ptr [EDI + 0x1F14], 0x2
				JZ LAB_00c7bea3

				MOV ECX, EDI
				PUSH HANDICAP_COMBO_INDEX_START
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV ECX, dword ptr [EAX]
				LOAD_STATIC_VALUE(EAX, 0x014aea2c)
				MOVZX EAX, byte ptr [EAX + 0x2B]

				PUSH 0x1
				PUSH EAX
				PUSH 0x1768950
				PUSH 0x6
				MOV EAX, 0x00ebaa70
				CALL EAX
				JMP LAB_00c7beca

				LAB_00c7bea3:
					MOV ESI, REPLACE_VALUE_INDICATOR
					MOV EBX, NEW_PLAYER_COUNT

					LAB_00c7beb0:
						MOV EAX, HANDICAP_COMBO_INDEX_START
						ADD EAX, EBX
						DEC EAX
						PUSH EAX
						MOV ECX, EDI
						CALL ESI

						MOV ECX, dword ptr [EAX]
						PUSH 0x1
						PUSH 0x0
						PUSH 0x1768e28
						PUSH 0x15
						MOV EAX, 0x00ebaa70
						CALL EAX

						SUB EBX, 0x1
						JNZ LAB_00c7beb0

				LAB_00c7beca:
					MOV EAX, 0x00c7beca
					JMP EAX
			}
			INJECTION_CODE_END(HandicapPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087be7a, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}
		
		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(DifficultyPatchStart, DifficultyPatchEnd)
			__asm
			{
				MOV ESI, REPLACE_VALUE_INDICATOR
				MOV EBX, NEW_PLAYER_COUNT

				LAB_00c7bed5:
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EBX
					ADD EAX, DIFFICULTY_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, EDI
					CALL ESI

					MOV ECX, dword ptr [EAX]
					PUSH 0x1
					PUSH 0x2
					PUSH 0x1768950
					PUSH 0x6
					MOV EAX, 0x00ebaa70
					CALL EAX
						
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EBX
					ADD EAX, DIFFICULTY_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, EDI
					CALL ESI

					MOV ECX, dword ptr [EAX]
					PUSH 0x179d06c
					ADD ECX, 0x550
					MOV EAX, 0x010da9c0
					CALL EAX
						
					MOV EAX, NEW_PLAYER_COUNT
					SUB EAX, EBX
					ADD EAX, HANDICAP_COMBO_INDEX_START
					PUSH EAX
					MOV ECX, EDI
					CALL ESI

					MOV ECX, dword ptr [EAX]
					PUSH 0x179d06c
					ADD ECX, 0x550
					MOV EAX, 0x010da9c0
					CALL EAX

					SUB EBX, 0x1
					JNZ LAB_00c7bed5

				MOV EAX, 0x00c7bf14
				JMP EAX
			}
			INJECTION_CODE_END(DifficultyPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087beca, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(TeamPatchStart, TeamPatchEnd)
		__asm
		{
			MOV dword ptr [EBP - 0x10], NEW_PLAYER_COUNT

			LAB_00c7bf50:
				MOV EAX, NEW_PLAYER_COUNT
				SUB EAX, dword ptr [EBP - 0x10]
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EBX, EAX
				MOV dword ptr [EBP - 0x14], EBX
				MOV EAX, dword ptr [EBX]
				LEA ECX, [EBP - 0x48]
				OR dword ptr [EAX + 0x564], 0x2
				LOAD_STATIC_VALUE(EAX, 0x014aebb8)
				MOV EAX, dword ptr [EAX + 0x10]
				ADD EAX, 0x4DA8
				PUSH EAX
				MOV EAX, 0x010da9c0
				CALL EAX

				MOV EAX, 0x017a7674
				MOVQ XMM0, qword ptr [EAX]
				SUB ESP, 0x14
				LOAD_STATIC_VALUE(ECX, 0x017a767c)
				MOV EAX, ESP
				SUB ESP, 0xC
				MOVQ qword ptr [EAX], XMM0
				MOV dword ptr [EAX + 0x8], ECX
				MOV EAX, ESP
				PUSH 0x8
				MOVQ qword ptr [EAX], XMM0
				MOV dword ptr [EAX + 0x8], ECX
				LEA EAX, [EBP - 0x48]
				MOV ECX, dword ptr [EBX]
				PUSH EAX
				MOV EAX, 0x00561310
				CALL EAX

				LOAD_STATIC_VALUE(EAX, 0x014aebb8)
				LEA ECX, [EBP - 0x48]
				MOV EAX, dword ptr [EAX + 0x10]
				ADD EAX, 0x260C
				PUSH EAX
				MOV EAX, 0x010da9c0
				CALL EAX

				MOV ECX, 0x017a7674
				MOVQ XMM0, qword ptr [ECX]
				SUB ESP, 0x14
				LOAD_STATIC_VALUE(ECX, 0x017a767c)
				MOV EAX, ESP
				SUB ESP, 0xC
				MOVQ qword ptr [EAX], XMM0
				MOV dword ptr [EAX + 0x8], ECX
				MOV EAX, ESP
				PUSH 0x5
				MOVQ qword ptr [EAX], XMM0
				MOV dword ptr [EAX + 0x8], ECX
				LEA EAX, [EBP - 0x48]
				MOV ECX, dword ptr [EBX]
				PUSH EAX
				MOV EAX, 0x00561310
				CALL EAX

				MOV EDI, dword ptr [EBP - 0x14]
				XOR ESI, ESI

				LAB_00c7bff0:
					PUSH 0xA
					PUSH 0x400
					LEA EBX, [ESI + 0x1]
					PUSH 0x15ab048
					PUSH EBX
					MOV EAX, 0x011d5828
					CALL dword ptr [EAX]

					MOV EAX, 0x15ab048
					ADD ESP, 0x10
					LEA EDX, [EAX + 0x2]

				LAB_00c7c011:
					MOV CX, word ptr [EAX]
					ADD EAX, 0x2
					TEST CX, CX
					JNZ LAB_00c7c011

					SUB EAX, EDX
					LEA ECX, [EBP - 0x48]
					SAR EAX, 1
					PUSH -0x1
					PUSH EAX
					MOV EAX, 0x010d1c30
					CALL EAX

					TEST EAX, EAX
					JNZ LAB_00c7c047

					MOV ECX, dword ptr [EBP - 0x48]
					PUSH 0x15ab048
					MOV EAX, dword ptr [ECX + 0x8]
					INC EAX
					PUSH EAX
					PUSH dword ptr [ECX]
					MOV EAX, 0x011d5a88
					CALL dword ptr [EAX]

					ADD ESP, 0xC

				LAB_00c7c047:
					MOV ECX, 0x017a7674
					MOVQ XMM0, qword ptr [ECX]
					SUB ESP, 0x14
					LOAD_STATIC_VALUE(ECX, 0x017a767c)
					MOV EAX, ESP
					SUB ESP, 0xC
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					MOV EAX, ESP
					PUSH ESI
					MOVQ qword ptr [EAX], XMM0
					MOV dword ptr [EAX + 0x8], ECX
					LEA EAX, [EBP - 0x48]
					MOV ECX, dword ptr [EDI]
					PUSH EAX
					MOV EAX, 0x00561310
					CALL EAX

					MOV ESI, EBX
					CMP ESI, 0x4
					JL LAB_00c7bff0
						
				MOV EBX, dword ptr [EBP - 0x14]
				MOV ECX, dword ptr [EBP - 0x18]
				MOV EDI, dword ptr [EBP - 0x1C]

				MOV EAX, dword ptr [EBX]
				MOV dword ptr [EAX + 0x568], ECX

				SUB dword ptr [EBP - 0x10], 0x1
				JNZ LAB_00c7bf50

			TEST byte ptr [EDI + 0x1F14], 0x2
			JNZ LAB_00c7c0e3

			LOAD_STATIC_VALUE(EAX, 0x014aea2c)
			CMP byte ptr [EAX + 0x24], 0x7
			JNZ LAB_00c7c0e3

			MOV EBX, NEW_PLAYER_COUNT
			MOV EAX, EBX
			SHR EAX, 0x2
			SUB EBX, EAX

			loopBegin:
				MOV EAX, EBX
				ADD EAX, TEAM_COMBO_INDEX_START
				PUSH EAX
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV dword ptr [EAX + 0x568], 0x8

				INC EBX
				CMP EBX, NEW_PLAYER_COUNT
				JL loopBegin

			LAB_00c7c0e3:
				MOV EAX, 0x00c7c0e3
				JMP EAX
		}
		INJECTION_CODE_END(TeamPatchEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0087bf36, "ExtendedSetupWin::SetupCombos", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinFiletypeComboComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(FiletypeComboFirstPatchStart, FiletypeComboFirstPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EBX
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				LEA ECX, [EAX + 0x550]
				LEA EAX, [EBP - 0x4C]
				PUSH EAX
				MOV EAX, 0x010da9c0
				CALL EAX

				MOV EAX, 0x00c7c947
				JMP EAX
			}
			INJECTION_CODE_END(FiletypeComboFirstPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087c931, "ExtendedSetupWin::FiletypeCombo()", getComboBoxFuncPtr, JumpType::USE_ECX))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(FiletypeComboSecondPatchStart, FiletypeComboSecondPatchEnd)
		__asm
		{
			PUSH ESI
			MOV ECX, EBX
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			MOV EDX, EAX
			LOAD_STATIC_VALUE(ECX, 0x017a767c)
			MOV EAX, 0x00c7c993
			JMP EAX
		}
		INJECTION_CODE_END(FiletypeComboSecondPatchEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0087c984, "ExtendedSetupWin::FiletypeCombo()", getComboBoxFuncPtr, JumpType::USE_ECX);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinCategoryComboComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(CategoryComboStart, CategoryComboEnd)
		__asm
		{
			PUSH EBP
			MOV EBP, ESP

			PUSH dword ptr [EBP + 0x4]
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			PUSH 0x1
			PUSH dword ptr [EBP + 0x10]
			PUSH dword ptr [EBP + 0xC]
			PUSH dword ptr [EBP + 0x8]

			MOV ECX, dword ptr [EAX]
			MOV EAX, 0x00ebaa70
			CALL EAX
			POP EBP
			RET 0x10
		}
		INJECTION_CODE_END(CategoryComboEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0087d3e0, "ExtendedSetupWin::CategoryCombo()", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnPulldownChangeComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(PulldownChangeFirstPatchStart, PulldownChangeFirstPatchEnd)
			__asm
			{
				INC ESI
				ADD EDI, 0x4
				CMP ESI, COMBO_BOX_COUNT
				JL jumpBackToBeginning

				MOV ECX, 0x00c7f323
				JMP ECX

				jumpBackToBeginning:
					MOV ECX, 0x00c7f0e2
					JMP ECX
			}
			INJECTION_CODE_END(PulldownChangeFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087f10e, "ExtendedSetupWin::OnPulldownChange"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(PulldownChangeSecondPatchStart, PulldownChangeSecondPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EBX
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDI, EAX
				MOV ECX, dword ptr [EDI]
				MOV EAX, 0x01110190
				CALL EAX
				TEST EAX, EAX

				MOV EAX, 0x00c7f0eb
				JMP EAX
			}
			INJECTION_CODE_END(PulldownChangeSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f0e2, "ExtendedSetupWin::OnPulldownChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(PulldownChangeThirdPatchStart, PulldownChangeThirdPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EBX
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV ECX, dword ptr [EAX + 0x184]

				MOV EAX, 0x00c7f287
				JMP EAX
			}
			INJECTION_CODE_END(PulldownChangeThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f27a, "ExtendedSetupWin::OnPulldownChange", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		INJECTION_FUNCTION INJECTION_CODE_BEGIN(PulldownChangeFourthPatchStart, PulldownChangeFourthPatchEnd)
		__asm
		{
			PUSH ESI
			MOV ECX, EBX
			MOV EAX, REPLACE_VALUE_INDICATOR
			CALL EAX

			MOV ECX, dword ptr [EAX]
			LEA EAX, [EBP - 0x14]
			PUSH EAX

			MOV EAX, 0x00c7f2ac
			JMP EAX
		}
		INJECTION_CODE_END(PulldownChangeFourthPatchEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0087f2a1, "ExtendedSetupWin::OnPulldownChange", getComboBoxFuncPtr);
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinOnComboMoveComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(OnComboMoveFirstPatchStart, OnComboMoveFirstPatchEnd)
			__asm
			{
				MOV EAX, ESI
				CMP ESI, PLAYER_COMBO_COUNT
				JGE LAB_00c7f4c0

				CDQ
				AND EDX, 0x7
				ADD EAX, EDX
				AND EAX, 0xFFFFFFF8

				LAB_00c7f4c0:
					PUSH EBX
					LEA EBX, [EAX + EAX * 0x2]
					MOV EAX, 0x00c7f4c4
					JMP EAX
			}
			INJECTION_CODE_END(OnComboMoveFirstPatchEnd)

			if (!InsertJumpWithData(start, end, 0x0087f4b0, "ExtendedSetupWin::OnComboMove"))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(OnComboMoveSecondPatchStart, OnComboMoveSecondPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV ECX, dword ptr [EAX + 0x184]
				TEST ECX, ECX
				MOV EAX, 0x00c7f513
				JMP EAX
			}
			INJECTION_CODE_END(OnComboMoveSecondPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f504, "ExtendedSetupWin::OnComboMove", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(OnComboMoveThirdPatchStart, OnComboMoveThirdPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDX, dword ptr [EAX]
				PUSH EDX
				MOV ECX, EDX
				LEA EAX, [EBP - 0x28]
				PUSH EAX
				MOV EAX, 0x0110fa30
				CALL EAX

				MOV EAX, dword ptr [EDI + 0x2590]
				LEA ECX, [EBP - 0x40]
				ADD EAX, EBX
				PUSH EAX
				MOV EAX, 0x010da9c0
				CALL EAX

				LEA EAX, [EBP - 0x40]
				PUSH EAX
				LEA ECX, [EBP - 0x20]
				MOV EAX, 0x010da9c0
				CALL EAX

				POP EDX
				MOV ECX, dword ptr [EDX + 0x74]
				MOV EAX, 0x00c7f562
				JMP EAX
			}
			INJECTION_CODE_END(OnComboMoveThirdPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f52b, "ExtendedSetupWin::OnComboMove", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(OnComboMoveFourthPatchStart, OnComboMoveFourthPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EAX, dword ptr [EAX]
				MOV EBX, dword ptr [EAX + 0x568]
				MOV EAX, 0x00c7f5d2
				JMP EAX
			}
			INJECTION_CODE_END(OnComboMoveFourthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f5c5, "ExtendedSetupWin::OnComboMove", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(OnComboMoveFifthPatchStart, OnComboMoveFifthPatchEnd)
			__asm
			{
				PUSH ESI
				MOV ECX, EDI
				MOV EAX, REPLACE_VALUE_INDICATOR
				CALL EAX

				MOV EDX, dword ptr [EAX]
				MOV ECX, dword ptr [EDX + 0x74]
				TEST ECX, ECX
				MOV EAX, 0x00c7f702
				JMP EAX
			}
			INJECTION_CODE_END(OnComboMoveFifthPatchEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x0087f6f6, "ExtendedSetupWin::OnComboMove", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		REPLACE_CMP_STATEMENT(0x0087f778, FirstSingleLinePatch, ESI, MODS_COMBO)
		REPLACE_SINGLE_LINE(0x0087f745, SecondSingleLinePatch, LEA EAX COMMA [ESI - PLAYER_COMBO_COUNT])
		REPLACE_SINGLE_LINE(0x0087f57f, ThirdSingleLinePatch, LEA EAX COMMA [ESI - STARTING_TOWNS_COMBO])
		REPLACE_CMP_STATEMENT(0x0087f587, FourthSingleLinePatch, ESI, CANNON_TIMES_COMBO)
		REPLACE_CMP_STATEMENT(0x0087f58c, FifthSingleLinePatch, ESI, MODS_COMBO)
		REPLACE_SINGLE_LINE(0x0087f5d2, SixthSingleLinePatch, LEA EAX COMMA [ESI - TRIBE_COMBO_INDEX_START])
		REPLACE_CMP_STATEMENT(0x0087f5d5, SeventhSingleLinePatch, EAX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_SINGLE_LINE(0x0087f677, EighthSingleLinePatch, LEA EAX COMMA [ESI - HANDICAP_COMBO_INDEX_START])
		REPLACE_CMP_STATEMENT(0x0087f67a, NinthSingleLinePatch, EAX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_SINGLE_LINE(0x0087f69a, TenthSingleLinePatch, LEA EAX COMMA [ESI - TRIBE_COMBO_INDEX_START])
		REPLACE_CMP_STATEMENT(0x0087f69d, EleventhSingleLinePatch, EAX, NEW_PLAYER_COUNT_MINUS_ONE)
		REPLACE_CMP_STATEMENT(0x0087f6af, TwelvthSingleLinePatch, ESI, NUMBER_OF_PLAYERS_COMBO)
		REPLACE_CMP_STATEMENT(0x0087f6b4, ThirteenthSingleLinePatch, ESI, SCRIPS_COMBO)
		REPLACE_CMP_STATEMENT(0x0087f6b9, FourteenthSingleLinePatch, ESI, TRIBE_COMBO_INDEX_START)
		REPLACE_CMP_STATEMENT(0x0087f6be, FifteenthSingleLinePatch, ESI, COLOR_COMBO_INDEX_START)

		return true;
	}

	INJECTION_FUNCTION bool Injector::InsertNewSetupWinExecComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		{
			INJECTION_CODE_BEGIN(SetupWinExecStart, SetupWinExecEnd)
			__asm
			{
				XOR EDI, EDI
				XOR EBX, EBX

				loopBegin:
					MOV ECX, ESI
					PUSH EDI
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EAX, dword ptr [EAX]
					MOV EDX, dword ptr [EAX + 0x568]

					LOAD_CONNECTION_DATA(ECX)
					ADD ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN

					CMP EDI, TRIBE_COMBO_INDEX_START
					JGE checkNation

					MOV EAX, EDI
					MOV byte ptr [ECX + EBX + 0x34], DL
					JMP sendPlayer

					checkNation:
						CMP EDI, COLOR_COMBO_INDEX_START
						JGE checkColor

						LEA EAX, [EDI - TRIBE_COMBO_INDEX_START]
						MOV byte ptr [ECX + EBX + 0x35], DL
						JMP sendPlayer

					checkColor:
						CMP EDI, HANDICAP_COMBO_INDEX_START
						JGE checkHandicap
							
						LEA EAX, [EDI - COLOR_COMBO_INDEX_START]
						MOV byte ptr [ECX + EBX + 0x36], DL
						JMP sendPlayer

					checkHandicap:
						CMP EDI, DIFFICULTY_COMBO_INDEX_START
						JGE checkDifficulty
							
						LEA EAX, [EDI - HANDICAP_COMBO_INDEX_START]
						MOV byte ptr [ECX + EBX + 0x38], DL
						JMP sendPlayer

					checkDifficulty:
						CMP EDI, TEAM_COMBO_INDEX_START
						JGE checkTeam
							
						LEA EAX, [EDI - DIFFICULTY_COMBO_INDEX_START]
						MOV byte ptr [ECX + EBX + 0x39], DL
						JMP sendPlayer

					checkTeam:
						LEA EAX, [EDI - TEAM_COMBO_INDEX_START]
						MOV byte ptr [ECX + EBX + 0x39], DL

					sendPlayer:
						PUSH 0x0
						PUSH EAX
						SUB ECX, CONNECTION_DATA_PLAYER_DATA_SERVER_BEGIN
						MOV EAX, 0x0100a220 ; ConnectionData::send_player
						CALL EAX

						ADD EBX, 0x3B
						INC EDI
						CMP EDI, NEW_PLAYER_COUNT
						JL loopBegin

						MOV EAX, 0x00c83580
						JMP EAX
			}
			INJECTION_CODE_END(SetupWinExecEnd)

			if (!InsertJumpWithFunctionCall(start, end, 0x008834eb, "ExtendedSetupWin::Exec", getComboBoxFuncPtr))
			{
				return false;
			}
		}

		{
			INJECTION_FUNCTION INJECTION_CODE_BEGIN(ExecPrefsStart, ExecPrefsEnd)
			__asm
			{
				PUSH EBX
				PUSH ECX
				PUSH EDX
				PUSH EDI

				XOR EBX, EBX
				MOV ECX, ESI
				MOV EDX, REPLACE_VALUE_INDICATOR
				MOV EDI, 0x017b5e8c

				prefsUpdateLoopBegin:
					PUSH EBX
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					MOV dword ptr [EDI], EAX
					
					LEA EAX, [EBX + TRIBE_COMBO_INDEX_START]
					PUSH EAX
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					MOV dword ptr [EDI + 0x20], EAX
					
					LEA EAX, [EBX + COLOR_COMBO_INDEX_START]
					PUSH EAX
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					MOV dword ptr [EDI + 0x40], EAX
					
					LEA EAX, [EBX + DIFFICULTY_COMBO_INDEX_START]
					PUSH EAX
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					MOV dword ptr [EDI + 0x60], EAX
					
					LEA EAX, [EBX + TEAM_COMBO_INDEX_START]
					PUSH EAX
					CALL EDX

					MOV EAX, dword ptr [EAX]
					MOV EAX, dword ptr [EAX + 0x568]
					MOV dword ptr [EDI + 0x80], EAX

					ADD EDI, 0x4
					INC EBX
					CMP EBX, NEW_PLAYER_COUNT
					JL prefsUpdateLoopbegin
						
				POP EDI
				POP EDX
				POP ECX
				POP EBX

				MOV EAX, 0x00c83cc1
				JMP EAX
			}
			INJECTION_CODE_END(ExecPrefsEnd)

			if (!InsertJumpWithData(start, end, 0x00883a19, "ExtendedSetupWin::Exec"))
			{
				return false;
			}
		}

		REPLACE_PUSH_VALUE(0x008830af, GAME_SPEEDS_COMBO, DefaultSettingsGameSpeed)
		REPLACE_PUSH_VALUE(0x008830c7, UNKNOWN_COMBO, DefaultSettingsUnknown)
		REPLACE_PUSH_VALUE(0x008830df, STARTING_TOWNS_COMBO, DefaultSettingsStartingTowns)
		REPLACE_PUSH_VALUE(0x008830f7, STARTING_RESOURCES_COMBO, DefaultSettingsStartingResources)
		REPLACE_PUSH_VALUE(0x0088310f, STARTING_RESOURCES2_COMBO, DefaultSettingsStartingResources2)
		REPLACE_PUSH_VALUE(0x00883127, TECH_COSTS_COMBO, DefaultSettingsTechCosts)
		REPLACE_PUSH_VALUE(0x0088313f, REVEAL_MAPS_COMBO, DefaultSettingsRevealMaps)
		REPLACE_PUSH_VALUE(0x00883157, POP_LIMITS_COMBO, DefaultSettingsPopLimits)
		REPLACE_PUSH_VALUE(0x0088316f, RUSH_RULES_COMBO, DefaultSettingsRushRules)
		REPLACE_PUSH_VALUE(0x00883187, CANNON_TIMES_COMBO, DefaultSettingsCannonTimes)
		REPLACE_PUSH_VALUE(0x0088319f, STARTING_TECHNOLOGIES_COMBO, DefaultSettingsStartingTechnologies)
		REPLACE_PUSH_VALUE(0x008831b7, STARTING_TECHNOLOGIES2_COMBO, DefaultSettingsStartingTechnologies2)
		REPLACE_PUSH_VALUE(0x008831cf, ENDING_TECHNOLOGIES_COMBO, DefaultSettingsEndingTechnologies)
		REPLACE_PUSH_VALUE(0x008831e7, ELEMINATIONS_COMBO, DefaultSettingsEleminationsCombo)
		REPLACE_PUSH_VALUE(0x008831ff, VICTORIES_COMBO, DefaultSettingsVictoriesCombo)
		REPLACE_PUSH_VALUE(0x00883217, WONDERWINS_COMBO, DefaultSettingsWonderwins)
		REPLACE_PUSH_VALUE(0x0088322f, SCORES_COMBO, DefaultSettingsScores)
		REPLACE_PUSH_VALUE(0x00883247, POPWINS_COMBO, DefaultSettingsPopwins)
		REPLACE_PUSH_VALUE(0x0088325f, TIME_LIMITS_COMBO, DefaultSettingsTimeLimits)
		REPLACE_PUSH_VALUE(0x00883277, CHAIRS_COMBO, DefaultSettingsChairs)
		REPLACE_PUSH_VALUE(0x0088328f, ECOWINS_COBMO, DefaultSettingsEcowins)
		REPLACE_PUSH_VALUE(0x008832a7, SCENARIOS_COMBO, DefaultSettingsScenarios)
		REPLACE_PUSH_VALUE(0x008832bf, SCRIPS_COMBO, DefaultSettingsScripts)
		REPLACE_PUSH_VALUE(0x008832d7, MODS_COMBO, DefaultSettingsMods)
		REPLACE_PUSH_VALUE(0x008832e6, GAME_RULES_COMBO, DefaultSettingsGameRules)
			
		REPLACE_PUSH_VALUE(0x008833ea, MAP_SIZES_COMBO, ExecComboFirstParam)
		REPLACE_PUSH_VALUE(0x008833f7, MAP_STYLES_COMBO, ExecComboSecondParam)
		REPLACE_PUSH_VALUE(0x00883406, GAME_RULES_COMBO, ExecComboThirdParam)
			
		REPLACE_PUSH_VALUE(0x0088344e, TEAM_STYLES_COMBO, ExecComboFourthParam)
		REPLACE_PUSH_VALUE(0x008835d0, MODS_COMBO, ExecComboFifthParam)
		return true;
	}

	// TODO relies on unpatched allocated_slots
	INJECTION_FUNCTION bool Injector::InsertNewSetupWinResetGameComboBoxHandling(DWORD getComboBoxFuncPtr)
	{
		INJECTION_CODE_BEGIN(ResetGameStart, ResetGameEnd)
		__asm
		{
			MOV ESI, PLAYER_COMBO_COUNT

			LAB_00c6c1e0:
				CMP ESI, UNKNOWN_COMBO
				JZ LAB_00c6c24a

				LEA ECX, [ESI - PLAYER_COMBO_COUNT]
				CMP ECX, 0x1D
				JA LAB_00c6c1f6

				MOV EAX, 0x00c92bc0
				CALL EAX

				MOV EDX, EAX
				JMP LAB_00c6c1f8

				LAB_00c6c1f6:
					XOR EDX, EDX

				LAB_00c6c1f8:
					CMP ESI, ENDING_TECHNOLOGIES_COMBO
					JNZ LAB_00c6c1fe

					DEC EDX

				LAB_00c6c1fe:
					LOAD_STATIC_VALUE(EAX, 0x014aea2c)
					CMP byte ptr [EAX + 0x4A4], 0x0
					JZ LAB_00c6c237

					PUSH ESI
					MOV ECX, EBX
					MOV EAX, REPLACE_VALUE_INDICATOR
					CALL EAX

					MOV EDI, dword ptr [EAX]
					TEST EDI, EDI
					JZ LAB_00c6c237

					MOV ECX, dword ptr [EDI + 0x74]
					TEST ECX, ECX
					JZ LAB_00c6c227

					MOV EAX, 0x004640f0
					CALL EAX

					TEST EAX, EAX
					JZ LAB_00c6c247

				LAB_00c6c227:
					TEST byte ptr [EDI + 0x8], 0x18
					MOV EAX, 0x0
					SETZ AL
					TEST EAX, EAX
					JZ LAB_00c6c247

				LAB_00c6c237:
					MOV CL, byte ptr [EBP - 0x1]
					TEST ECX, ECX
					JZ LAB_00c6c299

					PUSH EDX
					PUSH ESI
					MOV ECX, EBX
					MOV EAX, 0x00c772e0
					CALL EAX

				LAB_00c6c247:
					MOV CL, byte ptr [EBP - 0x1]

				LAB_00c6c24a:
					INC ESI
					CMP ESI, COMBO_BOX_COUNT
					JL LAB_00c6c1e0

			MOV EAX, 0x00c6c250
			JMP EAX

			LAB_00c6c299:
				LOAD_CONNECTION_DATA(EAX)
				MOV byte ptr [EAX + ESI + 0x268], DL
				JMP LAB_00c6c24a
		}
		INJECTION_CODE_END(ResetGameEnd)

		return InsertJumpWithFunctionCall(start, end, 0x0086c1da, "ExtendedSetupWin::ResetGame", getComboBoxFuncPtr);
	}

#pragma endregion COMBO_BOXES

	bool Injector::HandleSetupWin(const DWORD getColorFuncPtr)
	{
		DWORD getComboBoxLocation = WriteGetComboBoxAtFunction();
		if (getComboBoxLocation == -1)
		{
			return false;
		}

		// Leaving a game and then joining one _may_ crash the game?
		return RemoveFloodProtection()
				&& ReduceStartCountdown()
				&& InsertNewSetupWinConstructorHandling()
				// TODO investigate why freeing the malloc'd memory results in a memory corruption thingy
				// && InsertNewSetupWinDestructorComboBoxHandling()
				&& InsertNewSetupWinSetupEloBoxesEloBoxHandling()
				&& InsertNewSetupWinUpdateUIPlayerEloEloBoxHandling()
				&& InsertNewSetupWinUpdateInterfaceEloBoxHandling()
				&& InsertNewSetupWinUpdateInterfaceReadyBoxesHandling()
				&& InsertNewSetupWinSetupChecksReadyBoxesHandling()
				&& InsertNewSetupWinOnCheckMoveReadyBoxesHandling()

				&& InsertNewSetupWinUpdateInterfaceComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinSetupGameComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinDoScenarioScriptSyncComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinDoScenarioChangesComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinValidateScenarioStartComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinCheckScenarioSettingsComboBoxHandling(getComboBoxLocation)
				// TODO Patch
				// && InsertNewSetupWinValidateScenarioSelectComboBoxHandling()
				&& InsertNewSetupWinProcessComboChangeComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinGetComboValueComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinMakeComboChangeComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinValidateStartComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinProcessCheckChangeComboBoxHandling()
				// TODO depends on ConnectionData player data patch!
				// && InsertNewSetupWinUpdateUIForRankedComboBoxHandling()
				&& InsertNewSetupWinSetupForRankedComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinSetupCombosComboBoxHandling(getComboBoxLocation, getColorFuncPtr)
				&& InsertNewSetupWinFiletypeComboComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinCategoryComboComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinOnPulldownChangeComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinOnComboMoveComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinExecComboBoxHandling(getComboBoxLocation)
				&& InsertNewSetupWinResetGameComboBoxHandling(getComboBoxLocation);
	}
}